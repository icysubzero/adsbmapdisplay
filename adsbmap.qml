import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12
import QtPositioning 5.12
import QtLocation 5.12
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Universal 2.12
import AdsbListenerModel 1.0
import FAAChart 1.0
import InstalledCharts 1.0
import AvailableCharts 1.0
import DownloadManager 1.0
import Settings 1.0
import QtQuick.VirtualKeyboard 2.4

Window {
    visible: true
    width: 800
    height: 480
    id:window

    property Window appWindow : window
    property alias map: maploader.item
    property bool noChart: settings.chartIndex === 0

    Universal.theme: Universal.Dark
    Universal.accent: Universal.Yellow

    Loader {
        id: maploader
        anchors.fill: parent
    }

    Component.onCompleted: {        
        settings.maxGainIndex = adsbListenerModel.getMaxGainIndex()
        brightnessButton.setBrightness(settings.brightness)
        changeHost()
    }

    Connections {
        target: settings

        function onOwnshipChanged() { adsbListenerModel.setOwnshipString(settings.ownshipicao) }
    }

    // Start an ADS-B Listener object to read data from the ADS-B antenna
    AdsbListenerModel {
        id: adsbListenerModel
        hostname: params.hostname
        port: params.port
        ownship: settings.ownshipicao
        ownshipGainAdjust: settings.scanOwnship
        ownshipGainIndex: settings.ownshipGainIndex
        normalGainIndex: settings.normalGainIndex        
        projectedTrackMinutes: settings.projectedTrackMinutes
    }

    Item {
        id: wifichooserpopup
        visible: false
        width: parent.width * 0.8
        height: parent.height * 0.95
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 10

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12
        }
        Loader {
            anchors.fill: parent
            anchors.margins: 5
            id: wifichooserloader
        }
        Connections {
            target: wifichooserloader.item

            function onClosed() {
                wifichooserloader.source = ''
                wifichooserpopup.visible = false
            }
        }
    }

    Item {
        id: helppopup
        visible: false
        width: parent.width * 0.8
        height: parent.height * 0.95
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 10

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12
        }
        Loader {
            anchors.fill: parent
            anchors.margins: 5
            id: helploader
        }
        Connections {
            target: helploader.item

            function onClosed() {
                helploader.source = ''
                helppopup.visible = false
            }
        }
    }

    Item {
        id: downloadmanagerpopup
        visible: false
        width: parent.width * 0.8
        height: parent.height * 0.95
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 10

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12
        }
        Loader {
            id: downloadmanagerloader
            anchors.fill: parent
            anchors.margins: 5
        }

        Connections {
            target: downloadmanagerloader.item
            function onClosed() {
                downloadmanagerpopup.visible = false
                downloadmanagerloader.source = ''
            }
        }
    }

    Item {
        id: settingspopup        
        visible: false
        width: parent.width * 0.8
        height: parent.height * 0.95
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 10

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12
        }

        Loader {
            id: settingsloader
            anchors.margins: 5
            anchors.fill: parent        
        }

        Connections {
            target: settingsloader.item

            function onClosed() {
                adsbListenerModel.ownship = settings.ownshipicao
                settingsloader.source = ''
                settingspopup.visible = false
            }
        }
    }

    Rectangle {
        id: menuEnabled
        property bool enabled: false
        radius: menuItems.buttonRadius
        width: menuItems.buttonSize
        height: menuItems.buttonSize
        color: enabled ? 'yellow' : noChart ? 'gray' : 'transparent'
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 5

        border.color: 'black'
        border.width: enabled ? 2 : 0

        Image {
            source: "images/hamburger.png"
            anchors.centerIn: parent
            scale: menuItems.imageScale
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                parent.enabled = !parent.enabled
            }
            onPressAndHold: {
                // This is to reset the screen brightness to the maximum value
                // in the event that the user lowered it to a point where the
                // buttons become invisible
                brightnessButton.setBrightness(params.maxBrightness)
            }
        }
    }

    Rectangle {
        id: settingsButton
        anchors.top: parent.top
        anchors.left: menuEnabled.right
        anchors.margins: 5
        color: settingspopup.visible ? 'yellow' : 'gray'
        radius: menuItems.buttonRadius
        width: menuItems.buttonSize
        height: menuItems.buttonSize
        border.color: 'black'
        border.width: 2
        visible: menuEnabled.enabled
        Image {
            source: "images/settings.png"
            anchors.centerIn: parent
            scale: menuItems.imageScale
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if( !settingspopup.visible ) {
                    settingspopup.visible = true
                    settings.setRestorePoint()
                    settingsloader.setSource('settings.qml',
                                             {"settings":settings,
                                              "currentOwnship":adsbListenerModel.ownship,
                                              "currentCenter": map.center,
                                              "map": map})
                }
            }
        }
    }

    Rectangle {
        id: chartMenuButton
        anchors.top: parent.top
        anchors.left: settingsButton.right
        anchors.margins: 5
        color: chartDialog.visible ? 'yellow' : 'gray'
        radius: menuItems.buttonRadius
        width: menuItems.buttonSize
        height: menuItems.buttonSize
        border.color: 'black'
        border.width: 2
        visible: menuEnabled.enabled && params.chartURL.startsWith("file:")
        Image {
            source: "images/map.png"
            anchors.centerIn: parent
            scale: menuItems.imageScale
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(!chartDialog.visible) {
                    chartDialog.width = Math.round(window.width * 0.8)
                    chartDialog.height = window.height - 40
                    chartDialog.visible = true
                }
            }
        }
    }

    Rectangle {
        id: downloadManagerButton
        anchors.top: parent.top
        anchors.left: chartMenuButton.right
        anchors.margins: 5        
        radius: menuItems.buttonRadius
        width: menuItems.buttonSize
        height: menuItems.buttonSize
        border.color: 'black'
        border.width: 2
        visible: chartMenuButton.visible
        color: downloadmanagerpopup.visible ? 'yellow' : 'gray'
        Image {
            source: "images/download.png"
            anchors.centerIn: parent
            scale: menuItems.imageScale
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(!downloadmanagerpopup.visible) {
                    downloadmanagerpopup.visible = true
                    downloadmanagerloader.setSource('download/downloadmanagercomponent.qml',
                                                    {"downloadmanager":availableCharts.downloadManager})
                }
            }
        }
    }


    Item {
        id: chartDialog
        visible: false
        width: parent.width * 0.8
        height: parent.height * 0.95
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 10

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: 5
                Text {
                    Layout.margins: 5
                    text: "ADS-B Database Update and FAA Chart Selection"
                    font.bold: true
                    color: 'white'
                }

                Rectangle {
                    border.width: 2
                    border.color: 'white'
                    color: 'darkgray'
                    radius: 5
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.margins: 5

                    ListView {
                        id: chartlistview
                        model: availableCharts
                        spacing: 3
                        anchors.fill: parent
                        anchors.margins: 5                        
                        clip: true

                        delegate: Rectangle {
                            id: listRectItem
                            width: chartlistview.width
                            height: textSize * 5
                            radius: 5
                            border.width: 0

                            color: model.faachart.installed ? (model.faachart.toBeInstalled ? 'darkgray' : 'lightpink')
                                                            : (model.faachart.toBeInstalled ? 'lightgreen' : 'darkgray')
                            MouseArea {
                                anchors.fill: parent
                                onClicked: { control.checkState = control.nextCheckState() }
                            }

                            CheckBox {
                                id: control
                                anchors.left: parent.left
                                anchors.verticalCenter: parent.verticalCenter
                                //anchors.margins: 2
                                checkState: model.faachart.toBeInstalled ? Qt.Checked : Qt.Unchecked
                                nextCheckState: function() {
                                    if (checkState === Qt.Checked)
                                        return Qt.Unchecked
                                    else
                                        return Qt.Checked
                                }
                                indicator: Rectangle {
                                    implicitWidth: 12
                                    implicitHeight: 12
                                    border.color: 'white'
                                    anchors.verticalCenter: parent.verticalCenter

                                    Rectangle {
                                        width: 8
                                        height: 8
                                        anchors.centerIn: parent
                                        color: 'black'
                                        visible: control.checked
                                    }
                                }

                                onCheckStateChanged: {
                                    model.faachart.toBeInstalled = checkState == Qt.Checked
                                    parent.color = model.faachart.installed ? (model.faachart.toBeInstalled ? 'darkgray' : 'lightpink')
                                                                                   : (model.faachart.toBeInstalled ? 'lightgreen' : 'darkgray')
                                }
                            }

                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.left: control.right
                                anchors.margins: 5
                                text: model.faachart.name                                
                            }
                        }
                    }
                }
                RowLayout {
                    Layout.margins: 5
                    Layout.alignment: Qt.AlignRight
                    Layout.preferredHeight: 50

                    Button {
                        text: "Refresh"
                        onClicked: availableCharts.refreshCharts()
                        font.pointSize: buttonSize
                    }
                    Button {
                        text: "Update charts"
                        onClicked: {
                            availableCharts.updateCharts()
                            chartDialog.visible = false
                        }
                        font.pointSize: buttonSize
                    }
                    Button {
                        text: "Cancel"
                        onClicked: {
                            chartDialog.visible = false
                            availableCharts.resetToBeInstalled()
                        }
                        font.pointSize: buttonSize
                    }
                }
            }
        }
    }

    // Wifi Chooser button
    Rectangle {
        id: wifichooserButton
        radius: menuItems.buttonRadius
        width: menuItems.buttonSize
        height: menuItems.buttonSize
        visible: menuEnabled.enabled && params.wifiChooser
        anchors.top: parent.top
        anchors.left: downloadManagerButton.right
        anchors.margins: 5
        color: wifichooserpopup.visible ? 'yellow' : 'gray'
        border.color: 'black'
        border.width: 2

        Image {
            source: "images/wifi.png"
            anchors.centerIn: parent
            scale: menuItems.imageScale
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                wifimanager.scanWiFi()
                wifichooserpopup.visible = true
                wifichooserloader.source = 'wifichooser/WifiChooserComponent.qml'
            }
        }
    }

    // Wifi Chooser button
    Rectangle {
        id: helpButton
        radius: menuItems.buttonRadius
        width: menuItems.buttonSize
        height: menuItems.buttonSize
        visible: menuEnabled.enabled
        anchors.top: parent.top
        anchors.left: wifichooserButton.right
        anchors.margins: 5
        color: helppopup.visible ? 'yellow' : 'gray'
        border.color: 'black'
        border.width: 2

        Image {
            source: "images/help.png"
            anchors.centerIn: parent
            scale: menuItems.imageScale
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                helppopup.visible = true
                helploader.setSource('help.qml', {"downloadmanager":availableCharts.downloadManager,
                                     "listener": adsbListenerModel});
            }
        }
    }

    ComboBox {
        id: cb
        visible: menuEnabled.enabled
        anchors.top: menuEnabled.bottom
        anchors.left: parent.left
        anchors.margins: 5
        anchors.topMargin: 25
        width: parent.width * 0.4
        height: menuItems.buttonSize
        textRole: "name"
        model: availableCharts.installedCharts
        currentIndex: settings.chartIndex
        enabled: !settingspopup.visible // Disable to prevent stale map object if settings window is open
        font.pointSize: textSize

        delegate: ItemDelegate {
            width: window.width * 0.7
            contentItem: Text {
                text: availableCharts.installedCharts.getName(index)                
                color: "white"                
                font.bold: map === null ? false : (availableCharts.installedCharts.coversPos(index, map.center, map.zoomLevel))
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
            highlighted: cb.highlightedIndex === index
        }

        onCurrentIndexChanged: {
            settings.chartIndex = currentIndex
            // If no chart is selected, we put the display in Track Up mode
            if(currentIndex === 0)
                settings.northUp = false
            if(noChart) {
                if( map != null ) {
                    settings.autoCenter = true
                    settings.northUp = false
                }                
            }
            changeHost()
        }
    }

    function changeHost() {
        var item = maploader.item
        // If we are changing the chart, we want to display it using the same position, zoom level, etc. as
        // the chart that is currently being displayed. If not, we want to apply the default settings to
        // initial chart.
        var zoomLevel = item ? item.zoomLevel : settings.zoomLevel
        var center = item ? item.center : settings.defaultCenter
        var bearing = item ? item.bearing : 0
        var tilt = item ? item.tilt : 0

        maploader.setSource("adsbmapcomponent.qml", {
                                "settings" : settings,
                                "charthostname" : params.chartURL + availableCharts.installedCharts.getFolder(settings.chartIndex) + "/",
                                "center" : center,
                                "zoomLevel" : zoomLevel,
                                "nmeasource" : params.gps,
                                "noChart" : noChart,
                                "bearing" : bearing,
                                "tilt" : tilt,
                                "noGL" : params.noGL,
                                "adsbListenerModel" : adsbListenerModel
                            } );
    }

    GridLayout {
        id: menuItems
        visible: menuEnabled.enabled
        z: 2

        property int buttonSize: Screen.pixelDensity * 7 //window.width / 15 //40
        property double imageScale: 0.6 * buttonSize / 40
        property int buttonRadius: 10 * buttonSize / 40

        flow: GridLayout.TopToBottom
        anchors.top: cb.bottom
        anchors.left: parent.left        
        anchors.margins: 5
        rows: Math.floor( ( window.height - y - 15 ) / ( buttonSize + 2 * anchors.margins ) )

        // Create a button to recenter the map on the current position
        Rectangle {
            id: myPositionButton
            property bool enabled: settings.autocenter
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2

            states: [
                State {
                    name: "noautocenter"
                    when: settings.autocenter === 1
                    PropertyChanges { target: myPositionIcon; source: "images/myposition50.png" }
                    PropertyChanges { target: myPositionButton; color: 'gray' }
                },
                State {
                    name: "autocenter"
                    when: settings.autocenter === 2
                    PropertyChanges { target: myPositionIcon; source: "images/myposition50.png" }
                    PropertyChanges { target: myPositionButton; color: "yellow" }
                },
                State {
                    name: "autocenteroffset"
                    when: settings.autocenter === 3
                    PropertyChanges { target: myPositionIcon; source: "images/mypositionoffset.png" }
                    PropertyChanges { target: myPositionButton; color: "yellow" }
                }
            ]
            Image {
                id: myPositionIcon
                source: "images/myposition50.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onClicked: {                   
                    settings.autocenter++
                    if(settings.autocenter === 3) {
                        // Offset autocenter selected from auto center mdoe. Must be in Track Up mode.
                        // If not, skip state and set to No Autocenter mode
                        if(settings.northUp)
                            settings.autocenter = 1
                    } else if(settings.autocenter > 3)
                        settings.autocenter = 1                    
                }
            }
        }

        // Create a button to show/hide reticle
        Rectangle {
            id: rangeCirclesButton            
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize            
            border.color: 'black'
            border.width: 2

            states: [
                State {
                    name: "reticle"
                    when: settings.rangecircles === 1
                    PropertyChanges { target: reticleimg; source: "images/reticle_1.png" }
                    PropertyChanges { target: rangeCirclesButton; color: 'yellow' }
                },
                State {
                    name: "pos"
                    when: settings.rangecircles === 2
                    PropertyChanges { target: reticleimg; source: "images/reticle_2.png" }
                    PropertyChanges { target: rangeCirclesButton; color: 'yellow' }
                },
                State {
                    name: "none"
                    when: settings.rangecircles === 3
                    PropertyChanges { target: reticleimg; source: "images/reticle_1.png" }
                    PropertyChanges { target: rangeCirclesButton; color: 'gray' }
                }
            ]
            Image {
                id: reticleimg
                source: "images/reticle_1.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    settings.rangecircles++
                    if(settings.rangecircles > 3)
                        settings.rangecircles = 1
                }
            }
        }

        // Create a button to set north up
        Rectangle {
            id: northUpButton
            property bool enabled: settings.northUp
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/northup50.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {                    
                    //if((settings.chartIndex === 0) && !settings.northUp) return;
                    settings.northUp = !settings.northUp
                    // See comments in myPositionButton
                    if(settings.northUp && (settings.autocenter === 3))
                        settings.autocenter = 2

                    adsbListenerModel.refreshAll()
                }
            }
        }

        // Create a button to declutter the map
        Rectangle {
            id: declutterButton
            property int declutterLevel: settings.declutter
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/declutter"+(parent.declutterLevel === 0 ? 1 : parent.declutterLevel)+".png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    parent.declutterLevel = (parent.declutterLevel === map.maxDeclutter) ? map.minDeclutter : parent.declutterLevel + 1;
                    declutterButtonClickedAnim.start()
                }
            }
            ColorAnimation {
                id: declutterButtonClickedAnim
                from: "yellow"
                to: "gray"
                duration: 1000
                target: declutterButton
                property: "color"
            }
            onDeclutterLevelChanged: {
                settings.declutter = declutterLevel
                adsbListenerModel.refreshAll()
            }


        }
        // Create a button to show/hide airplane tracks
        Rectangle {
            id: trackButton
            property bool enabled: settings.tracksVisible
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2
            visible: !params.noGL // Disable airplane tracks if GL is not enabled

            Image {
                source: "images/trackonoff.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    settings.tracksVisible =!settings.tracksVisible
                }
            }
        }

        // Create a button to show/hide projected airplane tracks
        Rectangle {
            id: projectedTrackButton
            property bool enabled: settings.projectedTrackMinutes > 0
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2
            visible: !params.noGL // Disable tracks if GL is not enabled

            Image {
                source: "images/projtrack" + settings.projectedTrackMinutes + ".png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(settings.projectedTrackMinutes === 3)
                        settings.projectedTrackMinutes = 0
                    else
                        settings.projectedTrackMinutes++
                }
            }
        }

        // Create a button to filter targets based on altitude range
        Rectangle {
            id: altfilterButton
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: 'gray'
            border.color: 'black'
            border.width: 2            

            Image {
                source: "images/altfilter" + settings.altitudeFilter + ".png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(settings.altitudeFilter === 2)
                        settings.altitudeFilter = 0
                    else
                        settings.altitudeFilter++                    
                    adsbListenerModel.refreshAll()
                }
            }
        }

        // Create a button to adjust display brightness
        Rectangle {
            id: brightnessButton

            function setBrightness(value) {
                if(params.brightness) {
                    var request = new XMLHttpRequest()
                    request.open("PUT", "file:///sys/class/backlight/rpi_backlight/brightness", false)
                    request.send(value);
                    settings.brightness = value
                }
            }

            property bool enabled: false
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2
            visible: params.brightness

            Image {
                source: "images/brightness50.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    parent.enabled = !parent.enabled
                }
            }

            Rectangle {
                anchors.left: brightnessButton.right
                anchors.margins: 5
                anchors.verticalCenter: brightnessButton.verticalCenter
                width: parent.enabled ? 210 : 0
                height: parent.height
                border.color: 'black'
                radius: parent.parent.buttonRadius
                border.width: 2
                color: 'gray'
                visible: parent.enabled

                Behavior on width { NumberAnimation { duration: 250; easing.type: Easing.OutBack } }

                Slider {
                    id: brightnessSlider
                    visible: parent.parent.enabled
                    anchors.centerIn: parent
                    width: 200
                    height: 50
                    from: 0
                    to: params.maxBrightness
                    value: settings.brightness
                    stepSize: 1
                    snapMode: Slider.SnapAlways

                    onValueChanged: {
                        // On the Raspberry Pi, writing to this file will adjuste the screen brightnessButton
                        // The file must be writable by the process for this to work. (chown/chmod the file)
                        // On other platforms, the /sys directory will most likely be readonly so this will simply
                        // fail.
                        // We assume that this file exists and is writeable, so this must be checked when setting
                        // the params.brightness parameter to true
                        if(params.brightness) {
                            brightnessButton.setBrightness(value)
                        }
                    }
                }
            }
        }

        // Create a button to adjust radio gain
        Rectangle {
            id: gainButton

            property bool enabled: false
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2
            visible: adsbListenerModel.gainSettingAvailable

            Image {
                source: parent.enabled ? "qrc:/images/gainhighlighted.png" : "images/gain.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    parent.enabled = !parent.enabled
                }
            }

            Rectangle {                
                anchors.left: gainButton.right
                anchors.leftMargin: 5
                anchors.verticalCenter: gainButton.verticalCenter
                width: parent.enabled ? gainLayout.width + 30 : 0
                height: parent.height
                border.color: 'black'
                radius: parent.parent.buttonRadius
                border.width: 2
                color: 'gray'
                visible: parent.enabled

                Behavior on width { NumberAnimation { duration: 250; easing.type: Easing.OutBack } }

                RowLayout {
                    id: gainLayout
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 3

                    CheckBox {                        
                        id: ownshipGainAdjust
                        Layout.alignment: Qt.AlignVCenter
                        visible: adsbListenerModel.ownship !== ''
                        checked: settings.scanOwnship
                        onToggled: {
                            settings.scanOwnship = checked
                        }
                    }
                    Slider {                        
                        id: gainSlider                                                
                        width: 150
                        Layout.alignment: Qt.AlignVCenter
                        from: 0
                        to: adsbListenerModel.gains.length - 1
                        value: settings.normalGainIndex
                        stepSize: 1
                        snapMode: Slider.SnapAlways
                        visible: !ownshipGainAdjust.checked || (adsbListenerModel.ownship === '')

                        onMoved: {
                            settings.normalGainIndex = value
                        }
                    }
                    Text {
                        id: ownshipGainValue                                                
                        Layout.preferredWidth: 40
                        Layout.alignment: Qt.AlignVCenter
                        font.pointSize: textSize
                        color: 'white'
                        text: adsbListenerModel.ownshipGainIndex == 0 ? "AUTO" : adsbListenerModel.ownshipGain / 10.0
                        visible:gainRangeSlider.visible
                    }
                    RangeSlider {
                        id: gainRangeSlider
                        Layout.alignment: Qt.AlignVCenter
                        width: 150
                        height: 50
                        from: 0
                        to: adsbListenerModel.gains.length - 1
                        first.value: settings.ownshipGainIndex
                        second.value: settings.normalGainIndex
                        stepSize: 1
                        snapMode: Slider.SnapAlways
                        visible: !gainSlider.visible

                        first.onMoved: {
                            settings.ownshipGainIndex = first.value
                        }
                        second.onMoved: {
                            settings.normalGainIndex = second.value
                        }
                    }
                    Text {
                        id: normalGainValue
                        font.pointSize: textSize
                        Layout.alignment: Qt.AlignVCenter
                        Layout.preferredWidth: 40
                        text: adsbListenerModel.normalGainIndex == 0 ? "AUTO" : adsbListenerModel.normalGain / 10.0
                        color: 'white'
                    }
                }
            }
        }

        // Add button to enable LoggingCategory
        Rectangle {
            id: debugButton
            visible: params.showDebugButton
            property bool enabled: adsbListenerModel.debugLevel > 0
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: enabled ? 'yellow' : 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/debug50.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    adsbListenerModel.debugLevel = (adsbListenerModel.debugLevel == 0 ? 1 : 0)
                }
                onPressAndHold: {
                    // Holding the button will clear the current debug log
                    adsbListenerModel.clearDebugFile()
                }

            }
        }

    }

    RowLayout {        
        visible: menuItems.visible
        anchors.top: menuItems.top
        anchors.left: menuItems.right
        anchors.leftMargin: 5
        z: 1 // So font size buttons show under any popup item like gain sliders in the manu

        Rectangle {            
            id: fontsmallerButton
            radius: menuItems.buttonRadius
            width: menuItems.buttonSize
            height: menuItems.buttonSize
            color: 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/fontsmaller.png"
                anchors.centerIn: parent
                scale: menuItems.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(settings.fontsize > 5) settings.fontsize-=3
                    fontsmallerAnim.start()
                    adsbListenerModel.refreshAll()
                }
            }
            ColorAnimation {
                id: fontsmallerAnim
                from: "yellow"
                to: "gray"
                duration: 500
                target: fontsmallerButton
                property: "color"
            }
        }
        Rectangle {            
            id: fontbiggerButton
            radius: menuItems.buttonRadius
            width: menuItems.buttonSize
            height: menuItems.buttonSize
            color: 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/fontbigger.png"
                anchors.centerIn: parent
                scale: menuItems.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(settings.fontsize < 50) settings.fontsize+=3
                    fontbiggerAnim.start()
                    adsbListenerModel.refreshAll()
                }
            }
            ColorAnimation {
                id: fontbiggerAnim
                from: "yellow"
                to: "gray"
                duration: 500
                target: fontbiggerButton
                property: "color"
            }
        }
    }

    Rectangle {
        anchors.margins: 5
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        radius: 5
        border.color: 'black'
        color: 'lightgray'
        height: targetTxt.height + 8
        width: targetTxt.width + 12
        visible: menuEnabled.enabled

        Text {
            id: targetTxt
            property int targetsTracked: adsbListenerModel.targetsTracked
            property int gain: adsbListenerModel.gain / 10.0
            anchors.centerIn: parent
            color: 'black'
            text: 'Targets tracked: ' + targetsTracked
                  + " My altitude: " + map.myAltitude + " (" + map.altitudeSource + ")" + " Gain: " + (gain == -10 ? "Auto" : gain)
        }
    }

    GridLayout {
        columns: 2
        property int buttonSize: menuItems.buttonSize
        property double imageScale: menuItems.imageScale
        property int buttonRadius: menuItems.buttonRadius

        flow: GridLayout.TopToBottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 5
        anchors.bottomMargin: 50
        height: 80
        rows: ( parent.height - y ) / ( buttonSize + 2 * anchors.margins )
        visible: params.zoomButtons

        // Zoom in button
        Rectangle {
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/zoomin.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    map.zoomLevel += 1
                }
            }
        }
        // Zoom out button
        Rectangle {
            radius: parent.buttonRadius
            width: parent.buttonSize
            height: parent.buttonSize
            color: 'gray'
            border.color: 'black'
            border.width: 2

            Image {
                source: "images/zoomout.png"
                anchors.centerIn: parent
                scale: parent.parent.imageScale
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    map.zoomLevel -= 1
                }
            }
        }
    }

    InputPanel {
        id: inputPanel
        y: Qt.inputMethod.visible ? parent.height - inputPanel.height : parent.height
        z: 100
        visible: Qt.inputMethod.visible
        anchors.left: parent.left
        anchors.right: parent.right                

    }        
}
