import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12
import DownloadManager 1.0
import DownloadItem 1.0
import DownloadAndUnzip 1.0
import ProgramUpdater 1.0
import AdsbListenerModel 1.0

Item {
    id: helpItem
    property DownloadManager downloadmanager
    property AdsbListenerModel listener

    anchors.fill: parent
    signal closed()

    Component.onCompleted: {
        programUpdater.checkUpdateAvailable()
    }

    Item {
        id: rawdatapopup
        visible: false
        width: parent.width
        height: parent.height
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        z: 11

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12
        }

        Loader {
            id: rawadsbloader
            anchors.fill: parent
            anchors.margins: 5
        }

        Connections {
            target: rawadsbloader.item
            onClosed: {
                rawadsbloader.source = ''
                rawadsbloader.visible = false
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        Text {
            Layout.margins: 5
            text: "About this program"
            font.pointSize: textSize
            font.bold: true
            color: 'white'
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5
            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5

            Image {
                source: "images/splash.png"
                smooth: true
                anchors.margins: 5
                width: parent.width/1.5
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                fillMode: Image.PreserveAspectFit
            }

            Text {
                anchors.bottom: parent.bottom
                font.pointSize: textSize
                color: 'white'
                anchors.bottomMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: "ADS-B Traffic Display (build " + buildnumber + ")\n(C) 2021 Charles Vaillancourt"
            }
        }

        RowLayout {
            id: helpButtons
            Layout.margins: 5
            Layout.alignment: Qt.AlignRight
            Layout.preferredHeight: 50
            Button {
                text: "Raw data"
                font.pointSize: buttonSize
                onClicked: {
                    helppopup.visible = true
                    helploader.setSource('rawadsbdata.qml', {"listener": listener});
                }
            }
            Button {
                text: "Update DB"
                enabled: !openskyupdater.running
                onClicked: {
                    openskyupdater.update(downloadmanager)
                }
                font.pointSize: buttonSize
            }
            Button {
                id: progUpdateButton
                text: programUpdater.status === DownloadItem.NotStarted ? (programUpdater.updateAvailable ? (helpItem.width < 400 ? 'Upd. program' : 'Update program') : "Up to date" )
                                                                        : programUpdater.status === DownloadItem.Finished ? 'Restart'
                                                                                                                          : programUpdater.status === DownloadItem.Failed ? 'Failed - Retry'
                                                                                                                                                                          : 'Updating...'
                font.pointSize: buttonSize
                enabled: ( ( programUpdater.status === DownloadItem.NotStarted && programUpdater.updateAvailable )
                          || programUpdater.status === DownloadItem.Failed
                          || programUpdater.status === DownloadItem.Finished )

                onClicked: {
                    if(programUpdater.canBeStarted)
                        programUpdater.updateProgram(downloadmanager)
                    if(programUpdater.status == DownloadItem.Finished) {                        
                        programUpdater.reboot()
                    }
                }
            }
            Button {
                text: "Close"
                font.pointSize: buttonSize
                onClicked: {
                    helpItem.closed()
                }
            }
        }
    }
}
