#include "adsblistenermodel.h"

AdsbListenerModel::AdsbListenerModel()
{
    m_listener = new AdsbListener(this);

    setHostname("");
    setPort(0);
    setActive(false);
    setOwnshipString("");
    setProjectedTrackMinutes(1);

    init();
}

AdsbListenerModel::AdsbListenerModel(QObject *parent) : QAbstractListModel(parent)
{
    AdsbListenerModel();
}

AdsbListenerModel::AdsbListenerModel(AdsbListener *listener, QObject *parent) : QAbstractListModel(parent)
{
    m_listener = listener;    

    init();
}


void AdsbListenerModel::init()
{
    connect(m_listener, &AdsbListener::targetAdded, this, &AdsbListenerModel::addPoint);
    connect(m_listener, &AdsbListener::targetLost,  this, &AdsbListenerModel::removePoint);
    connect(m_listener, &AdsbListener::ownshipUpdated, this, &AdsbListenerModel::ownshipChanged);
    connect(m_listener, &AdsbListener::ownshipAltitudeChanged, this, &AdsbListenerModel::ownshipAltitudeFtChanged);
    connect(m_listener, &AdsbListener::ownshipAvailable, this, &AdsbListenerModel::ownshipAvailable);
    connect(m_listener, &AdsbListener::ownshipGainChanged, this, &AdsbListenerModel::ownshipGainChanged);
    connect(m_listener, &AdsbListener::gainChanged, this, &AdsbListenerModel::gainChanged);
    connect(m_listener, &AdsbListener::normalGainChanged, this, &AdsbListenerModel::normalGainChanged);
    connect(m_listener, &AdsbListener::ownshipGainAdjustChanged, this, &AdsbListenerModel::ownshipGainAdjustChanged);
    connect(m_listener, &AdsbListener::dataReceived, this, &AdsbListenerModel::dataReceived);
    connect(m_listener, &AdsbListener::debugLevelChanged, this, &AdsbListenerModel::debugLevelChanged);
    connect(m_listener, &AdsbListener::activeChanged, this, &AdsbListenerModel::activeChanged);    
}

int AdsbListenerModel::getMaxGainIndex() const
{
    return m_listener->getGains().size();
}

int AdsbListenerModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return m_list.count();
}

QVariant AdsbListenerModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid() || index.row() < 0 || index.row() >= rowCount())
        return QVariant();

    AdsbTarget* target = m_list[index.row()];

    switch(role) {
        case positionRole:
            return QVariant::fromValue(target->coords());
        case dataRole:
            return QVariant::fromValue(target);
        case trackRole: {
            // When the track is requested, it is most likely by a MapPolyline object
            // The documentation for MapPolyline specifies that the list of points
            // cannot be dynamic and must be reloaded completely for each point
            // addition. That is why we copy the list in a new object each time
            // it is reqested instead of just returning the list stored in this object.
            QVariantList track_list;
            QListIterator<QGeoCoordinate> i(target->getTrack());

            while(i.hasNext())
                track_list << QVariant::fromValue(i.next());

            return track_list;
        }
        case projectedTrackRole: {
            QVariantList track_list;
            if(target->coordsValid()) {
            track_list << QVariant::fromValue(target->coords())
                       << QVariant::fromValue(target->getCoordsInXsec(60 * getProjectedTrackMinutes()));
            }
            return track_list;
        }
    }

    return QVariant();
}

void AdsbListenerModel::addPoint(AdsbTarget *g)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_list << g;
    endInsertRows();

    connect(g, &AdsbTarget::adsbChanged, this, &AdsbListenerModel::adsbChanged);
    emit targetsChanged();
}

void AdsbListenerModel::removePoint(AdsbTarget *g)
{
    int i = m_list.indexOf(g);
    beginRemoveRows(QModelIndex(), i, i);
    m_list.removeAt(i);
    endRemoveRows();

    disconnect(g, &AdsbTarget::adsbChanged, this, &AdsbListenerModel::adsbChanged);
    emit targetsChanged();
}

QHash<int, QByteArray> AdsbListenerModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[positionRole] = "position";
    roles[dataRole] = "adsbdata";
    roles[trackRole] = "track";
    roles[projectedTrackRole] = "projectedTrack";
    return roles;
}

QString AdsbListenerModel::hostname() const
{
    return m_listener->hostname();
}

void AdsbListenerModel::setHostname(QString hostname)
{
    m_listener->setHostname(hostname);
    // If both port and hostname are set, activate the listener
    if(port() != 0)
        setActive(true);
}

uint AdsbListenerModel::port() const
{
    return m_listener->port();
}

void AdsbListenerModel::setPort(uint port)
{
    m_listener->setPort(port);
    // If both port and hostname are set, activate the listener
    if(hostname() != "" )
        setActive(true);
}

bool AdsbListenerModel::active() const
{
    return m_listener->active();
}

void AdsbListenerModel::setActive(bool active)
{
    m_listener->setActive(active);
}

uint AdsbListenerModel::targetsTracked() const
{
    return rowCount();
}

bool AdsbListenerModel::ownshipAltitudeAvailable() const
{
    return m_listener->ownshipAltitudeAvailable();
}

qint32 AdsbListenerModel::ownshipAltitudeFt() const
{
    return m_listener->getOwnshipAltitudeFt();
}

void AdsbListenerModel::refreshAll()
{
    beginResetModel();
    endResetModel();    
}

bool AdsbListenerModel::gainSettingAvailable() const
{
    return m_listener->gainSettingAvailable();
}

QList<int> AdsbListenerModel::getGains() const
{
    return m_listener->getGains();
}

int AdsbListenerModel::getActualGain() const
{
    return m_listener->getActualGain();
}

int AdsbListenerModel::getNormalGain() const
{
    return m_listener->getNormalGain();
}

int AdsbListenerModel::getOwnshipGain() const
{
    return m_listener->getOwnshipGain();
}

void AdsbListenerModel::setGain(int g)
{
    m_listener->setGain(g);
}

int AdsbListenerModel::getGainIndex() const
{
    return m_listener->getGainIndex();
}

void AdsbListenerModel::setGainIndex(int gainIndex)
{
    m_listener->setGainIndex(gainIndex);
}

int AdsbListenerModel::getOwnshipGainIndex() const
{
    return m_listener->getOwnshipGainIndex();
}

void AdsbListenerModel::setOwnshipGainIndex(int gainIndex)
{
    m_listener->setOwnshipGainIndex(gainIndex);
}

int AdsbListenerModel::getNormalGainIndex() const
{
    return m_listener->getNormalGainIndex();
}

void AdsbListenerModel::setNormalGainIndex(int gainIndex)
{
    m_listener->setNormalGainIndex(gainIndex);
}

void AdsbListenerModel::setOwnshipGainAdjust(bool b)
{
    m_listener->setOwnshipGainUpdate(b);
}

bool AdsbListenerModel::getOwnshipGainAdjust() const
{
    return m_listener->getOwnshipGainUpdate();
}

bool AdsbListenerModel::isRadioAvailable() const
{
    return m_listener->isRadioAvailable();
}

int AdsbListenerModel::getDebugLevel() const
{
    return m_listener->getDebugLevel();
}

void AdsbListenerModel::setDebugLevel(int debugLevel)
{
    m_listener->setDebugLevel(debugLevel);
}

bool AdsbListenerModel::getListenerActive() const
{
    return m_listener->active();
}

QString AdsbListenerModel::getLabel(QString address, QGeoCoordinate position, int altitudeFt, int declutter) const
{
    QString txt = "";

    AdsbTarget *a = m_listener->getTarget(address);
    if(a == nullptr) return txt;

    // Create data label for target
    QString altitudeDelta = "";
    QString altitudeDeltaBrackets;
    if( ownshipAltitudeAvailable() ) {
        if(a->altitudeFt() > ownshipAltitudeFt())
            altitudeDelta = "+";
        altitudeDelta += QString::number(a->altitudeFt() - ownshipAltitudeFt());
        altitudeDeltaBrackets = "(" + altitudeDelta + ")";
    } else {
        if(a->altitudeFt() > altitudeFt)
            altitudeDelta = "+";

        altitudeDelta += QString::number(a->altitudeFt() - altitudeFt, 'f', 0) + '*';
        altitudeDeltaBrackets = "(" + altitudeDelta + ")";
    }

    QString id;
    if(a->callsign() != "")
        id = a->callsign();
    else if(a->getAircraft()->m_registration == "")
        id = a->address();
    else
        id = a->getAircraft()->m_registration;

    switch(declutter) {
        case 1: // Show only altitude difference or, if own altitude unknown, target altitude
            txt = QString::number(position.distanceTo(a->coords())/1852, 'f', 1) + "nm " + altitudeDelta;

            break;
        case 2: // Show all data except aircraft information
            txt =  id + " " + QString::number(a->altitudeFt()) + altitudeDeltaBrackets + "\n"
                       + QString::number(position.distanceTo(a->coords())/1852, 'f', 1) + "nm "
                       + (a->verticalRate() > 0 ? "+" : "")
                       + QString::number(a->verticalRate() / 0.3048, 'f', 0) + "fpm " + QString::number(a->groundSpeed()) +"kts";
            break;
        case 3: // Show all data
            // Get rid of too much information in brackets in aircraft model field
            QString s = a->getAircraft()->m_model;
            s = s.left(s.indexOf('(') == -1 ? s.length() : s.indexOf('(') - 1);

            txt = id + " " + QString::number(a->altitudeFt()) + altitudeDeltaBrackets + "\n"
                       + QString::number(position.distanceTo(a->coords())/1852, 'f', 1) + "nm "
                       + (a->verticalRate() > 0 ? "+" : "")
                       + QString::number(a->verticalRate() / 0.3048, 'f', 0) + "fpm " + QString::number(a->groundSpeed()) +"kts"
                       + (a->getAircraft()->dataAvailable() ? ( "\n" + (a->callsign() == "" ? a->getAircraft()->m_manufacturericao : a->getAircraft()->m_registration ) + " " + a->getAircraft()->m_model ) : "");

            break;
        }
    return txt;
}

void AdsbListenerModel::clearDebugFile()
{
    m_listener->clearDebugFile();
}

void AdsbListenerModel::adsbChanged(AdsbTarget *g)
{
    QModelIndex i = index(m_list.indexOf(g));
    emit dataChanged(i, i);
}

QString AdsbListenerModel::ownshipString() {
    return m_listener->ownshipString();
}

void AdsbListenerModel::setOwnshipString(QString o) {
    m_listener->setOwnshipString(o);
    emit normalGainChanged();
    emit ownshipGainChanged();
    emit gainChanged();
}

void AdsbListenerModel::setProjectedTrackMinutes(int min) {
    m_projectedTrackMinutes = min;
    emit projectedTrackMinutesChanged();
}

int AdsbListenerModel::getProjectedTrackMinutes() const {
    return m_projectedTrackMinutes;
}
