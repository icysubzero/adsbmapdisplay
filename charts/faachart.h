#ifndef FAACHART_H
#define FAACHART_H

#include <QObject>
#include <QProcess>
#include <QTemporaryDir>
#include <QGeoCoordinate>
#include "../download/downloaditem.h"
#include "../download/downloadandunzipitem.h"

class FAAChart: public DownloadItem
{
    Q_OBJECT

    Q_PROPERTY(QString  url MEMBER m_url NOTIFY changed);    
    Q_PROPERTY(QString  chartType READ chartTypeString NOTIFY changed);
    Q_PROPERTY(bool     installed READ installed NOTIFY changed);
    Q_PROPERTY(bool     availableOnline READ availableOnline NOTIFY changed);
    Q_PROPERTY(bool     toBeInstalled READ toBeInstalled WRITE setToBeInstalled NOTIFY changed);

public:
    typedef enum {VFR, IFR} chartType_t;

    FAAChart(QObject *parent = nullptr);
    FAAChart(const FAAChart&, QObject *parent = nullptr);
    FAAChart(QString name, QString url, chartType_t chartType, QString m_chartFolder, QObject *parent = nullptr);
    void operator=(const FAAChart &);

    QString url() const;
    void setUrl(const QString &url);

    QString chartFolder() const;
    void setChartFolder(const QString &chartFolder);

    QString chartTypeString() const;
    chartType_t chartType() const;
    void setChartType(const chartType_t &chartType);

    bool installed() const;
    void setInstalled(bool installed);

    bool availableOnline() const;        

    bool toBeInstalled() const;
    void setToBeInstalled(bool toBeInstalled);

    Q_INVOKABLE bool coversPosition(QGeoCoordinate&, float zoomLevel = 8) const;

    // From DownloadItem
    Q_INVOKABLE virtual void startDownload();
    Q_INVOKABLE virtual void pauseDownload();
    Q_INVOKABLE virtual void resumeDownload();
    Q_INVOKABLE virtual void cancelDownload();

    QString chartsFolder() const;
    void setChartsFolder(const QString &chartsFolder);

signals:
    void changed();         // Signal that chart properties changed
    void chartDeleted(FAAChart *);    // Signal that the chart was deleted
    void chartInstalled(FAAChart *);  // Signal that the chart was recently installed

private slots:
    void updateProgress();
private:
    QString m_url, m_chartFolder, m_chartsFolder;
    bool m_installed, m_toBeInstalled;
    chartType_t m_chartType;
    int outputLines = 0;
    QProcess proc;

    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);
};

#endif // FAACHART_H
