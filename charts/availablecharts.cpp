#include "availablecharts.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QDir>

AvailableCharts::AvailableCharts(QString path) : QAbstractListModel()
{        
    manager = new QNetworkAccessManager(this);
    setURL(path);
    downloadChartList();

    m_downloadManager = new DownloadManager(this);
}

int AvailableCharts::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_charts.size();
}

QVariant AvailableCharts::data(const QModelIndex &index, int role) const
{    
    FAAChart *c = m_charts.values().at(index.row());
    switch(role) {
        case nameRole:          return c->getName();
        case urlRole:           return c->url();
        case installedRole:     return c->installed();
        case faaChartRole:      return QVariant::fromValue(c);
        case folderRole:        return c->chartFolder();
    }
    return QVariant("");
}

QHash<int, QByteArray> AvailableCharts::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[nameRole]         = "name";
    roles[urlRole]          = "url";
    roles[installedRole]    = "installed";
    roles[faaChartRole]     = "faachart";
    roles[folderRole]       = "folder";

    return roles;
}

void AvailableCharts::resetToBeInstalled()
{
    beginResetModel();
    QList<FAAChart*> l = m_charts.values();
    QList<FAAChart*>::iterator i;

    for(i = l.begin(); i != l.end(); ++i)
        (*i)->setToBeInstalled((*i)->installed());
    endResetModel();
}

void AvailableCharts::updateCharts()
{
    QList<FAAChart*> l = m_charts.values();
    QList<FAAChart*>::iterator i;

    for(i = l.begin(); i != l.end(); ++i) {
        (*i)->setChartsFolder(QUrl(getURL()).path());
        m_downloadManager->addDownloadItem(*i);
    }
}

void AvailableCharts::refreshCharts() {

    beginResetModel();
    m_charts.clear();
    endResetModel();

    downloadChartList();
}

void AvailableCharts::downloadChartList() {
    findInstalledCharts();
    downloadVFRChartList();
    downloadIFRChartList();
}

void AvailableCharts::downloadVFRChartList()
{
    QUrl qurl(VFR_URL);
    QNetworkRequest request(qurl);
    request.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);

    QNetworkReply* reply = manager->get(request);

    connect(reply, SIGNAL(finished()), this, SLOT(vfrReplyFinished()));

    if ( reply->error() != QNetworkReply::NoError ) {
        qWarning() <<"ErrorNo: "<< reply->error() << "for url: " << reply->url().toString();
        qDebug() << "Request failed, " << reply->errorString();
        qDebug() << "Headers:"<<  reply->rawHeaderList()<< "content:" << reply->readAll();
        return;
    }
}

void AvailableCharts::downloadIFRChartList()
{
    QUrl qurl(IFR_URL);
    QNetworkRequest request(qurl);
    request.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);

    QNetworkReply* reply = manager->get(request);

    connect(reply, SIGNAL(finished()), this, SLOT(ifrReplyFinished()));

    if ( reply->error() != QNetworkReply::NoError ) {
        qWarning() <<"ErrorNo: "<< reply->error() << "for url: " << reply->url().toString();
        qDebug() << "Request failed, " << reply->errorString();
        qDebug() << "Headers:"<<  reply->rawHeaderList()<< "content:" << reply->readAll();
        return;
    }
}

void AvailableCharts::findInstalledCharts()
{
    if(getURL().length() > 0) {
        QUrl url(getURL());
        QString path = url.path();
        QDir dir(path);

        QStringList charts = dir.entryList(QDir::Dirs);

        beginResetModel();

        for(QStringList::iterator i = charts.begin(); i != charts.end(); ++i) {
            QString name = *i;
            FAAChart::chartType_t t;
            if(name.startsWith(QString("IFR"))) {
                t = FAAChart::IFR;
            } else if(name.startsWith(QString("VFR"))) {
                t = FAAChart::VFR;
            } else {
                continue;
            }            
            QString folder = name;
            name.replace("_", " ");
            FAAChart *c = new FAAChart(name, QString(""), t, folder);
            c->setInstalled(true);
            c->setChartsFolder(path);
            connect(c, SIGNAL(chartDeleted(FAAChart *)), this, SLOT(chartDeleted(FAAChart *)));
            m_charts[name] = c;
        }
        updateInstalledCharts();
        endResetModel();

        emit installedChartsChanged();
    }
}

DownloadManager *AvailableCharts::getDownloadManager() const
{
    return m_downloadManager;
}

QString AvailableCharts::getURL() const
{
    return m_url;
}

void AvailableCharts::setURL(const QString &url)
{
    m_url = url;    
}

void AvailableCharts::updateInstalledCharts()
{
    QList<FAAChart *> s, l;
    QList<FAAChart *>::iterator i;

    s = m_charts.values();

    for(i = s.begin(); i != s.end(); ++i)
        if((*i)->installed()) l << *i;

    m_installedCharts.updateChartlist(l);
}

InstalledCharts *AvailableCharts::getInstalledChartList()
{
    return &m_installedCharts;
}

void AvailableCharts::debugCharts()
{
    qDebug() << "------------------------------------------------------------------------------------------------";
    for(int i = 0; i < m_charts.values().size(); i++ ) {
        FAAChart *c = m_charts.values().at(i);
        qDebug() << c->getName() << c->url() << c->chartType() << c->installed() << c->chartFolder();
    }
}

void AvailableCharts::vfrReplyFinished()
{
    QNetworkReply *reply = (QNetworkReply*)QObject::sender();
    QByteArray array = reply->readAll();
    QString s = QString(array);

    // Find chart URLs on page   
    QRegularExpression e("https://aeronav.faa.gov/visual/.{10}/sectional-files/.{1,30}.zip(?=\"?>GEO-TIFF)");
    // Extract chart name from URL
    //QRegularExpression ex("[^/\\&\?]+.(?=\\_.{2,3}.zip)");
    QRegularExpression ex("(?<=https://aeronav.faa.gov/visual/.{10}/sectional-files/).{1,30}(?=\\.zip)");
    // Extract chart version from URL
    //QRegularExpression ver("(?<=_)\\d{2,3}(?=.zip)");
    QRegularExpression dex("(?<=https://aeronav.faa.gov/visual/).{10}");

    QRegularExpressionMatchIterator i = e.globalMatch(s);

    beginResetModel();

    while(i.hasNext()) {
        QRegularExpressionMatch match = i.next();

        QString url = match.captured();        

        QString name = "VFR " + ex.match(url).captured() + " " + dex.match(url).captured();
        name = name.replace(QString("_"), QString(" "));

        // Check if chart is already installed
        if(m_charts.contains(name)) {           
            m_charts[name]->setUrl(url);
            m_charts[name]->setChartType(FAAChart::VFR);
            //m_charts[name]->setInstalled(true);
        } else {
            QString folder = name;
            FAAChart *c = new FAAChart(name, url, FAAChart::VFR, folder.replace(" ", "_"));
            connect(c, SIGNAL(chartDeleted(FAAChart *)), this, SLOT(chartDeleted(FAAChart *)));
            connect(c, SIGNAL(chartInstalled(FAAChart *)), this, SLOT(chartInstalled(FAAChart *)));
            m_charts[name] = c;
        }
    }

    endResetModel();
}

void AvailableCharts::ifrReplyFinished()
{
    QNetworkReply *reply = (QNetworkReply*)QObject::sender();
    QByteArray array = reply->readAll();
    QString s = QString(array);

    QRegularExpression e("https://aeronav.faa.gov/enroute/.{10}/.{1,10}.zip(?=\">GEO-TIFF)");
    QRegularExpressionMatchIterator i = e.globalMatch(s);

    QRegularExpression ex("(?<=https://aeronav.faa.gov/enroute/.{10}/).{1,15}(?=\\.zip)");
    QRegularExpression dex("(?<=https://aeronav.faa.gov/enroute/).{10}");

    beginResetModel();

    while(i.hasNext()) {
        QRegularExpressionMatch match = i.next();


        QString url = match.captured();

        // Exclude Caribbean charts, they have multiple charts in the same zip file
        if(!url.contains("CB")) {

            QString name = "IFR " + ex.match(url).captured() + " - " + dex.match(url).captured();
            name.replace(QString("enr_l"), QString("Low "));
            name.replace(QString("enr_h"), QString("High "));
            name.replace(QString("enr_akl"), QString("Alaska Low "));
            name.replace(QString("enr_akh"), QString("Alaska High "));
            name.replace(QString("enr_a"), QString("Area Chart "));
            name.replace(QString("enr_p"), QString("Hawaii-Pacific "));
            name = name.replace(QString("_"), QString(" "));

            // Check if chart is already installed
            if(m_charts.contains(name)) {
                m_charts[name]->setUrl(url);
                m_charts[name]->setChartType(FAAChart::IFR);
                //m_charts[name]->setInstalled(true);
            } else {
                QString folder = name;
                FAAChart *c = new FAAChart(name, url, FAAChart::IFR, folder.replace(" ", "_"));
                connect(c, SIGNAL(chartDeleted(FAAChart *)), this, SLOT(chartDeleted(FAAChart *)));
                connect(c, SIGNAL(chartInstalled(FAAChart *)), this, SLOT(chartInstalled(FAAChart *)));
                m_charts[name] = c;
            }
        }
    }

    endResetModel();
}

void AvailableCharts::chartDeleted(FAAChart *c)
{
    Q_UNUSED(c)

    updateInstalledCharts();
    layoutChanged();
}

void AvailableCharts::chartInstalled(FAAChart *c)
{
    Q_UNUSED(c)

    updateInstalledCharts();
    layoutChanged();
}
