#include "installedcharts.h"

InstalledCharts::InstalledCharts()
{
    no_chart = new FAAChart("No chart", "", FAAChart::VFR, "no_chart");
}

InstalledCharts::~InstalledCharts()
{
    delete no_chart;
}

QString InstalledCharts::getFolder(int index)
{
    if(index == 0)
        return no_chart->chartFolder();
    else
        return m_installedCharts.at(index - 1)->chartFolder();
}

QString InstalledCharts::getName(int index)
{
    if(index == 0)
        return no_chart->getName();
    else
        return m_installedCharts.at(index - 1)->getName();
}

bool InstalledCharts::coversPos(int index, QGeoCoordinate coord, float zoomLevel)
{
    if(index == 0) return true; // Always return true if No Chart

    return m_installedCharts.at(index - 1)->coversPosition(coord, zoomLevel);
}

void InstalledCharts::listChanged()
{
    beginResetModel();
    endResetModel();
}

int InstalledCharts::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    // We return one more than the array size to include a "No chart" option in the list of installed charts
    return m_installedCharts.size() + 1;
}

QVariant InstalledCharts::data(const QModelIndex &index, int role) const
{
    FAAChart *c;

    if(index.row() == 0)
        c = no_chart;
    else
        c = m_installedCharts.at(index.row() - 1);

    switch(role) {
        case nameRole:          return c->getName();
        case urlRole:           return c->url();
        case installedRole:     return c->installed();
        case faaChartRole:      return QVariant::fromValue(c);
        case folderRole:        return c->chartFolder();
    }
    return QVariant("");
}

QHash<int, QByteArray> InstalledCharts::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[nameRole]         = "name";
    roles[urlRole]          = "url";
    roles[installedRole]    = "installed";
    roles[faaChartRole]     = "faachart";
    roles[folderRole]       = "folder";

    return roles;
}

void InstalledCharts::updateChartlist(QList<FAAChart *> &c)
{
    beginResetModel();
    m_installedCharts = c;
    endResetModel();
}

