#ifndef AVAILABLECHARTS_H
#define AVAILABLECHARTS_H

#include <QAbstractListModel>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "faachart.h"
#include "installedcharts.h"
#include "download/downloadmanager.h"

#define VFR_URL "https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/vfr/"
#define IFR_URL "https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/ifr/"
#define USER_AGENT "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36"

class AvailableCharts : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(InstalledCharts* installedCharts READ getInstalledChartList NOTIFY installedChartsChanged)
    Q_PROPERTY(DownloadManager* downloadManager READ getDownloadManager CONSTANT)

public:    
    AvailableCharts(QString s = "");

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE void resetToBeInstalled();
    Q_INVOKABLE void updateCharts();
    Q_INVOKABLE void refreshCharts();

    InstalledCharts *getInstalledChartList();

    QString getURL() const;
    void setURL(const QString &url);

    Q_INVOKABLE DownloadManager *getDownloadManager() const;


protected:
    void downloadChartList();
    void downloadVFRChartList();
    void downloadIFRChartList();
    void findInstalledCharts();

private:
    QMap<QString, FAAChart*> m_charts;    
    InstalledCharts m_installedCharts;
    QNetworkAccessManager *manager;
    QString m_url;
    DownloadManager *m_downloadManager;

    void updateInstalledCharts();
    void debugCharts();

signals:
    void installedChartsChanged();

private slots:
    void vfrReplyFinished();
    void ifrReplyFinished();
    void chartDeleted(FAAChart *);
    void chartInstalled(FAAChart *);
};

#endif // AVAILABLECHARTS_H
