#ifndef INSTALLEDCHARTS_H
#define INSTALLEDCHARTS_H

#include <QAbstractListModel>
#include "faachart.h"



// TODO: Make local to class
enum InstalledChartsRoles {nameRole    = Qt::UserRole + 1,
                          urlRole,
                          installedRole,
                          faaChartRole,
                          folderRole
                         };

class InstalledCharts : public QAbstractListModel
{
    Q_OBJECT

public:

    InstalledCharts();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
    void updateChartlist(QList<FAAChart *>&);

    ~InstalledCharts();

    Q_INVOKABLE QString getFolder(int index);
    Q_INVOKABLE QString getName(int index);
    Q_INVOKABLE bool coversPos(int index, QGeoCoordinate, float zoomLevel);

public slots:
    void listChanged();

private:
    FAAChart *no_chart;
    QList<FAAChart *> m_installedCharts;
};

#endif // INSTALLEDCHARTS_H
