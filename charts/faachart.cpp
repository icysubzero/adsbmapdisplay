#include "faachart.h"
#include <QDebug>
#include <QProcess>
#include <QDir>
#include <QUrl>
#include <QCoreApplication>
#include "settings.h"
#include <math.h>
#include "sys/stat.h"

FAAChart::FAAChart(QObject *parent) : DownloadItem(parent)
{
    m_url = "";
    setInstalled(false);
}

FAAChart::FAAChart(const FAAChart &c, QObject *parent) : DownloadItem(parent)
{    
    this->operator=(c);
}

FAAChart::FAAChart(QString name, QString url, chartType_t type, QString path, QObject *parent) : FAAChart(parent)
{    
    setName(name);
    setInstalled(false);
    m_url = url;
    m_chartFolder = path;
    m_chartType = type;    
}

void FAAChart::operator=(const FAAChart &c)
{
    setName(c.getName());
    m_url = c.url();
    m_chartFolder = c.chartFolder();
    m_chartType = c.chartType();
    m_installed = c.installed();
    m_toBeInstalled = c.toBeInstalled();

    emit changed();
}

QString FAAChart::url() const
{
    return m_url;
}

void FAAChart::setUrl(const QString &url)
{
    m_url = url;
    emit changed();
}

QString FAAChart::chartFolder() const
{
    return m_chartFolder;
}

void FAAChart::setChartFolder(const QString &chartFolder)
{
    m_chartFolder = chartFolder;
    emit changed();
}

QString FAAChart::chartTypeString() const
{
    return m_chartType == IFR ? "IFR" : "VFR";
}

FAAChart::chartType_t FAAChart::chartType() const
{
    return m_chartType;
}

void FAAChart::setChartType(const chartType_t &chartType)
{
    m_chartType = chartType;
    emit changed();
}

bool FAAChart::installed() const
{
    return m_installed;
}

void FAAChart::setInstalled(bool installed)
{
    m_installed = installed;
    m_toBeInstalled = installed;

    emit changed();    
}

bool FAAChart::availableOnline() const
{
    return m_url != "";
}

bool FAAChart::toBeInstalled() const
{
    return m_toBeInstalled;
}

void FAAChart::setToBeInstalled(bool toBeInstalled)
{
    m_toBeInstalled = toBeInstalled;
}

bool FAAChart::coversPosition(QGeoCoordinate &coord, float zoomLevel ) const
{
    if(!installed()) return false;
    int tileX = (floor((coord.longitude() + 180.0) / 360.0 * (1 << (int)zoomLevel)));
    double latrad = coord.latitude() * M_PI / 180.0;
    int tileY = (floor((1.0 - asinh(tan(latrad)) / M_PI) / 2.0 * (1 << (int)zoomLevel)));

    QString tileFilename = chartsFolder() + chartFolder() + "/" + QString::number((int)zoomLevel)
            + "/" + QString::number(tileX) + "/" + QString::number(tileY) + ".png";

    struct stat buffer;
    QByteArray ba = tileFilename.toLocal8Bit();
    const char *c_str2 = ba.data();

    return stat(c_str2, &buffer) == 0;
}

void FAAChart::startDownload()
{
    if(!getCanBeStarted()) return;

    QUrl qurl(chartsFolder());

    if(qurl.scheme().startsWith("http", Qt::CaseInsensitive)) {
        qDebug() << "Cannot change charts when a non-local chart server is used.";
        return;
    }
    QString path = qurl.path();
    QString fullPath = path + (path.endsWith("/") ? "" : "/") + chartFolder();

    if(this->installed() && !this->toBeInstalled()) {
        // The chart is installed but is flagged to be removed

        setCanBePaused(false);
        setCanBeStopped(false);
        setMessage("Removing chart");
        setStatus(DownloadItem::Downloading);
        setProgress(0);

        QDir dir(fullPath);
        dir.removeRecursively();
        setInstalled(false);
        setToBeInstalled(false);
        emit chartDeleted(this);

        setMessage("Chart removed");
        setStatus(DownloadItem::Finished);
        setProgress(100);
        emit completed(getStatus());

    } else if(!this->installed() && this->toBeInstalled() && getCanBeStarted()) {
        // The chart is not installed but is flagged to be installed

        setStatus(DownloadItem::Downloading);

        setCanBeStopped(true);
        setCanBePaused(false);
        setCanBeStarted(false);
        setMessage("Downloading and unzipping");

        QObject::connect(&proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &FAAChart::processFinished );
        QObject::connect(&proc, &QProcess::readyReadStandardOutput, this, &FAAChart::updateProgress);
        QObject::connect(QCoreApplication::instance(), &QCoreApplication::aboutToQuit, this, &FAAChart::cancelDownload);

        outputLines = 0;

        QStringList cmd;
        cmd << this->url() << this->chartFolder() << chartsFolder() << QString::number(Settings::getInstance()->getZoomLevel(chartType()));

        if(chartType() == VFR) cmd << "-expand" << "rgba";

        proc.start("processCharts.sh", cmd);

    } else {
        setMessage("Nothing to do");
        setCanBePaused(false);        
        setCanBeStopped(false);
        setStatus(DownloadItem::Finished);
        emit completed(getStatus());
    }
}

void FAAChart::updateProgress() {    

    if(getStatus() == DownloadItem::Canceled) return;

    outputLines++;
    if(outputLines == 4 ) {
        setStatus(Processing);
        setMessage("Tiling");
    }    
    int progress = (int)((outputLines > 1 ? 10.0 : 0) + outputLines / 99.0 * 100);
    setProgress( progress > 99 ? 99 : progress );
}

void FAAChart::processFinished( int exitCode, QProcess::ExitStatus exitStatus ) {

    if(getStatus() == DownloadItem::Canceled) return;

     if((exitStatus == QProcess::NormalExit) && (exitCode == 0 ) ) {
         setInstalled(true);
         setToBeInstalled(true);
         emit chartInstalled(this);

         setStatus(DownloadItem::Finished);
         setMessage("Installation complete");
         setProgress(100);
         setCanBeStarted(true);
         emit completed(getStatus());
     } else {
         qDebug() << "Download failed: " << exitCode << " NormalExit: " << exitStatus;
         setMessage("Error occurred");
         setStatus(DownloadItem::Failed);
         setCanBeStarted(true);
     }
}

void FAAChart::pauseDownload()
{
}

void FAAChart::resumeDownload()
{
}

void FAAChart::cancelDownload()
{
    if(getCanBeStopped()) {        
        proc.terminate();
        setCanBePaused(false);
        setCanBeStopped(false);
        setCanBeStarted(true);
        setMessage("Canceled");
        setProgress(0);
        setStatus(DownloadItem::Canceled);
        emit completed(getStatus());
    }
}

QString FAAChart::chartsFolder() const
{
    return m_chartsFolder;
}

void FAAChart::setChartsFolder(const QString &chartsFolder)
{
    m_chartsFolder = chartsFolder;
}
