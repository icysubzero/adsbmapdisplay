import QtQuick 2.0
import QtQuick.Controls 2.12
import AircraftData 1.0
import AdsbMessage 1.0
import AdsbListenerModel 1.0

Item {    
    id: aircraftData
    property AircraftData aircraft    
    property AdsbMessage adsbdata
    property bool enabled: false
    property int maxHeight: 300    
    property AdsbListenerModel listener
/*
    property var dataElements : [
        { label: adsbdata.aircraft.exists ? 'Registration: ' : 'ICAO Address: ',
            value: adsbdata.aircraft.exists ? adsbdata.aircraft.registration : adsbdata.address,
            visible: true },
        { label: 'Model',
            value: adsbdata.aircraft.model,
            visible: adsbdata.aircraft.exists && adsbdata.aircraft.model !== '' },
        { label: 'Callsign: ',
            value: adsbdata.callsign,
            visible: true },
        { label: 'Latitude: ',
            value: isNaN(adsbdata.coords.latitude) ? 'N/A' : adsbdata.coords.latitude,
            visible: true },
        { label: 'Longitude: ',
            value: isNaN(adsbdata.coords.longitude) ? 'N/A' : adsbdata.coords.longitude,
            visible: true },
        { label: 'Altitude: ',
            value: isNaN(adsbdata.coords.altitude) ? 'N/A' : adsbdata.altitudeFt + ' ft.',
            visible: true },
        { label: 'Vertical rate: ',
            value: adsbdata.verticalRateFt + ' fpm',
            visible: true },
        { label: 'Ground speed: ',
            value: isNaN(adsbdata.groundSpeed) ? 'N/A' : adsbdata.groundSpeed + ' kts',
            visible: true },
        { label: 'Ground track: ',
            value: isNaN(adsbdata.groundTrack) ? 'N/A' : adsbdata.groundTrack +'°',
            visible: true },
        { label: 'Manufacturer: ',
            value: adsbdata.aircraft.manufacturericao,
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.manufacturericao !== '') },
        { label: 'Model: ',
            value: adsbdata.aircraft.model + ' (' + adsbdata.aircraft.typecode + ')',
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.model !== '') },
        { label: 'Category: ',
            value: adsbdata.aircraft.category,
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.category !== '')
                     && !adsbdata.aircraft.category.startsWith('No ADS-B') },
        { label: 'S/N: ',
            value: adsbdata.aircraft.serialnumber,
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.serialnumber !== '') },
        { label: 'Engines: ',
            value: adsbdata.aircraft.engines,
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.engines !== '') },
        { label: 'Owner: ',
            value: adsbdata.aircraft.owner,
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.owner !== '') },
        { label: 'Operator: ',
            value: adsbdata.aircraft.oper,
            visible: adsbdata.aircraft.exists && (adsbdata.aircraft.oper !== '') },
        { label: 'Control field: ',
            value: adsbdata.controlText,
            visible: true },
        { label: 'Last message: ',
            value: adsbdata.lastMessage,
            visible: true },
    ];
    */

    z: 4

    ListModel {
        id: aircraftDataModel
    }

    anchors.verticalCenter: parent.verticalCenter
    anchors.right: parent.right
    anchors.margins: 5

    width: enabled ? parent.width * 0.4 : 0
    height: enabled ? maxHeight - 10 : 0
    visible: width > 0

    Behavior on width { NumberAnimation { duration: 250; easing.type: Easing.InCubic } }
    Behavior on height{ NumberAnimation { duration: 250; easing.type: Easing.InCubic } }

    Rectangle {
        anchors.fill: parent
        color: 'black'
        radius: 20
        opacity: 0.5
    }


    ListView {
        id: listView
        anchors.top: closeImage.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        model: aircraftDataModel
        clip: true
        delegate: aircraftDelegate

        ScrollBar.vertical: ScrollBar { policy: ScrollBar.AlwaysOn}

        Component {
            id: aircraftDelegate
            Rectangle {
                width: 290
                height: label.height + data.height + 5
                border.color: 'white'
                border.width: 0
                color: 'transparent'

                Text { id: label; color: 'white'; font.pointSize: textSize - 2; text: name; anchors.left: parent.left; anchors.top: parent.top; width: parent.width}
                Text { id: data; color: 'white'; font.pointSize: textSize; text: value; anchors.left: parent.left; anchors.top: label.bottom; anchors.topMargin: 0; wrapMode: Text.Wrap; width: parent.width }
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                parent.parent.enabled = false
            }
        }
    }

    Button {
        id: closeImage
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.rightMargin: 5        

        anchors.margins: 10

        contentItem: Text {
            text: "Close"
            font.pointSize: buttonSize
            color: 'white'
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle {
                color: 'transparent'
                border.color: 'white'
                border.width: 2
                radius: 5
        }
        onClicked: {
            parent.enabled = false
        }
    }

    Button {        
        anchors.right: closeImage.left
        anchors.margins: 10
        anchors.top: parent.top

        contentItem: Text {
            text: "Set as ownship"
            font.pointSize: buttonSize
            color: 'white'
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle {
                color: 'transparent'
                border.color: 'white'
                border.width: 2
                radius: 5
        }

        onClicked: {
            aircraft.visible = false
            listener.setOwnshipString(aircraft.icao24)
            parent.enabled = false
        }
    }

    onAircraftChanged: {        
        updateData()
    }

    function updateData() {
        aircraftDataModel.clear()
        if(aircraft == null) {
            enabled = false
        } else {
            if(aircraft.icao24 !== "") aircraftDataModel.append({name:"ICAO Address", value:aircraft.icao24});
            if(aircraft.registration !== "") aircraftDataModel.append({name:"Registration", value:aircraft.registration});
            if(aircraft.model !== "") aircraftDataModel.append({name:"Model", value:aircraft.model});
            if(aircraft.typecode !== "") aircraftDataModel.append({name:"Type code", value:aircraft.typecode});
            if(aircraft.oper !== "") aircraftDataModel.append({name:"Operator", value:aircraft.oper});
            if(aircraft.opercallsign !== "") aircraftDataModel.append({name:"Operator Callsign", value:aircraft.opercallsign});
            if(aircraft.opericao !== "") aircraftDataModel.append({name:"Operator ICAO", value:aircraft.opericao});
            if(aircraft.operiata !== "") aircraftDataModel.append({name:"Operator IATA", value:aircraft.operiata});
            if(aircraft.owner !== "") aircraftDataModel.append({name:"Owner", value:aircraft.owner});
            if(aircraft.manufacturericao !== "") aircraftDataModel.append({name:"Manufacturer ICAO", value:aircraft.manufacturericao});
            if(aircraft.manufacturername !== "") aircraftDataModel.append({name:"Manufacturer Name", value:aircraft.manufacturername});
            if(aircraft.icaoaircrafttype !== "") aircraftDataModel.append({name:"ICAO Aircraft Type", value:aircraft.icaoaircrafttype});
            if(aircraft.category !== "") aircraftDataModel.append({name:"Category", value:aircraft.category});

            if(aircraft.engines !== "") aircraftDataModel.append({name:"Engines", value:aircraft.engines});
            if(aircraft.serialnumber !== "") aircraftDataModel.append({name:"Serial Number", value:aircraft.serialnumber});
            if(aircraft.linenumber !== "") aircraftDataModel.append({name:"Line Number", value:aircraft.linenumber});
            if(aircraft.testreg !== "") aircraftDataModel.append({name:"Test registration", value:aircraft.testreg});
            if(aircraft.registered !== "") aircraftDataModel.append({name:"Registered", value:aircraft.registered});
            if(aircraft.reguntil !== "") aircraftDataModel.append({name:"Registered Until", value:aircraft.reguntil});
            if(aircraft.status !== "") aircraftDataModel.append({name:"Status", value:aircraft.status});
            if(aircraft.built !== "") aircraftDataModel.append({name:"Date Built", value:aircraft.built});
            if(aircraft.firstflightdate !== "") aircraftDataModel.append({name:"First Flight Date", value:aircraft.firstflightdate});
            if(aircraft.seatconfiguration !== "") aircraftDataModel.append({name:"Seat Configuration", value:aircraft.seatconfiguration});
            if(aircraft.modes !== "") aircraftDataModel.append({name:"Modes", value:aircraft.modes});
            if(aircraft.adsb !== "") aircraftDataModel.append({name:"ADS-B", value:aircraft.adsb});
            if(aircraft.acars !== "") aircraftDataModel.append({name:"ACARS", value:aircraft.acars});
            if(aircraft.notes !== "") aircraftDataModel.append({name:"Notes", value:aircraft.notes});

            aircraftDataModel.append({name:"Altitude", value:adsbdata.altitudeFt+" ft."});
            aircraftDataModel.append({name:"Ground speed", value:adsbdata.groundSpeed+""});
            aircraftDataModel.append({name:"Ground track", value:adsbdata.groundTrack+""});
        }
    }
}
