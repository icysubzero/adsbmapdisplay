#ifndef ADSBTARGET_H
#define ADSBTARGET_H

#include <QObject>
#include <QDateTime>
#include <QtPositioning/QGeoCoordinate>
#include <QTimer>
#include "aircraftdata/aircraftdata.h"

/* Holds an ADS-B message in SBS format
 *
 * Field 1 : always MSG
 * Field 2 : SBS message type, integer, 1-8
 * Field 3 : always 111
 * Field 4 : always 11111
 * Field 5 : ICAO address, 6-digit hex value e.g. C0607E
 * Field 6 : always 111111
 * Field 7 : message reception date AAAA/MM/DD
 * Field 8 : message reception time HH:MM:SS.mmm
 * Field 9 : message output date AAAA/MM/DD
 * Field 10: message output time HH:MM:SS.mmm
 * Field 11: callsign (e.g. ROU1559)
 * Field 12: altitude in feet (e.g. 12150)
 * Field 13: ground speed (e.g. 281)
 * Field 14: ground track (heading) (e.g. 305)
 * Field 15: latitude DD.mmmmm, e.g. 45.65142
 * Field 16: longitude (-)DD.mmmmm, e.g. -74.07076
 * Field 17: vertical rate in ft/min
 * Field 18: squawk 4-digit octal value e.g. 1234
 * Field 19: squawk changing alert field -1, 0, or nothing
 * Field 20: squawk emergency flag, -1 if squawking 7500, 7600 or 7700, 0 otherwise (or nothing)
 * Field 21: squawk ident flag, -1 if active, 0 or nothing otherwise
 * Field 22: on the ground flag, -1 if active, 0 or nothing otherwise
 *
 * Example: MSG,3,111,11111,39850B,111111,2020/01/07,19:24:58.775,2020/01/07,19:24:58.762,,11100,,,45.45035,-73.32550,,,,,,0
 *
 */

/*
 * NOTE: This object stores altitude and vertical rates in meters for consistency with
 *       the rest of the Qt system, but helper functions can return these in feet
 */

/*
 * NOTE: We've added an additional 23rd field at the end of the SBS message to show the control field:
 *
 * 0 *  "ADS-B ES/NT device with ICAO 24-bit address",
 * 1 *  "ADS-B ES/NT device with other address",
 * 2 *  "Fine format TIS-B",
 * 3 *  "Coarse format TIS-B",
 * 4 *  "TIS-B management message",
 * 5 *  "TIS-B relay of ADS-B message with other address",
 * 6 *  "ADS-B rebroadcast using DF-17 message format",
 * 7 *  "Reserved"
*/

#define ADSBMESSAGE_TIMEOUT 60 // Time in seconds before a target times out due to lack of refresh

class AircraftData;

class AdsbTarget : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QString  messageType     READ messageType    WRITE setMessageType    NOTIFY adsbChanged);
    Q_PROPERTY(quint8   msgType         READ msgType        WRITE setMsgType        NOTIFY adsbChanged);
    Q_PROPERTY(QString  address         READ address        WRITE setAddress        NOTIFY adsbChanged);
    Q_PROPERTY(QTime    recvTime        READ recvTime       WRITE setRecvTime       NOTIFY adsbChanged);
    Q_PROPERTY(QDate    recvDate        READ recvDate       WRITE setRecvDate       NOTIFY adsbChanged);
    Q_PROPERTY(QTime    msgTime         READ msgTime        WRITE setMsgTime        NOTIFY adsbChanged);
    Q_PROPERTY(QDate    msgDate         READ msgDate        WRITE setMsgDate        NOTIFY adsbChanged);
    Q_PROPERTY(QString  callsign        READ callsign       WRITE setCallsign       NOTIFY adsbChanged);
    Q_PROPERTY(quint16  groundSpeed     READ groundSpeed    WRITE setGroundSpeed    NOTIFY adsbChanged);
    Q_PROPERTY(quint16  groundTrack     READ groundTrack    WRITE setGroundTrack    NOTIFY adsbChanged);
    Q_PROPERTY(double   verticalRate    READ verticalRate   WRITE setVerticalRate   NOTIFY adsbChanged);
    Q_PROPERTY(qint32   verticalRateFt  READ verticalRateFt NOTIFY adsbChanged);
    Q_PROPERTY(qint32   altitudeFt      READ altitudeFt     NOTIFY adsbChanged);
    Q_PROPERTY(QString  squawk          READ squawk         WRITE setSquawk         NOTIFY adsbChanged);
    Q_PROPERTY(bool     squawkChg       READ squawkChg      WRITE setSquawkChg      NOTIFY adsbChanged);
    Q_PROPERTY(bool     squawkAlert     READ squawkAlert    WRITE setSquawkAlert    NOTIFY adsbChanged);
    Q_PROPERTY(bool     squawkIdent     READ squawkIdent    WRITE setSquawkIdent    NOTIFY adsbChanged);
    Q_PROPERTY(bool     onGround        READ onGround       WRITE setOnGround       NOTIFY adsbChanged);

    Q_PROPERTY(bool     coordsValid           READ coordsValid                      NOTIFY adsbChanged);
    Q_PROPERTY(QGeoCoordinate coords          READ coords         WRITE setCoords   NOTIFY positionChanged);
    Q_PROPERTY(QGeoCoordinate coordsIn60sec   READ coordsIn60sec                    NOTIFY adsbChanged)
    Q_PROPERTY(QGeoCoordinate coordsIn120sec  READ coordsIn120sec                   NOTIFY adsbChanged)
    Q_PROPERTY(QGeoCoordinate coordsIn180sec  READ coordsIn180sec                   NOTIFY adsbChanged)
    Q_PROPERTY(QGeoCoordinate estimatedCoords READ estimatedCoords                  NOTIFY adsbChanged)

    Q_PROPERTY(QString  lastMessage     READ getLastUpdate NOTIFY adsbChanged)
    Q_PROPERTY(quint8   controlField    READ getControl     WRITE setControl        NOTIFY adsbChanged);
    Q_PROPERTY(QString  controlText     READ getControlText NOTIFY adsbChanged);

    Q_PROPERTY(quint32  updateCount     READ updateCount NOTIFY adsbChanged);
    Q_PROPERTY(uint     timeSinceUpdate READ timeSinceUpdate ); // TODO: should add notification every second or so
    Q_PROPERTY(bool     timedOut        READ timedOut       NOTIFY stale );

    Q_PROPERTY(QString  update          READ getLastUpdate  WRITE setUpdate);
    Q_PROPERTY(bool     visible         READ visible        WRITE setVisible        NOTIFY adsbChanged);
    Q_PROPERTY(bool     isOwnship       READ isOwnship      WRITE setIsOwnship      NOTIFY adsbChanged);


    Q_PROPERTY(AircraftData *aircraft READ getAircraft CONSTANT);

public:
    AdsbTarget();
    AdsbTarget(QString receivedString);    
    ~AdsbTarget();

    static QString extractAddress(QString receivedString);
    void setUpdate(QString receivedString);
    QString getLastUpdate();

    QString messageType() const;
    void setMessageType(const QString &value);
    QString address() const;
    void setAddress(const QString &value);
    QString callsign() const;
    void setCallsign(const QString &value);
    QString squawk() const;
    void setSquawk(const QString &value);
    quint8 msgType() const;
    void setMsgType(const quint8 &value);
    bool squawkChg() const;
    void setSquawkChg(bool value);
    bool squawkAlert() const;
    void setSquawkAlert(bool value);
    bool squawkIdent() const;
    void setSquawkIdent(bool value);
    bool onGround() const;
    void setOnGround(bool value);
    QTime recvTime() const;
    void setRecvTime(const QTime &value);
    QTime msgTime() const;
    void setMsgTime(const QTime &value);
    QDate recvDate() const;
    void setRecvDate(const QDate &value);
    QDate msgDate() const;
    void setMsgDate(const QDate &value);
    QGeoCoordinate coords() const;
    void setCoords(const QGeoCoordinate &value);
    quint16 groundSpeed() const;
    void setGroundSpeed(const quint16 &value);
    quint16 groundTrack() const;
    void setGroundTrack(const quint16 &value);
    double verticalRate() const;
    qint32 verticalRateFt() const;
    qint32 altitudeFt() const;
    void setVerticalRate(const double &value);
    bool visible() const;
    void setVisible(bool visible);
    bool isOwnship() const;
    void setIsOwnship(bool isOwnship);
    QString getControlText() const;

    bool coordsValid() const;
    Q_INVOKABLE QGeoCoordinate getCoordsInXsec(const int) const;
    QGeoCoordinate coordsIn60sec() const;
    QGeoCoordinate coordsIn120sec() const;
    QGeoCoordinate coordsIn180sec() const;
    QGeoCoordinate estimatedCoords();
    QList<QGeoCoordinate>&getTrack();
    quint32 updateCount() const;
    void setUpdateCount(const quint32 &updateCount);
    uint timeSinceUpdate() const;

    bool timedOut();

signals:
    void adsbChanged(AdsbTarget*);
    void positionChanged(AdsbTarget*);
    void altitudeUpdated();
    void stale(AdsbTarget*);   

public slots:
    void timeout();

private:
    QString     receivedString;
    QTimer      *timer;

    QString     m_messageType, m_address, m_callsign, m_squawk;
    quint8      m_msgType;
    bool        m_squawkChg, m_squawkAlert, m_squawkIdent, m_onGround;
    QTime       m_recvTime, m_msgTime;
    QDate       m_recvDate, m_msgDate;
    QGeoCoordinate m_coords;
    QList<QGeoCoordinate> m_track;
    quint16     m_groundSpeed, m_groundTrack;
    double      m_verticalRate;
    quint32     m_updateCount;
    bool        m_timedOut, m_visible, m_isOwnship;
    quint8      m_control;

public:
    AircraftData* getAircraft() const;
    AircraftData *m_aircraft;
    quint8 getControl() const;
    void setControl(const quint8 &control);
};

#endif // ADSBTARGET_H
