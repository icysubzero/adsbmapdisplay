# QML Modules required
QT += quick positioning network sql virtualkeyboard quickcontrols2 widgets

# Libraries required for dump1090 support
LIBS += -lrtlsdr -lpthread -lm

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        adsbcommandlineargs.cpp \
        adsblistener.cpp \
        adsblistenermodel.cpp \
        adsbtarget.cpp \
        aircraftdata/aircraftdata.cpp \
        aircraftdata/openskyupdater.cpp \
        charts/availablecharts.cpp \
        charts/faachart.cpp \
        charts/installedcharts.cpp \
        download/downloadandunzipitem.cpp \
        download/downloaditem.cpp \
        download/downloadmanager.cpp \        
        download/programupdater.cpp \
        dump1090/anet.c \
        dump1090/dump1090.c \
        dump1090/interactive.c \
        dump1090/mode_ac.c \
        dump1090/mode_s.c \
        dump1090/net_io.c \
        main.cpp \
        settings.cpp \
        wifichooser/rpiwifimanager.cpp

RESOURCES += qml.qrc

# Let's generate a build number in version.h
version.target = version.h
version.commands = bash $$PWD/utils/generateVersion.sh $$PWD

QMAKE_EXTRA_TARGETS += version
PRE_TARGETDEPS = version.h

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment
target.path = /home/pi/bin
faa.files = $$PWD/faa/processCharts.sh $$PWD/faa/gdal2tilesG256.py
faa.path = /home/pi/bin
aircraftdb.files = $$PWD/aircraftdata/update.sql $$PWD/aircraftdata/createAircraftDB.sh $$PWD/aircraftdata/updateAircraftDB.sh
aircraftdb.path = /home/pi/bin

INSTALLS += target faa aircraftdb

copydata.commands = $(COPY) $${faa.files} $${aircraftdb.files} $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

HEADERS += \
    adsbcommandlineargs.h \
    adsblistener.h \
    adsblistenermodel.h \
    adsbtarget.h \
    aircraftdata/aircraftdata.h \
    aircraftdata/openskyupdater.h \
    charts/availablecharts.h \
    charts/faachart.h \
    charts/installedcharts.h \
    download/downloadandunzipitem.h \
    download/downloaditem.h \
    download/downloadmanager.h \
    download/programupdater.h \
    dump1090/anet.h \
    dump1090/coaa.h \
    dump1090/dump1090.h \
    dump1090/pthreads/pthread.h \
    dump1090/pthreads/sched.h \
    dump1090/pthreads/semaphore.h \
    dump1090/rtlsdr/rtl-sdr.h \
    dump1090/rtlsdr/rtl-sdr_export.h \
    settings.h \
    version.h \
    wifichooser/rpiwifimanager.h

DISTFILES += \
    AircraftDataList.qml \
    ChartDownloadDialog.qml \
    README.md \
    adsbmap.qml \
    adsbmapcomponent.qml \
    aircraftdata/README.aircraft \
    aircraftdata/createAircraftDB.sh \
    aircraftdata/update.sql \
    aircraftdata/updateAircraftDB.sh \
    download/downloadmanagercomponent.qml \
    download/testdownloadcomponent.qml \
    dump1090/datareplay.log \
    dump1090/dump1090.sh \
    dump1090/replay.sh \
    faa/README.charts \
    faa/gdal2tilesG256.py \
    faa/gdal2tilesG512.py \
    faa/processCharts.sh \
    faa/updateCharts.sh \
    help.qml \
    rawadsbdata.qml \
    settings.qml \
    settings_copy.qml \
    utils/1.log \
    utils/2.log \
    utils/createImage.sh \
    utils/datareplay.log \
    utils/datareplay_small.log \
    utils/generateVersion.sh \
    utils/pishrink.sh \
    utils/replay.sh \
    utils/ubuntu20.04qt5.15_setup/buildpi.sh \
    utils/ubuntu20.04qt5.15_setup/hostsetup.sh \
    utils/ubuntu20.04qt5.15_setup/pisetup.sh \
    utils/ubuntu20.04qt5.15_setup/setuppi.sh \
    utils/updateWebFrom.sh \
    wifichooser/WifiChooserComponent.qml \
    wifichooser/main.qml

SUBDIRS += \
    wifichooser/wifichooser.pro
