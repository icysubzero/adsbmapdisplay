# ADS-B Map Display (adsbreader)
## What is adsbreader?
**adsbreader** is a small utility that displays a map with nearby airplanes on a Linux device. It can be compiled for any Qt-supported target hardware.

## How does it work?
Modern airplanes continuously transmit their position and basic flight data on the 1090MHz frequency using a Mode S transponder with Extended Squitter (a.k.a. 1090ES). These signals are used by other airplanes to detect nearby traffic and by air traffic controllers to manage traffic. The signals are unencrypted and so can also be picked up by anyone using simple radio equipment. Software can then be used to decode the signals and this utility can display the resulting aircraft positions on a map.

Signals are read from a USB RTL-SDR (a Software Defined Radio that uses a DVB-T TV tuner dongle based on the RTL2832U chip) by the **dump1090** utility which outputs a CSV stream of aircraft data transmissions to a TCP socket to which the **adsbmap** program listens. **adsbmap** aggregates data from this stream into coherent airplane targets and uses Qt's OpenStreetMap (osm) plugin to display the targets on a map using an airplane icon and a label with basic flight data.

## What do I need to make it work?
You will need the following equipment:

- A USB RTL-SDR receiver like the [Nooelec NESDR Nano 2](https://www.nooelec.com/store/sdr/sdr-receivers/nesdr/nesdr-nano2.html)
- An [antenna](https://www.nooelec.com/store/1090mhz-ads-b-antenna-5dbi-sma.html) suitable for 1090MHz signals
- A [shielded cable](https://www.nooelec.com/store/male-mcx-to-male-sma-pigtail-rg316-0-5-length.html) to connect the antenna to the receiver
- A GPS receiver for displaying your position in relation to other aircraft that is exposed through gpsd (or you will need to change the code to use a different position source)

You will also need a computer to run this program and a way to compile the code in this repository:

- A computer or Raspberry Pi with a USB port for which you can compile Qt programs
- A C++ compiler or cross-compiler with the [Qt](https://www.qt.io/) libraries with which to compile this program
- Although the source code for the **dump1090** utility is embedded into the source of this program and can be launched with the -r option, it is probably a good idea to also get the **dump1090** program in order to test and troubleshoot the SDR receiver (there are several versions out there -- [this one works well](https://github.com/MalcolmRobb/dump1090) )

## How do I configure my computer to run this program?
You need to execute the following steps:

 1. Install the required libraries (they are listed in this [document](https://docs.google.com/document/d/1Zn4HFdG-yx8QZiq-ZpnpQef8TB8wAS8OEIbhcqAUZUM/edit?usp=sharing) but not in a single place)
 2. Get and compile **dump1090**
 3. Get and compile **adsbmap** (this program)
 4. Launch **dump1090** as a daemon. A typical command to launch dump1090 is:

                        nohup ./dump1090 --net --quiet &

 5. Launch **adsbmap**

## I want to use a Raspberry Pi as the target but build from a Linux PC cross-compiler. How do I do that?
Refer to this [document](https://docs.google.com/document/d/1Zn4HFdG-yx8QZiq-ZpnpQef8TB8wAS8OEIbhcqAUZUM/edit?usp=sharing)

## Can this program be used offline?
Yes! Aircraft data is received over the air and maps can be stored locally on a portable device like a Raspberry Pi so the program can work perfectly fine without a network connection. This means that you could carry a portable device running this program in an airplane to monitor surrounding traffic.
One source of offline charts is the [FAA Digital Products web site](https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/) where VFR sectionals, TACs and IFR charts can be obtained for free. These charts then need to be tiled in TMS format using a utility like `gdal2tiles.py` and installed on the local device's file system and/or served up by a local HTTP server like `lighttpd`. A script to automatically download the latest version of sectional and IFR charts, unzip them, tile them, stage them and copy them to a chart server is included in the faa folder

## What options can I specify with this program?
Usage: bin/adsbreader [options]
ADS-B Map Display v1.0

Options:
  -h, --help                     Displays this help.
  -v, --version                  Displays version information.
  -a, --adsbhostname <hostname>  Connect to ADS-B server <hostname> (default:
                                 localhost)
  -p, --adsbport <port>          Connect to ADS-B server on port <port>
                                 (default: 30003)
  -r, --radio                    Start RTL SDR ADS-B radio listener within this
                                 program (default: disabled). With this option
                                 enabled, adsbhostname and adsbport should be
                                 left to their default values so the radio used
                                 by this program will be used as the source of
                                 aircraft data.
  -c, --chartsurl <chartsURL>    Get charts from <chartsURL> (default:
                                 http://localhost/)
  -i, --chartindex <chartIndex>  Default chart index in chartlist starting at
                                 0, with -1 for no chart (default: -1)
  -n, --norangecircles           Do not display range circles around own
                                 position
  -d, --database <database>      Path to sqlite aircraft database file
                                 (default: aircraft.db)
  -g, --gpsurl <gps>             URL to GPS data (default:
                                 gpsd://localhost:2947)
  -o, --ownship <hexaddress>     ICAO hex address of our own plane (e.g.
                                 C01D8A) to suppress displaying it (default:
                                 none)
  -b, --brightness               Disable display brightness control (default:
                                 enabled if running on Raspberry Pi and display
                                 brightness control file is writeable)
  -z, --zoombuttons              Display buttons for zooming in and out on the
                                 chart. Useful when using a mouse without a
                                 wheel or a touchscreen without multi-touch
                                 capability (default: disabled)
  -f, --nogl                     Specify that this device does not support
                                 OpenGL in order to suppress features that
                                 require it, such as airplane tracks
