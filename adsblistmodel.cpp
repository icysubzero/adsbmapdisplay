#include "adsblistmodel.h"

AdsbListModel::AdsbListModel(QObject *parent) : QAbstractListModel(parent)
{
    m_listener = new AdsbListener("pi3",30003);

}

int AdsbListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_geolist.count();
}

void AdsbListModel::addAdsbMessage(const AdsbMessage msg)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_geolist.append(msg);
    endInsertRows();
}

void AdsbListModel::removeAdsbMessage(const AdsbMessage msg)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_geolist.removeAll(msg);
    endInsertRows();
}

QVariant AdsbListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_geolist.count())
        return QVariant();

    if(role == AdsbListModel::positionRole) {
        AdsbMessage m = m_geolist[index.row()];
        return QVariant::fromValue(m.coords());
    }

    if(role == AdsbListModel::dataRole)
        return QVariant::fromValue(m_geolist[index.row()]);

    return QVariant();
}

QHash<int, QByteArray> AdsbListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[positionRole] = "position";
    roles[dataRole] = "adsbdata";
    return roles;
}





