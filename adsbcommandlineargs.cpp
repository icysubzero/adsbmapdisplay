#include "adsbcommandlineargs.h"

AdsbCommandLineArgs::AdsbCommandLineArgs(QObject *parent) : QObject(parent)
{

}

void AdsbCommandLineArgs::setHostname(QString h)
{
    m_hostname = h;
}

void AdsbCommandLineArgs::setPort(uint p)
{
    m_port = p;
}

void AdsbCommandLineArgs::setChartURL(QString c)
{
    m_chartURL = c;
    // Add trailing "/" to URL
    if(!c.endsWith("/")) c += "/";
}

QString AdsbCommandLineArgs::hostname()
{
    return m_hostname;
}

uint AdsbCommandLineArgs::port()
{
    return m_port;
}

QString AdsbCommandLineArgs::chartURL()
{
    return m_chartURL;
}

int AdsbCommandLineArgs::chartIndex()
{
    return m_chartindex;
}

void AdsbCommandLineArgs::setChartIndex(int index)
{
    m_chartindex = index;
}

int AdsbCommandLineArgs::rangeCircles()
{
    return m_range;
}

void AdsbCommandLineArgs::setRangeCircles(int r)
{
    m_range = r;
}

QString AdsbCommandLineArgs::database()
{
    return m_database;
}

void AdsbCommandLineArgs::setDatabase(QString m)
{
    m_database = m;
}

QString AdsbCommandLineArgs::ownship()
{
    return m_ownship;
}

void AdsbCommandLineArgs::setOwnship(QString ship)
{
    m_ownship = ship;
}

QString AdsbCommandLineArgs::gps() const
{
    return m_gps;
}

void AdsbCommandLineArgs::setGps(const QString &gps)
{
    m_gps = gps;
}

void AdsbCommandLineArgs::setBrightness(bool b)
{
    m_brightness = b;
}

bool AdsbCommandLineArgs::brightness()
{
    return m_brightness;
}

void AdsbCommandLineArgs::setMaxBrightness(uint m)
{
    m_maxBrightness = m;
}

uint AdsbCommandLineArgs::maxBrightness()
{
    return m_maxBrightness;
}

void AdsbCommandLineArgs::setInitialBrightness(uint b)
{
    m_initialBrightness = b;
}

uint AdsbCommandLineArgs::initialBrightness()
{
    return m_initialBrightness;
}

void AdsbCommandLineArgs::setRadio(bool radio)
{
    m_radio = radio;
}

bool AdsbCommandLineArgs::radio()
{
    return m_radio;
}

void AdsbCommandLineArgs::setZoomButtons(bool z)
{
    m_zoomButtons = z;
}

bool AdsbCommandLineArgs::zoomButtons()
{
    return m_zoomButtons;
}

void AdsbCommandLineArgs::setNoGL(bool g)
{
    m_nogl = g;
}

bool AdsbCommandLineArgs::noGL()
{
    return m_nogl;
}

void AdsbCommandLineArgs::setUpdateCharts(bool u)
{
    m_updateCharts = u;
}

bool AdsbCommandLineArgs::updateCharts()
{
    return m_updateCharts;
}

void AdsbCommandLineArgs::setLatLong(QString s)
{
    // s is expected to be in lat,long format: 999.9999,999.9999
    QStringList l = s.split(',');
    m_pos = QGeoCoordinate(l[0].toDouble(), l[1].toDouble());
    m_pos.setAltitude(0);
}

QGeoCoordinate AdsbCommandLineArgs::pos()
{
    return m_pos;
}

void AdsbCommandLineArgs::setWifichooser(bool w)
{
    m_wifichooser = w;
}

bool AdsbCommandLineArgs::wifichooser() const
{
    return m_wifichooser;
}

uint AdsbCommandLineArgs::initialZoomLevel() const
{
    return m_initialZoomLevel;
}

void AdsbCommandLineArgs::setInitialZoomLevel(const uint &initialZoomLevel)
{
    m_initialZoomLevel = initialZoomLevel;
}

bool AdsbCommandLineArgs::showDebugButton() const
{
    return m_showDebugButton;
}

void AdsbCommandLineArgs::setShowDebugButton(bool showDebugButton)
{
    m_showDebugButton = showDebugButton;
}
