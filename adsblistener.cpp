#include "adsblistener.h"
#include <QDebug>
#include "dump1090/rtlsdr/rtl-sdr.h"
#include "dump1090/dump1090.h"
#include <thread>
#include <QThread>


AdsbListener::AdsbListener()
{
    // We assume that the radio was just instantiated. Let's give it some time to start
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    m_port = 0;
    m_hostname = "";
    m_active = false;
    m_ownshipTarget = nullptr;
    m_ownshipGainUpdate = false;    
    m_ownship = "";
    m_ownshipGainTimer = nullptr;
    setDebugLevel(0);

    // Get list of gain levels supported by our radio
    // If we are not running the radio inside this program,
    // m_gainsCount will be set to -1

    int gainsArray[1000]; // No way to tell beforehand how many gain values will be returned
    m_gainsCount = rtlsdr_get_tuner_gains(getDevice(), gainsArray);

    if( m_gainsCount != -1 ) {
        m_gainIndex = -1;
        int gainSetting = getActualGain();
        m_gains << MODES_AUTO_GAIN;
        for(int i = 0; i < m_gainsCount; i++) {
            m_gains << gainsArray[i];
            if(gainsArray[i] == gainSetting)
                m_gainIndex = i + 1;
        }
        // If m_gainIndex was not set, we could not find a gain setting that
        // matches the current gain, so we set the gain to the maximum value
        // possible, which is a good setting for this application.
        if(m_gainIndex == -1) {
            setGainIndex(m_gains.length() - 1);            
        } else {
            setGainIndex(m_gainIndex); // So gainChanged signal will be emitted
        }

        // Set default values (will normally be overriden by user settings values later)
        setNormalGainIndex(getGainIndex());
        setOwnshipGainIndex(ADSBLISTENER_OWNSHIP_GAIN_INDEX);
    }

    // Configure reconnect timer
    m_reconnectTimer.setSingleShot(true);
    m_reconnectTimer.callOnTimeout([&](){setActive(true);});

    // Open debug file
    m_file = new QFile("debug.log");
    m_file->open(QIODevice::Append | QIODevice::WriteOnly | QIODevice::Text);
}

bool AdsbListener::isRadioAvailable() const {
    return m_gainsCount != -1;
}

AdsbListener::AdsbListener(QObject *parent) : AdsbListener()
{
    setParent(parent);
}

AdsbTarget *AdsbListener::getTarget(QString address)
{
    QString s = address.toUpper();
    if(m_map.contains(s))
        return m_map[s];
    else
        return nullptr;
}

void AdsbListener::readAdsb()
{    
    QTextStream out(m_file);

    while(socket->canReadLine()) {

        // An ADS-B message was received. Let's read it
        QString data = socket->readLine();

        if(getDebugLevel() > 0)
            out << data;

        // Let's check that the address is valid
        QString address = AdsbTarget::extractAddress(data);
        if(address == "000000")
            continue;

        emit dataReceived(data);
        // We need to see if this is a message about a target we were already tracking or a new one
        AdsbTarget *existing = getTarget(address);

        if(existing != nullptr)
        {
            // If the target exists, we update its data with the new message
            existing->setUpdate(data);
        }
        else
        {
            // If the target does not exist, we create a new one from the data in our message
            AdsbTarget *newTarget = new AdsbTarget(data);
            m_map.insert(newTarget->address(), newTarget);

            // If the target is our ownship, we suppress its display and enable signals from it
            if(address.toUpper() == ownshipString().toUpper()) {
                newTarget->setVisible(false);
                newTarget->setIsOwnship(true);
                m_ownshipTarget = newTarget;
                connect(m_ownshipTarget, SIGNAL(altitudeUpdated()), this, SIGNAL(ownshipAltitudeChanged()));
                emit ownshipAvailable();                
            }

            // The target object is configured to expire after a certain time (usually 60 seconds)
            // without receiving any updates. When that happens, we need to remove the target from
            // our list of tracked targets
            connect(newTarget, &AdsbTarget::stale, this, &AdsbListener::expireTarget);

            emit targetAdded(newTarget);
        }
    }    
}


void AdsbListener::expireTarget(AdsbTarget *target)
{
    // We haven't received an update for a target for a while, so we remove it
    // from our list of tracked targets

    if(target == m_ownshipTarget) {
        disconnect(m_ownshipTarget, SIGNAL(altitudeUpdated()), this, SIGNAL(ownshipAltitudeChanged()));
        m_ownshipTarget = nullptr;
        emit ownshipAvailable();
        emit ownshipUpdated();
    }

    m_map.remove(target->address());

    target->deleteLater();
    emit targetLost(target);
}

int AdsbListener::getNormalGainIndex() const
{
    return m_normalGainIndex;
}

void AdsbListener::setNormalGainIndex(int normalGainIndex)
{
    m_normalGainIndex = normalGainIndex;
    if(!getOwnshipGainUpdate())
        setGainIndex(normalGainIndex);

    emit normalGainChanged();
}

int AdsbListener::getOwnshipGainIndex() const
{
    return m_ownshipGainIndex;
}

void AdsbListener::setOwnshipGainIndex(int ownshipGainIndex)
{
    m_ownshipGainIndex = ownshipGainIndex;
    emit ownshipGainChanged();
}

void AdsbListener::resetOwnshipTimer()
{    
    if(m_ownshipGainUpdate && (m_ownshipGainTimer != nullptr))
        m_ownshipGainTimer->start(ADSBLISTENER_OWNSHIP_GAIN_INTERVAL * 1000);
}

void AdsbListener::resetOwnshipTimerDueToUpdate()
{
    // The altitude of our ownship was just updated. No need to lower the gain for a while to try to receive it
    if(m_ownshipGainUpdate) {
        // Check to see if we're already listening at the lower gain
        if(m_ownshipGainTimeout->isActive()) {
            // If so, stop the timeout timer and trigger timeout so gain is reset to normal level
            m_ownshipGainTimeout->stop();
            listenTimeout();
        } else {
            // We were not listening at the lower gain setting so restart the interval timer from zero
            resetOwnshipTimer();
        }
    }
}

void AdsbListener::listenForOwnship()
{
    setGainIndex(getOwnshipGainIndex());
    m_ownshipGainTimeout->start(ADSBLISTENER_OWNSHIP_GAIN_WAIT * 1000);
}

int AdsbListener::getDebugLevel() const
{
    return m_debugLevel;
}

void AdsbListener::setDebugLevel(int debugLevel)
{
    m_debugLevel = debugLevel;    
    emit debugLevelChanged();
}

void AdsbListener::clearDebugFile()
{
    m_file->resize(0);
}

void AdsbListener::listenTimeout()
{
    setGainIndex(getNormalGainIndex());
    resetOwnshipTimer();
}

bool AdsbListener::getOwnshipGainUpdate() const
{
    return m_ownshipGainUpdate;
}

void AdsbListener::setOwnshipGainUpdate(bool ownshipGainUpdate)
{
    if(m_ownshipGainUpdate == ownshipGainUpdate) return;

    if(!ownshipGainUpdate) {
        m_ownshipGainUpdate = ownshipGainUpdate;
        if(m_ownshipGainTimer != nullptr) {
            disconnect(this, &AdsbListener::ownshipUpdated, this, &AdsbListener::resetOwnshipTimer);
            setGainIndex(getNormalGainIndex());
            m_ownshipGainTimer->stop();
            m_ownshipGainTimer->deleteLater();
            m_ownshipGainTimer = nullptr;
            m_ownshipGainTimeout->stop();
            m_ownshipGainTimeout->deleteLater();
        }
    } else {
        // Check that the radio is controlled by this program and that an ownship is set
        if(isRadioAvailable() && (m_ownship != "")) {
            m_ownshipGainUpdate = ownshipGainUpdate;

            if(m_ownshipGainTimer == nullptr ) {
                m_ownshipGainTimer = new QTimer(this);
                m_ownshipGainTimeout = new QTimer(this);
                m_ownshipGainTimer->setSingleShot(true);
                m_ownshipGainTimeout->setSingleShot(true);
                connect(m_ownshipGainTimer, &QTimer::timeout, this, &AdsbListener::listenForOwnship);
                connect(m_ownshipGainTimeout, &QTimer::timeout, this, &AdsbListener::listenTimeout);
                connect(this, &AdsbListener::ownshipAltitudeChanged, this, &AdsbListener::resetOwnshipTimerDueToUpdate);
            }            
            listenForOwnship();
        }
    }
    emit ownshipGainAdjustChanged();
}

AdsbTarget *AdsbListener::getOwnshipTarget() const
{
    return m_ownshipTarget;
}

void AdsbListener::socketError()
{
    qWarning() << "AdsbListener::socketError: Radio socket lost - Reconnecting";

    setActive(false);
    // Try reopening the socket, but wait a few seconds to avoid flooding the system
    m_reconnectTimer.start(3000);
}

AdsbListener::AdsbListener(QString hostname, uint port) : AdsbListener()
{
    m_hostname = hostname;
    m_port = port;
    setActive(true);
}

QMap<QString, AdsbTarget *> &AdsbListener::data()
{
    return m_map;
}

void AdsbListener::setActive(bool value)
{
    if(value && !m_active)
    {
        socket = new QTcpSocket(this);
        socket->connectToHost(m_hostname, m_port, QTcpSocket::ReadOnly);
        connect(socket, &QIODevice::readyRead, this, &AdsbListener::readAdsb);
        connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &AdsbListener::socketError);
    }
    else if(!value && m_active)
    {
        socket->close();
        socket->deleteLater();
    }

    m_active = value;
    emit activeChanged(m_active);
}

bool AdsbListener::active()
{
    return m_active;
}

void AdsbListener::setPort(uint port)
{
    m_port = port;
}

uint AdsbListener::port()
{
    return m_port;
}

void AdsbListener::setHostname(QString hostname)
{
    m_hostname = hostname;
}

QString AdsbListener::hostname()
{
    return m_hostname;
}

void AdsbListener::setOwnshipString(QString s)
{
    if(m_ownship != s.toUpper()) {
        m_ownship = s.toUpper();

        // Check to see if the desired ownship is alredy a tracked target
        if(m_map.contains(m_ownship)) {
            // If an ownship is being tracked, unset it as the ownship and set it as a regular target
            if(m_ownshipTarget != nullptr) {
                m_ownshipTarget->setIsOwnship(false);
                m_ownshipTarget->setVisible(true);
                disconnect(m_ownshipTarget, SIGNAL(altitudeUpdated()), this, SIGNAL(ownshipAltitudeChanged()));
            }
            // Then set the desired ownship
            m_ownshipTarget = m_map[m_ownship];
            m_ownshipTarget->setVisible(false);
            m_ownshipTarget->setIsOwnship(true);
            connect(m_ownshipTarget, SIGNAL(altitudeUpdated()), this, SIGNAL(ownshipAltitudeChanged()));

            emit ownshipAvailable();
            emit ownshipAltitudeChanged();
        }

        // Since hte ownship string was just set, enable periodic lower gain listening for ownship signals
        // Note: this may not be a desirable behavior from a user's perspective. To be debated.
        setOwnshipGainUpdate(true);
        setOwnshipGainIndex(ADSBLISTENER_OWNSHIP_GAIN_INDEX);
        setNormalGainIndex(m_gains.length() - 1);

        emit ownshipUpdated();
    }
}

QString AdsbListener::ownshipString()
{
    return m_ownship;
}

bool AdsbListener::ownshipAltitudeAvailable()
{
    return m_ownshipTarget != nullptr;
}

qint32 AdsbListener::getOwnshipAltitudeFt()
{
    if(ownshipAltitudeAvailable())
        return m_ownshipTarget->altitudeFt();
    else
        return -999999;
}

bool AdsbListener::gainSettingAvailable()
{
    return m_gainsCount != -1;
}

int AdsbListener::gainSettings()
{
    return m_gainsCount == -1 ? m_gainsCount : m_gains.size();
}

QList<int> AdsbListener::getGains() const
{
    return m_gains;
}

int AdsbListener::getGainValue(int index) const
{
    return m_gains[index];
}

void AdsbListener::setGain(int gain)
{
    if(isRadioAvailable()) {
        rtlsdr_set_tuner_gain(getDevice(), gain);
        m_gainIndex = m_gains.indexOf(this->getActualGain());
        if(m_gainIndex == -1) {
            qWarning() << "In AdsbListener::setGain(int gain): Actual gain returned not included in possible gain settings.";
        }
        emit gainChanged();
    }
}

int AdsbListener::getNormalGain() const
{
    return isRadioAvailable() ? getGainValue(getNormalGainIndex()) : 0;
}

int AdsbListener::getActualGain() const
{
    return rtlsdr_get_tuner_gain(getDevice());
}

int AdsbListener::getOwnshipGain() const
{
    return isRadioAvailable() ? getGainValue(getOwnshipGainIndex()) : 0;
}

void AdsbListener::setGainIndex(int g)
{
    if(isRadioAvailable()) {
        if((g < m_gains.length()) && (g >= 0)) {
            m_gainIndex = g;
            setGain(m_gains[g]);
        } else {
            qWarning() << "In AdsbListener::setGainIndex(int gainIndex): Gain index invalid";
        }
    }
}

int AdsbListener::getGainIndex() const
{
    return m_gainIndex;
}

