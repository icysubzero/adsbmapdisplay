#!/bin/bash
# The list of IFR charts to be loaded. The names correspond do the file names
# in the URLs from the FAA charts web site without the .zip extensions
# The list of available charts can be viewed here: https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/ifr/
# Just hover your mouse pointer over the TIFF links to find the file names
# of the charts you want

ifrcharts=( "enr_l30" "enr_l31" "enr_l32" "enr_l33" "enr_l34" )

# These are the folder names to use in the charts HTTP document folder.
# There needs to be one entry for each entry in the ifrcharts array.
# NOTE: spaces are not allowed in the folder names

ifrchartfolders=( "IFR_30_Pennsylvania" "IFR_31_Toronto" "IFR_32_Montreal" "IFR_33_Boston" "IFR_34_New_York" "VFR_Montreal" "VFR_New_York")

# These are the descriptive names that will appear in the app's dropdown list for charts.
# There needs to be one entry for each entry in the ifrcharts array.
# NOTE: Spaces are allowed here

ifrchartnames=( "IFR 30 Pennsylvania" "IFR 31 Toronto" "IFR 32 Montreal" "IFR 33 Boston" "IFR 34 New York" "VFR Montreal" "VFR New York")



# Same as for IFR charts, but the file names correspond do TIFF download links
# from this page: https://aeronav.faa.gov/content/aeronav/sectional_files/

vfrcharts=("Detroit" "Montreal" "New_York")

# Same as for ifrchartfolders

vfrfolders=("VFR_Detroit" "VFR_Montreal" "VFR_New_York")

# Same as for ifrchartnames

vfrnames=("VFR Detroit" "VFR Montreal" "VFR New York")


# This is the folder in the filesystem where the tiled charts will be staged.
# If a local HTTP server is running, this would typically be a folder in the 
# document root of the HTTP server.
staging=/var/www/html

# This is the target host and folder (in [user@]host:path format)
target=pi3:/var/www/html


# IFR chart processing
ifrindex="https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/ifr/"
html=`wget -O - ${ifrindex}`

chartlist="{\"charts\":["

for (( i=0; i<${#ifrcharts[@]}; i++ ))
do
  charturl=`echo $html|grep -E -o "https://aeronav.faa.gov/enroute/.{10}/${ifrcharts[i]}.zip"|tail -1`
  chartlist=${chartlist}"{\"name\":\""${ifrchartnames[i]}"\",\"folder\":\""${ifrchartfolders[i]}"\"},"

  ./processCharts.sh ${charturl} ${ifrchartfolders[i]} ${staging} &

done


# VFR Chart processing

vfrurl="https://aeronav.faa.gov/content/aeronav/sectional_files/"
vfrindex=`wget -O - ${vfrurl}`

for (( i=0; i<${#vfrcharts[@]}; i++ ))
do
  charturl=`echo $vfrindex | grep -E -o "${vfrcharts[i]}_.{2,3}.zip" | tail -1`
  charturl=${vfrurl}${charturl}
  chartlist=${chartlist}"{\"name\":\""${vfrnames[i]}"\",\"folder\":\""${vfrfolders[i]}"\"},"

  ./processCharts.sh ${charturl} ${vfrfolders[i]} ${staging} "-expand rgba" &

done


# Remove the extra comma in the chartlist file and add closing brace
chartlist=${chartlist::-1}"]}"

# Write chartlist file to staging area. The app will use this file to 
# populate the dropdown with all the available charts.
echo $chartlist > ${staging}/chartlist

# Wait for all processing jobs to finish
wait

# Copy files from local staging area to remote staging folder
rsync -avz ${staging}/ ${target}

