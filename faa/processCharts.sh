#!/bin/bash

# Usage: processChart.sh <zipfile> <tiles folder> <target> <zoomlevel> [<additional gdal_translate options>]

# Extract a tif chart from a <zipfile>, tile it into <tiles folder> and upload it to a
# <target> folder. Pass optional <additional gdal_translate options> to gdal_translate
# before tiling.

child=0
progdir=`pwd`
dir=`mktemp -d`
pushd ${dir}

function cleanup()
{
    # Kill any children processes that may be running
    if [ ${child} != 0 ]
    then
        kill -TERM "$child"
    fi
    # Remove temporary files
    popd
    rm -rf ${dir}    
    exit 0
}

trap cleanup EXIT
trap cleanup SIGINT
trap cleanup SIGTERM

# Extract zip filename from URL
zip=`basename $1`

# Download zip file and extract tif file
wget $1 &

child=$!
wait "$child"
child=0

# Extract tif filename from zip file and extract file
tif=`unzip -l ${zip} *.tif|sed -n '4 p'|cut -c 31-`
unzip $zip "${tif}" &

child=$!
wait "$child"
child=0

vrt=$2.vrt
folder=$2

# Tile chart
gdal_translate -of vrt $5 $6 $7 $8 $9 "${tif}" $vrt &

child=$!
wait "$child"
child=0

zoomlevel=$4

pythonversion=`gdalinfo --version | cut -b 6`
/usr/bin/python${pythonversion} ${progdir}/gdal2tilesG256.py -z 1-$zoomlevel -d $vrt &

child=$!
wait "$child"
child=0

# Copy chart to staging folder
mv $folder $3
