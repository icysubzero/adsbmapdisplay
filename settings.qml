import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Settings 1.0
import QtQuick.VirtualKeyboard 2.4
import QtQuick.Controls.Universal 2.12
import QtQuick.Dialogs 1.3
import QtPositioning 5.12
import QtQuick.Controls.Styles 1.4

Item {
    id: settingsItem
    property Settings settings
    property string currentOwnship
    property variant currentCenter
    property Item map

    property Item colorDialog : piColorDialog
    anchors.fill: parent

    signal closed()

    ColumnLayout {
        anchors.fill: parent
        Text {
            Layout.margins: 5
            text: "Settings"
            font.bold: true
            color: 'white'
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5
            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5

            Flickable {
                id: flickableSettings
                anchors.fill: parent
                anchors.margins: 5
                contentWidth: parent.width - 10
                contentHeight: contentItem.childrenRect.height
                flickableDirection: Flickable.VerticalFlick
                clip: true

                MouseArea {
                    // Cause fields to lose focus when clicking outside of them
                    anchors.fill: parent
                    onClicked: {
                        focus = true
                        Qt.inputMethod.hide()
                    }
                }

                Column {
                    id: colLayout
                    width: parent.width
                    spacing: 20
                    leftPadding: 5

                    Column {
                        width: parent.width
                        spacing: 10

                        Text {
                            id: icaoLabel
                            font.bold: true
                            color: 'white'
                            text: "Ownship ICAO hex address:"
                        }

                        Row {
                            width: parent.width
                            spacing: 10

                            TextField {
                                property int posY
                                width: parent.width * 0.5 // colLayout.width * 0.5

                                id: icao
                                font.capitalization: Font.AllLowercase

                                font.pointSize: textSize

                            }
                            Button {
                                visible: icao.text !== ''
                                text: "Clear"
                                font.pointSize: buttonSize
                                onClicked: {
                                    icao.text = ''
                                }
                            }

                            Button {
                                visible: (currentOwnship !== '') && (currentOwnship !== icao.text)
                                text: 'Set to current ownship'
                                font.pointSize: buttonSize
                                onClicked: {
                                    icao.text = currentOwnship
                                    focus = true
                                }
                            }
                        }
                    }

                    Column {
                        width: parent.width
                        spacing: 10

                        Rectangle {
                            width: parent.width
                            implicitHeight: 1
                            border.width: 2
                            border.color: 'white'
                        }
                        Text {
                            font.bold: true
                            color: 'white'
                            text: "Maximum zoom levels at which to tile charts:"
                        }
                        Row {
                            width: parent.width
                            spacing: 10

                            Text {
                                color: 'white'
                                text: 'VFR: '
                            }
                            SpinBox {
                                id: vfr
                                width: parent.width * 0.2
                                to: 11
                                from: 2
                                font.pointSize: textSize

                            }
                            Text {
                                color: 'white'
                                text: '   IFR: '
                            }
                            SpinBox {
                                id: ifr
                                width: parent.width * 0.2
                                to: 11
                                from: 2
                                font.pointSize: buttonSize

                            }
                        }
                    }

                    Column {
                        width: parent.width
                        spacing: 10

                        Rectangle {
                            width: parent.width
                            height: 1
                            border.width: 2
                            border.color: 'white'
                        }
                        Text {
                            font.bold: true
                            color: 'white'
                            text: "Reticle color:"
                        }
                        Row {
                            width: parent.width
                            spacing: 10

                            Text {
                                color: 'white'
                                text: 'Over no chart: '
                            }
                            Rectangle {
                                height: 25
                                width: 30
                                border.color: 'white'
                                border.width: 2

                            }

                            Text {
                                leftPadding: 20
                                color: 'white'
                                text: 'Over a chart: '
                            }
                            Rectangle {
                                height: 25
                                width: 30
                                border.color: 'white'
                                border.width: 2
                            }
                        }
                    }

                    Column {
                        width: parent.width
                        spacing: 10

                        Rectangle {
                            width: parent.width
                            height: 1
                            border.width: 2
                            border.color: 'white'
                        }

                        Text {
                            font.bold: true
                            color: 'white'
                            text: "Aircraft color:"
                        }
                        GridLayout {
                            columns: 4
                            flow: GridLayout.LeftToRight
                            Layout.fillWidth: true

                            Text {
                                color: 'white'
                                text: 'Over no chart: '
                            }
                            Rectangle {
                                height: 25
                                width: 30
                                border.color: 'white'
                                border.width: 2

                            }

                            Text {
                                Layout.leftMargin: 20
                                color: 'white'
                                text: 'Over a chart: '
                            }
                            Rectangle {
                                height: 25
                                width: 30
                                border.color: 'white'
                                border.width: 2

                            }

                            Text {
                                color: 'white'
                                text: 'Track color: '
                            }
                            Rectangle {
                                height: 25
                                width: 30
                                border.color: 'white'
                                border.width: 2

                            }

                            Text {
                                color: 'white'
                                text: 'Projected track: '
                            }
                            Rectangle {
                                height: 25
                                width: 30
                                border.color: 'white'
                                border.width: 2

                            }

                        }
                    }

                    Column {
                        width: parent.width
                        spacing: 10

                        Rectangle {
                            width: parent.width
                            height: 1
                            border.width: 2
                            border.color: 'white'
                        }

                        Text {
                            id: posLabel
                            font.bold: true
                            color: 'white'
                            text: "Default center position when no GPS available:"
                        }
                        GridLayout {
                            width: parent.width
                            columnSpacing: 10
                            rowSpacing: 10
                            columns: 3

                                Text {
                                    color: 'white'
                                    text: 'Lat.:'
                                    font.pointSize: textSize
                                }
                                TextField {
                                    property int posY
                                    implicitWidth: 150
                                    font.pointSize: textSize


                                }

                                Button {
                                    Layout.alignment: Qt.AlignBottom
                                    text: "Set to current center"
                                    font.pointSize: buttonSize

                                }

                                Text {
                                    color: 'white'
                                    text: 'Long.:'
                                }
                                TextField {
                                    property int posY
                                    implicitWidth: 150
                                    font.pointSize: textSize
                                }
                            }
                        }

                    Column {
                        width: parent.width
                        spacing: 10

                        Rectangle {
                            width: parent.width
                            height: 1
                            border.width: 2
                            border.color: 'white'
                        }

                        Text {
                            font.bold: true
                            color: 'white'
                            text: "Proximity alerts - Airplanes blink when less than"
                        }

                        Row {
                            width: parent.width
                            spacing: 10

                            ComboBox {
                                id: altAlertCB
                                textRole: 'text'
                                width: 135
                                font.pointSize: textSize

                                model: ListModel {
                                    id: altItems
                                    ListElement { text: "0 (no alerts)"; value: 0 }
                                    ListElement { text: "1000"; value: 1000 }
                                    ListElement { text: "3000"; value: 3000 }
                                    ListElement { text: "5000"; value: 5000 }
                                    ListElement { text: "any"; value: 50000 }
                                }

                            }
                            Text {
                                color: 'white'
                                text: 'feet of altitude AND'
                            }
                        }
                        Row {
                            width: parent.width
                            spacing: 10
                            ComboBox {
                                id: distAlertCB
                                textRole: 'text'
                                width: altAlertCB.width
                                font.pointSize: textSize

                                model: ListModel {
                                    id: distItems
                                    ListElement { text: "0 (no alerts)"; value: 0 }
                                    ListElement { text: "1"; value: 1 }
                                    ListElement { text: "3"; value: 3 }
                                    ListElement { text: "5"; value: 5 }
                                    ListElement { text: "any"; value: 50000 }
                                }
                            }
                            Text {
                                color: 'white'
                                text: 'nautical miles away'
                            }
                        }
                    }

                    Column {
                        width: parent.width
                        spacing: 10

                        Rectangle {
                            id: chartsettingsrect
                            width: parent.width
                            height: 1
                            border.width: 2
                            border.color: 'white'
                        }

                        Text {
                            font.bold: true
                            color: 'white'
                            text: "Chart settings:"
                        }

                        GridLayout {
                            Layout.fillWidth: true
                            columns: 2
                            columnSpacing: 10
                            property int col1: chartsettingsrect.width * 0.3
                            property int col2: chartsettingsrect.width * 0.3
                            property int col3: chartsettingsrect.width * 0.3

                            Text{
                                Layout.preferredWidth: parent.col1
                                Layout.fillWidth: true
                                horizontalAlignment: Text.AlignRight
                                wrapMode: Text.WordWrap
                                color: 'white'
                                text: "Auto center on GPS position:"
                            }
                            ComboBox {
                                id: autocenterMode
                                textRole: 'text'
                                font.pointSize: textSize
                                Layout.preferredWidth: parent.col2

                                model: ListModel {
                                    id: autocenterItems
                                    ListElement { text: "No"; value: 1 }
                                    ListElement { text: "Center"; value: 2 }
                                    ListElement { text: "Offset"; value: 3 }
                                }
                            }

                            Text {
                                horizontalAlignment: Text.AlignRight
                                Layout.fillWidth: true
                                wrapMode: Text.WordWrap
                                color: 'white'
                                text: "Reticle style:"
                            }
                            ComboBox {
                                id: reticleStyle
                                textRole: 'text'
                                font.pointSize: textSize
                                Layout.preferredWidth: parent.col2

                                model: ListModel {
                                    id: reticleItems
                                    ListElement { text: "Full"; value: 1 }
                                    ListElement { text: "Position"; value: 2 }
                                    ListElement { text: "None"; value: 3 }
                                }
                            }

                            Text {
                                horizontalAlignment: Text.AlignRight
                                Layout.fillWidth: true
                                color: 'white'
                                text: "Track up"
                            }
                            Row {
                                Switch {

                                }
                                Text {
                                    color: 'white'
                                    text: "North up"
                                }
                            }

                            Text {
                                Layout.preferredWidth: parent.col1
                                horizontalAlignment: Text.AlignRight
                                wrapMode: Text.WordWrap
                                color: 'white'
                                text: 'Only show aircraft within '
                            }
                            Row {
                                spacing: 10
                                ComboBox {
                                    id: control
                                    model: [ "any ft.", "5 000 ft.", "10 000 ft." ]
                                    font.pointSize: textSize
                                }

                                Text {
                                    color: 'white'
                                    text: 'above or below'
                                    wrapMode: Text.WordWrap
                                }

                            }

                            Text {
                                horizontalAlignment: Text.AlignRight
                                Layout.preferredWidth: parent.col1
                                wrapMode: Text.WordWrap
                                color: 'white'
                                text: "Map zoom level:"
                            }
                            Row {
                                spacing: 10
                                Slider {
                                    id: zoomSlider
                                    from: 5
                                    to: 15
                                    stepSize: 0.1
                                }
                                Text {
                                    color: 'white'
                                }
                            }

                            Text {
                                Layout.preferredWidth: parent.col1
                                horizontalAlignment: Text.AlignRight
                                wrapMode: Text.WordWrap
                                color: 'white'
                                text: "Font & airplane size:"
                            }
                            Row {
                                spacing: 10
                                Slider {
                                    id: fontSlider
                                    from: 5
                                    to: 50
                                    stepSize: 1
                                }
                                Text {
                                    color: 'white'
                                }
                            }
                            Text {
                                Layout.preferredWidth: parent.col1
                                horizontalAlignment: Text.AlignRight
                                wrapMode: Text.WordWrap
                                color: 'white'
                                text: "Screen brightness:"
                            }
                            Row {
                                spacing: 10
                                Slider {
                                    id: brightnessSlider
                                    from: 0
                                    stepSize: 1
                                }
                                Text {
                                    color: 'white'
                                }
                            }
                        }
                    }
                }
            }
        }
        Row {
            id: settingsButtons
            spacing: 10
            Layout.alignment: Qt.AlignRight
            rightPadding: 10
            bottomPadding: 10

            Button {
                text: "Default values"
                font.pointSize: buttonSize
                onClicked: {
                    settings.resetDefaults();
                }
            }
            Button {
                text: "Save"
                font.pointSize: buttonSize
                onClicked: {
                    settings.saveSettings();
                    settingsItem.closed();
                }
            }
            Button {
                text: "Apply"
                font.pointSize: buttonSize
                onClicked: {
                    settingsItem.closed();
                }
            }
            Button {
                text: "Cancel"
                font.pointSize: buttonSize
                onClicked: {
                    settings.rollbackChanges();
                    settingsItem.closed();

                }
            }
        }
    }

    Item {
        id: piColorDialog

        property color color

        visible: false
        width: parent.width
        height: parent.height
        x: (parent.parent.width - width) / 2
        y: (parent.parent.height - height) / 2
        z: 10

        signal onAccepted
        signal onRejected

        Rectangle {
            anchors.fill: parent
            color: '#222222'
            border.color: 'white'
            border.width: 4
            radius: 12
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 5
            Text {
                Layout.margins: 5
                text: "Please choose a color"
                font.pointSize: 8
                font.bold: true
                color: 'white'
            }

            Rectangle {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.margins: 5
                border.width: 2
                border.color: 'white'
                color: 'transparent'
                radius: 5

                ColumnLayout {

                    ButtonGroup {
                        buttons: colorMode.children
                    }

                    RowLayout {
                        id: colorMode
                        Layout.leftMargin: 5
                        Layout.rightMargin: 5
                        Layout.topMargin: 2
                        Text {
                            font.pointSize: 8
                            color: 'white'
                            text: 'Color mode: '
                        }

                        RadioButton {
                            id: rgb
                            checked: true
                            text: "RGB"
                            font.pointSize: 8
                        }
                        RadioButton {
                            id: hsv
                            checked: false
                            text: "HSV"
                            font.pointSize: 8
                        }
                    }
                    RowLayout {
                        Layout.leftMargin: 5
                        Layout.rightMargin: 5
                        Layout.topMargin: 2
                        ColumnLayout {
                            visible: rgb.checked
                            Layout.leftMargin: 5
                            Layout.rightMargin: 5
                            Layout.topMargin: 2

                            RowLayout {
                                Text {
                                    text: "R: "
                                    font.pointSize: 8
                                    color: 'white'
                                }
                                Slider {
                                    id: rSlider
                                    from: 0
                                    to: 100
                                    value: piColorDialog.color.r * 100
                                    x: 50
                                    width: 250
                                    onMoved: { piColorDialog.color.r = value / 100.0 }
                                }
                            }
                            RowLayout {
                                Text {
                                    text: "G: "
                                    font.pointSize: 8
                                    color: 'white'
                                }
                                Slider {
                                    id: gSlider
                                    from: 0
                                    to: 100
                                    value: piColorDialog.color.g * 100
                                    x: 50
                                    width: 250
                                    onMoved: { piColorDialog.color.g = value / 100.0 }
                                }
                            }
                            RowLayout {
                                Text {
                                    text: "B: "
                                    font.pointSize: 8
                                    color: 'white'
                                }
                                Slider {
                                    id: bSlider
                                    from: 0
                                    to: 100
                                    value: piColorDialog.color.b * 100
                                    x: 50
                                    width: 250
                                    onMoved: { piColorDialog.color.b = value / 100.0 }
                                }
                            }
                        }
                        ColumnLayout {
                            visible: hsv.checked
                            Layout.leftMargin: 5
                            Layout.rightMargin: 5
                            Layout.topMargin: 2

                            RowLayout {
                                Text {
                                    text: "H: "
                                    font.pointSize: 8
                                    color: 'white'
                                }
                                Slider {
                                    id: hSlider
                                    from: 0
                                    to: 100
                                    value: piColorDialog.color.hsvHue * 100
                                    x: 50
                                    width: 250
                                    onMoved: { piColorDialog.color.hsvHue = value / 100.0 }
                                }
                            }
                            RowLayout {
                                Text {
                                    text: "S: "
                                    font.pointSize: 8
                                    color: 'white'
                                }
                                Slider {
                                    id: sSlider
                                    from: 0
                                    to: 100
                                    value: piColorDialog.color.hsvSaturation * 100
                                    x: 50
                                    width: 250
                                    onMoved: { piColorDialog.color.hsvSaturation = value / 100.0 }
                                }
                            }
                            RowLayout {
                                Text {
                                    text: "V: "
                                    font.pointSize: 8
                                    color: 'white'
                                }
                                Slider {
                                    id: vSlider
                                    from: 0
                                    to: 100
                                    value: piColorDialog.color.hsvValue * 100
                                    x: 50
                                    width: 250
                                    onMoved: { piColorDialog.color.hsvValue = value / 100.0 }
                                }
                            }
                        }

                        Rectangle {
                            Layout.leftMargin: 0
                            Layout.rightMargin: 5
                            height: 80
                            width: 80
                            color: piColorDialog.color
                            border.color: 'white'
                            border.width: 4
                            radius: 5
                        }
                    }
                    RowLayout {
                        visible: piColorDialog.height > 310
                        Layout.leftMargin: 5
                        Layout.rightMargin: 5
                        Layout.topMargin: 2
                        Text {
                            text: "Standard colors:"
                            font.pointSize: 8
                            color: 'white'
                        }
                        ComboBox {
                            id: cbColors
                            Layout.fillWidth: true
                            font.pointSize: textSize

                            delegate: ItemDelegate {
                                width: cbColors.width
                                highlighted: cbColors.highlightedIndex === index
                                contentItem: Rectangle {
                                    anchors.margins: 2
                                    anchors.fill: parent
                                    border.color: 'white'
                                    border.width: 0
                                    radius: 5
                                    color: name
                                    Text{
                                        font.pointSize: 8
                                        anchors.centerIn: parent
                                        text: name
                                    }
                                }
                            }

                            model: ListModel {
                                id: model
                                ListElement { name: 'aliceblue'; code: '#f0f8ff' }
                                ListElement { name: 'antiquewhite'; code: '#faebd7' }
                                ListElement { name: 'aqua'; code: '#00ffff' }
                                ListElement { name: 'aquamarine'; code: '#7fffd4' }
                                ListElement { name: 'azure'; code: '#f0ffff' }
                                ListElement { name: 'beige'; code: '#f5f5dc' }
                                ListElement { name: 'bisque'; code: '#ffe4c4' }
                                ListElement { name: 'black'; code: '#000000' }
                                ListElement { name: 'blanchedalmond'; code: '#ffebcd' }
                                ListElement { name: 'blue'; code: '#0000ff' }
                                ListElement { name: 'blueviolet'; code: '#8a2be2' }
                                ListElement { name: 'brown'; code: '#a52a2a' }
                                ListElement { name: 'burlywood'; code: '#deb887' }
                                ListElement { name: 'cadetblue'; code: '#5f9ea0' }
                                ListElement { name: 'chartreuse'; code: '#7fff00' }
                                ListElement { name: 'chocolate'; code: '#d2691e' }
                                ListElement { name: 'coral'; code: '#ff7f50' }
                                ListElement { name: 'cornflowerblue'; code: '#6495ed' }
                                ListElement { name: 'cornsilk'; code: '#fff8dc' }
                                ListElement { name: 'crimson'; code: '#dc143c' }
                                ListElement { name: 'cyan'; code: '#00ffff' }
                                ListElement { name: 'darkblue'; code: '#00008b' }
                                ListElement { name: 'darkcyan'; code: '#008b8b' }
                                ListElement { name: 'darkgoldenrod'; code: '#b8860b' }
                                ListElement { name: 'darkgray'; code: '#a9a9a9' }
                                ListElement { name: 'darkgreen'; code: '#006400' }
                                ListElement { name: 'darkgrey'; code: '#a9a9a9' }
                                ListElement { name: 'darkkhaki'; code: '#bdb76b' }
                                ListElement { name: 'darkmagenta'; code: '#8b008b' }
                                ListElement { name: 'darkolivegreen'; code: '#556b2f' }
                                ListElement { name: 'darkorange'; code: '#ff8c00' }
                                ListElement { name: 'darkorchid'; code: '#9932cc' }
                                ListElement { name: 'darkred'; code: '#8b0000' }
                                ListElement { name: 'darksalmon'; code: '#e9967a' }
                                ListElement { name: 'darkseagreen'; code: '#8fbc8f' }
                                ListElement { name: 'darkslateblue'; code: '#483d8b' }
                                ListElement { name: 'darkslategray'; code: '#2f4f4f' }
                                ListElement { name: 'darkslategrey'; code: '#2f4f4f' }
                                ListElement { name: 'darkturquoise'; code: '#00ced1' }
                                ListElement { name: 'darkviolet'; code: '#9400d3' }
                                ListElement { name: 'deeppink'; code: '#ff1493' }
                                ListElement { name: 'deepskyblue'; code: '#00bfff' }
                                ListElement { name: 'dimgray'; code: '#696969' }
                                ListElement { name: 'dimgrey'; code: '#696969' }
                                ListElement { name: 'dodgerblue'; code: '#1e90ff' }
                                ListElement { name: 'firebrick'; code: '#b22222' }
                                ListElement { name: 'floralwhite'; code: '#fffaf0' }
                                ListElement { name: 'forestgreen'; code: '#228b22' }
                                ListElement { name: 'fuchsia'; code: '#ff00ff' }
                                ListElement { name: 'gainsboro'; code: '#dcdcdc' }
                                ListElement { name: 'ghostwhite'; code: '#f8f8ff' }
                                ListElement { name: 'gold'; code: '#ffd700' }
                                ListElement { name: 'goldenrod'; code: '#daa520' }
                                ListElement { name: 'gray'; code: '#808080' }
                                ListElement { name: 'grey'; code: '#808080' }
                                ListElement { name: 'green'; code: '#008000' }
                                ListElement { name: 'greenyellow'; code: '#adff2f' }
                                ListElement { name: 'honeydew'; code: '#f0fff0' }
                                ListElement { name: 'hotpink'; code: '#ff69b4' }
                                ListElement { name: 'indianred'; code: '#cd5c5c' }
                                ListElement { name: 'indigo'; code: '#4b0082' }
                                ListElement { name: 'ivory'; code: '#fffff0' }
                                ListElement { name: 'khaki'; code: '#f0e68c' }
                                ListElement { name: 'lavender'; code: '#e6e6fa' }
                                ListElement { name: 'lavenderblush'; code: '#fff0f5' }
                                ListElement { name: 'lawngreen'; code: '#7cfc00' }
                                ListElement { name: 'lemonchiffon'; code: '#fffacd' }
                                ListElement { name: 'lightblue'; code: '#add8e6' }
                                ListElement { name: 'lightcoral'; code: '#f08080' }
                                ListElement { name: 'lightcyan'; code: '#e0ffff' }
                                ListElement { name: 'lightgoldenrodyellow'; code: '#fafad2' }
                                ListElement { name: 'lightgray'; code: '#d3d3d3' }
                                ListElement { name: 'lightgreen'; code: '#90ee90' }
                                ListElement { name: 'lightgrey'; code: '#d3d3d3' }
                                ListElement { name: 'lightpink'; code: '#ffb6c1' }
                                ListElement { name: 'lightsalmon'; code: '#ffa07a' }
                                ListElement { name: 'lightseagreen'; code: '#20b2aa' }
                                ListElement { name: 'lightskyblue'; code: '#87cefa' }
                                ListElement { name: 'lightslategray'; code: '#778899' }
                                ListElement { name: 'lightslategrey'; code: '#778899' }
                                ListElement { name: 'lightsteelblue'; code: '#b0c4de' }
                                ListElement { name: 'lightyellow'; code: '#ffffe0' }
                                ListElement { name: 'lime'; code: '#00ff00' }
                                ListElement { name: 'limegreen'; code: '#32cd32' }
                                ListElement { name: 'linen'; code: '#faf0e6' }
                                ListElement { name: 'magenta'; code: '#ff00ff' }
                                ListElement { name: 'maroon'; code: '#800000' }
                                ListElement { name: 'mediumaquamarine'; code: '#66cdaa' }
                                ListElement { name: 'mediumblue'; code: '#0000cd' }
                                ListElement { name: 'mediumorchid'; code: '#ba55d3' }
                                ListElement { name: 'mediumpurple'; code: '#9370db' }
                                ListElement { name: 'mediumseagreen'; code: '#3cb371' }
                                ListElement { name: 'mediumslateblue'; code: '#7b68ee' }
                                ListElement { name: 'mediumspringgreen'; code: '#00fa9a' }
                                ListElement { name: 'mediumturquoise'; code: '#48d1cc' }
                                ListElement { name: 'mediumvioletred'; code: '#c71585' }
                                ListElement { name: 'midnightblue'; code: '#191970' }
                                ListElement { name: 'mintcream'; code: '#f5fffa' }
                                ListElement { name: 'mistyrose'; code: '#ffe4e1' }
                                ListElement { name: 'moccasin'; code: '#ffe4b5' }
                                ListElement { name: 'navajowhite'; code: '#ffdead' }
                                ListElement { name: 'navy'; code: '#000080' }
                                ListElement { name: 'oldlace'; code: '#fdf5e6' }
                                ListElement { name: 'olive'; code: '#808000' }
                                ListElement { name: 'olivedrab'; code: '#6b8e23' }
                                ListElement { name: 'orange'; code: '#ffa500' }
                                ListElement { name: 'orangered'; code: '#ff4500' }
                                ListElement { name: 'orchid'; code: '#da70d6' }
                                ListElement { name: 'palegoldenrod'; code: '#eee8aa' }
                                ListElement { name: 'palegreen'; code: '#98fb98' }
                                ListElement { name: 'paleturquoise'; code: '#afeeee' }
                                ListElement { name: 'palevioletred'; code: '#db7093' }
                                ListElement { name: 'papayawhip'; code: '#ffefd5' }
                                ListElement { name: 'peachpuff'; code: '#ffdab9' }
                                ListElement { name: 'peru'; code: '#cd853f' }
                                ListElement { name: 'pink'; code: '#ffc0cb' }
                                ListElement { name: 'plum'; code: '#dda0dd' }
                                ListElement { name: 'powderblue'; code: '#b0e0e6' }
                                ListElement { name: 'purple'; code: '#800080' }
                                ListElement { name: 'red'; code: '#ff0000' }
                                ListElement { name: 'rosybrown'; code: '#bc8f8f' }
                                ListElement { name: 'royalblue'; code: '#4169e1' }
                                ListElement { name: 'saddlebrown'; code: '#8b4513' }
                                ListElement { name: 'salmon'; code: '#fa8072' }
                                ListElement { name: 'sandybrown'; code: '#f4a460' }
                                ListElement { name: 'seagreen'; code: '#2e8b57' }
                                ListElement { name: 'seashell'; code: '#fff5ee' }
                                ListElement { name: 'sienna'; code: '#a0522d' }
                                ListElement { name: 'silver'; code: '#c0c0c0' }
                                ListElement { name: 'skyblue'; code: '#87ceeb' }
                                ListElement { name: 'slateblue'; code: '#6a5acd' }
                                ListElement { name: 'slategray'; code: '#708090' }
                                ListElement { name: 'slategrey'; code: '#708090' }
                                ListElement { name: 'snow'; code: '#fffafa' }
                                ListElement { name: 'springgreen'; code: '#00ff7f' }
                                ListElement { name: 'steelblue'; code: '#4682b4' }
                                ListElement { name: 'tan'; code: '#d2b48c' }
                                ListElement { name: 'teal'; code: '#008080' }
                                ListElement { name: 'thistle'; code: '#d8bfd8' }
                                ListElement { name: 'tomato'; code: '#ff6347' }
                                ListElement { name: 'turquoise'; code: '#40e0d0' }
                                ListElement { name: 'violet'; code: '#ee82ee' }
                                ListElement { name: 'wheat'; code: '#f5deb3' }
                                ListElement { name: 'white'; code: '#ffffff' }
                                ListElement { name: 'whitesmoke'; code: '#f5f5f5' }
                                ListElement { name: 'yellow'; code: '#ffff00' }
                                ListElement { name: 'yellowgreen'; code: '#9acd32' }
                            }
                            onActivated: {
                                piColorDialog.color = model.get(currentIndex).name;

                            }

                        }
                    }
                }
            }

            RowLayout {
                Layout.margins: 5
                Layout.alignment: Qt.AlignRight
                Layout.preferredHeight: 50
                Button {
                    text: "Select"
                    font.pointSize: 8
                    onClicked: {
                        piColorDialog.onAccepted()
                        piColorDialog.visible = false
                    }
                }
                Button {
                    text: "Cancel"
                    font.pointSize: 8
                    onClicked: {
                        piColorDialog.onRejected()
                        piColorDialog.visible = false
                    }
                }
            }
        }
    }
}
