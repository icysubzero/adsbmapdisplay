#ifndef OPENSKYUPDATER_H
#define OPENSKYUPDATER_H

#include <QObject>
#include <QProcess>
#include <QDebug>
#include "download/downloadmanager.h"
#include "download/downloadandunzipitem.h"

class OpenSkyUpdater : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool running READ running NOTIFY statusChanged)

public:
    Q_INVOKABLE static OpenSkyUpdater& getInstance();
    OpenSkyUpdater(OpenSkyUpdater const&) = delete;
    void operator=(OpenSkyUpdater const&) = delete;

    Q_INVOKABLE void update(DownloadManager*);
    bool running();

signals:
    void statusChanged();

private slots:
    void downloadSignals(DownloadItem::DownloadStatus);
    void importFinished(int exitCode, QProcess::ExitStatus);
    void importFailed(QProcess::ProcessError es);

private:
    OpenSkyUpdater();
    void importData();
    void setRunning(bool);

    static OpenSkyUpdater *instance;
    const QList<QStringList> m_files = {{"Aircraft Database", "https://opensky-network.org/datasets/metadata/aircraftDatabase.csv"},
                                        {"Aircraft Types", "https://opensky-network.org/datasets/metadata/doc8643AircraftTypes.csv"},
                                        {"Aircraft Manufacturers", "https://opensky-network.org/datasets/metadata/doc8643Manufacturers.csv"}};

    QList<DownloadAndUnzipItem*> m_downloaders;
    bool m_running;
    int m_finishedCount;
    QProcess m_proc;
};


#endif // OPENSKYUPDATER_H
