#!/bin/bash
#
# This script creates the OpenSky ADS-B databases in sqlite3 from the CSV files
#
# Syntax: createAircraftDB.sh <database filename>

# Remove quotes and commas from manufacturer names in csv because
# sqlite canoot process them
sed -i 's/\"//g;' doc8643Manufacturers.csv

sqlite3 new$1 < update.sql

rm $1 aircraftDatabase.csv doc8643AircraftTypes.csv doc8643Manufacturers.csv
mv new$1 $1

