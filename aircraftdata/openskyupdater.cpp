#include "openskyupdater.h"
#include "aircraftdata/aircraftdata.h"

OpenSkyUpdater::OpenSkyUpdater() : QObject()
{
    setRunning(false);
    QObject::connect(&m_proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &OpenSkyUpdater::importFinished );
    QObject::connect(&m_proc, QOverload<QProcess::ProcessError>::of(&QProcess::errorOccurred), this, &OpenSkyUpdater::importFailed );

    for( auto file : m_files ){
        m_downloaders.append(new DownloadAndUnzipItem(this, file[0], QUrl(file[1]), ".", false));
    }
}


OpenSkyUpdater& OpenSkyUpdater::getInstance()
{
    static OpenSkyUpdater instance;

    return instance;
}

void OpenSkyUpdater::setRunning(bool running)
{
    m_running = running;
    emit statusChanged();
}

bool OpenSkyUpdater::running()
{
    return m_running;
}

void OpenSkyUpdater::update(DownloadManager *dm)
{
   if(!m_running) {
       m_finishedCount = 0;
       setRunning(true);
       for( auto downloader : m_downloaders ) {
           downloader->setCanBeStarted(true);
           dm->addDownloadItem(downloader);
           connect(downloader, &DownloadItem::statusChanged, this, &OpenSkyUpdater::downloadSignals);
       }
   }
}

void OpenSkyUpdater::downloadSignals(DownloadItem::DownloadStatus s)
{
    if(s == DownloadItem::Finished) {
        m_finishedCount++;
        if(m_finishedCount == m_files.count())
            importData();
    } else if(s == DownloadItem::Canceled || s == DownloadItem::Failed) {
        for( auto downloader : m_downloaders ) {
            disconnect(downloader, &DownloadItem::statusChanged, this, &OpenSkyUpdater::downloadSignals);
            downloader->cancelDownload(); // This will merely pause the download
            downloader->cancelDownload(); // This will cancel it for good
            downloader->setCanBeStarted(true);
        }
        setRunning(false);
    }
}

void OpenSkyUpdater::importData()
{
    QStringList cmd;
    cmd << AircraftData::databaseFilename();

    AircraftData::closeDatabaseConnection();
    m_proc.start("createAircraftDB.sh", cmd);
}


void OpenSkyUpdater::importFinished(int exitCode, QProcess::ExitStatus es)
{
    qDebug() << exitCode << es;

    AircraftData::openDatabaseConnection();
    setRunning(false);
}

void OpenSkyUpdater::importFailed(QProcess::ProcessError es)
{
    qDebug() << es;
    AircraftData::openDatabaseConnection();
    setRunning(false);
}
