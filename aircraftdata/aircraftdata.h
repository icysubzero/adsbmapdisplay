#ifndef AIRCRAFTDATA_H
#define AIRCRAFTDATA_H

#include <QObject>
#include <QSqlDatabase>

class AdsbTarget;

class AircraftData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString  icao24 MEMBER m_icao24 NOTIFY dataChanged);
    Q_PROPERTY(QString  registration MEMBER m_registration NOTIFY dataChanged);
    Q_PROPERTY(QString  manufacturericao MEMBER m_manufacturericao NOTIFY dataChanged);
    Q_PROPERTY(QString  manufacturername MEMBER m_manufacturername NOTIFY dataChanged);
    Q_PROPERTY(QString  model MEMBER m_model NOTIFY dataChanged);
    Q_PROPERTY(QString  typecode MEMBER m_typecode NOTIFY dataChanged);
    Q_PROPERTY(QString  serialnumber MEMBER m_serialnumber NOTIFY dataChanged);
    Q_PROPERTY(QString  linenumber MEMBER m_linenumber NOTIFY dataChanged);
    Q_PROPERTY(QString  icaoaircrafttype MEMBER m_icaoaircrafttype NOTIFY dataChanged);
    Q_PROPERTY(QString  oper MEMBER m_operator NOTIFY dataChanged);
    Q_PROPERTY(QString  opercallsign MEMBER m_operatorcallsign NOTIFY dataChanged);
    Q_PROPERTY(QString  opericao MEMBER m_operatoricao NOTIFY dataChanged);
    Q_PROPERTY(QString  operiata MEMBER m_operatoriata NOTIFY dataChanged);
    Q_PROPERTY(QString  owner MEMBER m_owner NOTIFY dataChanged);
    Q_PROPERTY(QString  testreg MEMBER m_testreg NOTIFY dataChanged);
    Q_PROPERTY(QString  registered MEMBER m_registered NOTIFY dataChanged);
    Q_PROPERTY(QString  reguntil MEMBER m_reguntil NOTIFY dataChanged);
    Q_PROPERTY(QString  status MEMBER m_status NOTIFY dataChanged);
    Q_PROPERTY(QString  built MEMBER m_built NOTIFY dataChanged);
    Q_PROPERTY(QString  firstflightdate MEMBER m_firstflightdate NOTIFY dataChanged);
    Q_PROPERTY(QString  seatconfiguration MEMBER m_seatconfiguration NOTIFY dataChanged);
    Q_PROPERTY(QString  engines MEMBER m_engines NOTIFY dataChanged);
    Q_PROPERTY(QString  modes MEMBER m_modes NOTIFY dataChanged);
    Q_PROPERTY(QString  adsb MEMBER m_adsb NOTIFY dataChanged);
    Q_PROPERTY(QString  acars MEMBER m_acars NOTIFY dataChanged);
    Q_PROPERTY(QString  notes MEMBER m_notes NOTIFY dataChanged);
    Q_PROPERTY(QString  category MEMBER m_categorydescription NOTIFY dataChanged);
    Q_PROPERTY(AdsbTarget* target MEMBER target NOTIFY dataChanged);
    Q_PROPERTY(bool     visible READ visible WRITE setVisible NOTIFY visibleChanged );
    Q_PROPERTY(bool     exists READ dataAvailable NOTIFY dataChanged)

public:
    explicit AircraftData(AdsbTarget *parent = nullptr);
    AircraftData(QString icao24, AdsbTarget *parent = nullptr);
    AircraftData(const AircraftData &);
    void setICAO24(QString);

    void operator=(const AircraftData&);
    bool operator==(const AircraftData&);

    bool visible() const;
    void setVisible(bool visible);

// @TODO:: Add getters & setters for C++
    QString m_icao24, m_registration,
            m_manufacturericao, m_manufacturername,
            m_model, m_typecode, m_serialnumber, m_linenumber,
            m_icaoaircrafttype, m_operator, m_operatorcallsign, m_operatoricao, m_operatoriata,
            m_owner, m_testreg, m_registered, m_reguntil, m_status, m_built, m_firstflightdate,
            m_seatconfiguration, m_engines, m_modes, m_adsb, m_acars, m_notes, m_categorydescription;
    AdsbTarget *target;    

    static void openDatabaseConnection();
    static void closeDatabaseConnection();
    static QString databaseFilename();
    static void setDatabaseFilename(const QString &databaseFilename);
    static QString m_databaseFilename;

    bool m_dataAvailable;
    Q_INVOKABLE bool dataAvailable() const;

signals:
    void visibleChanged();
    void dataChanged();
};
//Q_DECLARE_METATYPE(AircraftData)


#endif // AIRCRAFTDATA_H
