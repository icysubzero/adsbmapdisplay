#!/bin/bash
#
# This script downloads the OpenSky ADS-B databases and loads them into a sqlite3 databases
#
# Syntax: updateAircraftDB.sh <database filename>
#
# <database filename> is the name of the database to be created
#
if [ -z "$1" ] ; then
    echo "Argument missing"
    echo "Usage: updateAircraftDB.sh <database filename>"
    exit 1
fi

rm $1 aircraftDatabase.csv doc8643AircraftTypes.csv doc8643Manufacturers.csv manufacturers.csv

function cleanup()
{
    # Kill any children processes that may be running
    kill -TERM $c1 $c2 $c3
    rm aircraftDatabase.csv doc8643AircraftTypes.csv doc8643Manufacturers.csv manufacturers.csv
    exit 0
}

trap cleanup EXIT
trap cleanup SIGINT
trap cleanup SIGTERM

wget https://opensky-network.org/datasets/metadata/aircraftDatabase.csv &
c1=$!
wget -v https://opensky-network.org/datasets/metadata/doc8643AircraftTypes.csv &
c2=$!
wget https://opensky-network.org/datasets/metadata/doc8643Manufacturers.csv &
c3=$!

wait $c3
wait $c2
wait $c1

# Remove quotes and commas from manufacturer names in csv because
# sqlite canoot process them
sed -s 's/\"//g;' doc8643Manufacturers.csv > manufacturers.csv

sqlite3 $1 < update.sql
