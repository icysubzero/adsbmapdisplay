#include "aircraftdata.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDebug>
#include "adsbtarget.h"

QString AircraftData::m_databaseFilename = "";

AircraftData::AircraftData(AdsbTarget *parent) : QObject((QObject *)parent)
{
    target = parent;
}

AircraftData::AircraftData(QString icao24, AdsbTarget *parent) : AircraftData(parent)
{
    setICAO24(icao24);
}

AircraftData::AircraftData(const AircraftData &a) : AircraftData()
{
    this->operator=(a);
}

void AircraftData::openDatabaseConnection() {

    // Create a connection to the aircraft database that will be used throughout the program
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "AircraftDatabase");
    db.setDatabaseName(databaseFilename());

    if(!db.open()) {
        qWarning() << "Error occurred opening database file" << databaseFilename();
        qWarning("%s.", qPrintable(db.lastError().text()));
    }
}

void AircraftData::closeDatabaseConnection() {
    QSqlDatabase db = QSqlDatabase::database("AircraftDatabase");

    if(db.isOpen())
        db.close();
}

QString AircraftData::databaseFilename()
{
    return m_databaseFilename;
}

void AircraftData::setDatabaseFilename(const QString &databaseFilename)
{
    m_databaseFilename = databaseFilename;
}

bool AircraftData::dataAvailable() const
{
    return m_dataAvailable;
}

void AircraftData::setICAO24(QString icao24)
{
    if(icao24.toLower() != m_icao24) {
        m_icao24 = icao24.toLower();

        QSqlDatabase db = QSqlDatabase::database("AircraftDatabase");

        // If no database is available, don't bother querying it
        if(!db.isOpen()) {
            qDebug("Database not available.");
            // Set m_icao24 to nothing so the database operation will be attempted again at the next update
            m_icao24 = "";
            m_dataAvailable = false;
            return;
        }

        QSqlQuery query(db);
        query.prepare("SELECT registration, model, typecode, serialnumber, linenumber,"
                             "icaoaircrafttype, operator, operatorcallsign, operatoricao,"
                             "operatoriata, owner, testreg, registered, reguntil, status,"
                             "built, firstflightdate, seatconfiguration, engines, modes, "
                             "adsb, acars, notes, categorydescription, manufacturericao, manufacturername FROM aircraft WHERE icao24=?");
        query.bindValue(0, m_icao24);

        if( !query.exec() ) {
            qDebug("Error occurred querying.");
            qDebug("%s.", qPrintable(db.lastError().text()));
            m_dataAvailable = false;
        }

        if(query.next()) {
            m_registration = query.value(0).toString();
            m_model = query.value(1).toString();
            m_typecode = query.value(2).toString();
            m_serialnumber = query.value(3).toString();
            m_linenumber = query.value(4).toString();
            m_icaoaircrafttype = query.value(5).toString();
            m_operator = query.value(6).toString();
            m_operatorcallsign = query.value(7).toString();
            m_operatoricao = query.value(8).toString();
            m_operatoriata = query.value(9).toString();
            m_owner = query.value(10).toString();
            m_testreg = query.value(11).toString();
            m_registered = query.value(12).toString();
            m_reguntil = query.value(13).toString();
            m_status = query.value(14).toString();
            m_built = query.value(15).toString();
            m_firstflightdate = query.value(16).toString();
            m_seatconfiguration = query.value(17).toString();
            m_engines = query.value(18).toString();
            m_modes = query.value(19).toString();
            m_adsb = query.value(20).toString();
            m_acars = query.value(21).toString();
            m_notes = query.value(22).toString();
            m_categorydescription = query.value(23).toString();
            m_manufacturericao = query.value(24).toString();
            m_manufacturername = query.value(25).toString();
            m_dataAvailable = true;
        } else {
            m_registration = m_model = m_typecode = m_serialnumber = m_linenumber
                = m_icaoaircrafttype = m_operator = m_operatorcallsign = m_operatoricao = m_operatoriata
                = m_owner = m_testreg = m_registered = m_reguntil = m_status = m_built = m_firstflightdate
                = m_seatconfiguration = m_engines = m_modes = m_adsb = m_acars = m_notes = m_categorydescription
                = m_manufacturericao = m_manufacturername
                = "";
            m_dataAvailable = false;
        }
        emit dataChanged();
    }
}

void AircraftData::operator=(const AircraftData &a)
{
    m_icao24 = a.m_icao24;
    m_model = a.m_model;
    m_registration = a.m_registration;
    target = a.target;
}

bool AircraftData::operator==(const AircraftData &a)
{
    return a.m_icao24 == m_icao24;
}

bool AircraftData::visible() const
{
    return target->visible();
}

void AircraftData::setVisible(bool visible)
{
    target->setVisible(visible);
    emit visibleChanged();
}
