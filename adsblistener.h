#ifndef ADSBLISTENER_H
#define ADSBLISTENER_H

#include <QObject>
#include <QMap>
#include <QTcpSocket>
#include <QDataStream>
#include <QQmlListProperty>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include "adsbtarget.h"

// The interval between gain changes to read ownship altitude, in seconds
#define ADSBLISTENER_OWNSHIP_GAIN_INTERVAL  30
// The maximum time to wait for a transmission from ownship after the gain was changed to listen to it
#define ADSBLISTENER_OWNSHIP_GAIN_WAIT      10
// The gainIndex for the gain we want to set to listen to ownship
#define ADSBLISTENER_OWNSHIP_GAIN_INDEX     4

class AdsbListener : public QObject
{
    Q_OBJECT

public:
    AdsbListener();
    AdsbListener(QObject*);
    AdsbListener(QString hostname, uint port);
    QMap<QString, AdsbTarget *> &data();
    AdsbTarget *getTarget(QString address);

    void setActive(bool value);
    bool active();
    void setPort(uint);
    uint port();
    void setHostname(QString);
    QString hostname();
    void setOwnshipString(QString);
    QString ownshipString();
    bool ownshipAltitudeAvailable();
    qint32 getOwnshipAltitudeFt();
    bool gainSettingAvailable();
    int gainSettings();
    QList<int> getGains() const;
    int getGainValue(int index) const;
    void setGain(int gain);
    int getActualGain() const;
    int getNormalGain() const;
    int getOwnshipGain() const;
    void setGainIndex(int);
    int getGainIndex() const;

    AdsbTarget *getOwnshipTarget() const;

    bool getOwnshipGainUpdate() const;
    void setOwnshipGainUpdate(bool ownshipGainUpdate);

    int getOwnshipGainIndex() const;
    void setOwnshipGainIndex(int ownshipGainIndex);

    int getNormalGainIndex() const;
    void setNormalGainIndex(int normalGainIndex);

    bool isRadioAvailable() const;

    int getDebugLevel() const;
    void setDebugLevel(int debugLevel);
    void clearDebugFile();

signals:    
    void targetAdded(AdsbTarget*);
    void targetLost(AdsbTarget*);
    void ownshipAvailable();
    void ownshipAltitudeChanged();
    void ownshipUpdated();
    void gainChanged();
    void ownshipGainAdjustChanged();
    void normalGainChanged();
    void ownshipGainChanged();
    void dataReceived(QString);
    void debugLevelChanged();
    void activeChanged(bool);

protected:
    void readAdsb();
    void socketError();

protected slots:
    void expireTarget(AdsbTarget *target);

private:
    void listenForOwnship();
    QMap<QString, AdsbTarget *> m_map;

    QString m_hostname, m_ownship;
    AdsbTarget *m_ownshipTarget;
    uint m_port;
    QTcpSocket *socket;
    QDataStream in;
    bool m_active, m_ownshipGainUpdate;
    int m_debugLevel = 0;
    QFile *m_file;

    QList<int> m_gains;
    int m_gainsCount = -1, m_gainIndex, m_ownshipGainIndex, m_normalGainIndex;

    QTimer *m_ownshipGainTimer, *m_ownshipGainTimeout, m_reconnectTimer;
    void listenTimeout();
    void resetOwnshipTimer();
    void resetOwnshipTimerDueToUpdate();
};

#endif // ADSBLISTENER_H
