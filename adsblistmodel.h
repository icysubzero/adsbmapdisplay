#ifndef ADSBLISTMODEL_H
#define ADSBLISTMODEL_H

#include <QAbstractListModel>
#include "adsblistener.h"

class AdsbListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    AdsbListModel(QObject *parent);

    enum MarkerRoles{positionRole = Qt::UserRole + 1, dataRole = Qt::UserRole + 2};

    Q_INVOKABLE void addAdsbMessage(const AdsbTarget msg);
    void removeAdsbMessage(const AdsbTarget msg);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    QList<AdsbTarget> m_geolist;
    AdsbListener *m_listener;
};

#endif // ADSBLISTMODEL_H
