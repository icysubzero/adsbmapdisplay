#ifndef ADSBCOMMANDLINEARGS_H
#define ADSBCOMMANDLINEARGS_H

#include <QObject>
#include <QGeoCoordinate>

class AdsbCommandLineArgs : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString hostname READ hostname CONSTANT)
    Q_PROPERTY(uint port READ port CONSTANT)
    Q_PROPERTY(QString chartURL READ chartURL CONSTANT)
    Q_PROPERTY(int chartIndex READ chartIndex CONSTANT)
    Q_PROPERTY(int rangeCircles READ rangeCircles CONSTANT)
    Q_PROPERTY(QString database READ database CONSTANT)
    Q_PROPERTY(QString gps READ gps CONSTANT)
    Q_PROPERTY(QString ownship READ ownship CONSTANT)
    Q_PROPERTY(bool brightness READ brightness CONSTANT)
    Q_PROPERTY(uint maxBrightness READ maxBrightness CONSTANT)
    Q_PROPERTY(uint initialBrightness READ initialBrightness CONSTANT)
    Q_PROPERTY(bool radio READ radio CONSTANT)
    Q_PROPERTY(bool zoomButtons READ zoomButtons CONSTANT)
    Q_PROPERTY(bool noGL READ noGL CONSTANT)
    Q_PROPERTY(bool updateCharts READ updateCharts CONSTANT)
    Q_PROPERTY(bool wifiChooser READ wifichooser CONSTANT)
    Q_PROPERTY(uint initialZoomLevel READ initialZoomLevel CONSTANT)
    Q_PROPERTY(QGeoCoordinate pos READ pos CONSTANT)
    Q_PROPERTY(bool showDebugButton READ showDebugButton CONSTANT)

public:
    explicit AdsbCommandLineArgs(QObject *parent = nullptr);

    void setHostname(QString);
    void setPort(uint);
    void setChartURL(QString);
    QString hostname();
    uint port();
    QString chartURL();
    int chartIndex();
    void setChartIndex(int);
    int rangeCircles();
    void setRangeCircles(int);
    QString database();
    void setDatabase(QString);
    QString ownship();
    void setOwnship(QString);
    QString gps() const;
    void setGps(const QString &gps);
    void setBrightness(bool);
    bool brightness();
    void setMaxBrightness(uint);
    uint maxBrightness();
    void setInitialBrightness(uint);
    uint initialBrightness();
    void setRadio(bool);
    bool radio();
    void setZoomButtons(bool);
    bool zoomButtons();
    void setNoGL(bool);
    bool noGL();
    void setUpdateCharts(bool);
    bool updateCharts();
    void setLatLong(QString);
    QGeoCoordinate pos();
    void setWifichooser(bool);
    bool wifichooser() const;

    uint initialZoomLevel() const;
    void setInitialZoomLevel(const uint &initialZoomLevel);

    bool showDebugButton() const;
    void setShowDebugButton(bool showDebugButton);

private:
    QString m_hostname, m_chartURL, m_database, m_gps, m_ownship;
    uint m_port, m_maxBrightness, m_initialBrightness, m_initialZoomLevel;
    int m_chartindex, m_range;
    bool m_brightness, m_radio, m_zoomButtons, m_nogl, m_updateCharts, m_wifichooser, m_showDebugButton;
    QGeoCoordinate m_pos;
};

#endif // ADSBCOMMANDLINEARGS_H
