import QtQuick 2.0
import QtQuick.Window 2.12
import QtPositioning 5.12
import QtLocation 5.12
import AdsbListenerModel 1.0
import Settings 1.0
import QtGraphicalEffects 1.0

// Create a map object using our plugin and connected to our ADS-B listener
// The listener is an AdsbListenerModel object registered under "adsbListenerModel"

Map {
    id: mapview    

    property Settings settings
    property string charthostname
    property alias mypos: gpsPos.position
    property string nmeasource
    property alias trackUp: gpsPos.trackUp
    //property alias bearing: mapview.bearing
    //property alias tilt: mapview.tilt
    readonly property int minDeclutter: 1
    readonly property int maxDeclutter: 3
    property bool noGL: false
    property bool noChart: false
    property var myPosition: settings.defaultCenter
    property var autocenter: settings.autocenter    

    property int myAltitude: adsbListenerModel.ownshipAltitudeAvailable ? adsbListenerModel.ownshipAltitudeFt
                                                                        : ( gpsPos.position.altitudeValid
                                                                             ? gpsPos.position.coordinate.altitude * 3.82084
                                                                             : settings.defaultCenter.altitude * 3.82084 )

    property string altitudeSource: adsbListenerModel.ownshipAltitudeAvailable ? "ADS-B"
                                                                               : gpsPos.position.altitudeValid ? "GPS" : "?"


    // Create plugin to read charts from the local chart server
    // installed on our Pi where we put our FAA offline charts
    // NOTE: Don't forget the trailing "/" in the URL of the PluginParameter

    // If a local chart server is not available, simply remove the PluginParameter lines
    // in this section AND the activeMapType parameter right after the Plugin section
    // to use the standard OpenStreetMap server.

    color: 'black'
    zoomLevel: settings.zoomLevel
    center: settings.defaultCenter

    plugin: Plugin {
        name: "osm"
        PluginParameter { name: "osm.mapping.custom.host"; value : mapview.charthostname }
        // The memory and disk caches somehow survive component instantiations so we don't
        // want too much caching because it confuses the osm plugin when the chart is changed.
        PluginParameter { name: "osm.mapping.cache.disk.cost_strategy"; value : "unitary" }
        PluginParameter { name: "osm.mapping.cache.disk.size"; value : 0}
        PluginParameter { name: "osm.mapping.cache.memory.cost_strategy"; value : "unitary" }
        PluginParameter { name: "osm.mapping.cache.memory.size"; value : 10}
        PluginParameter { name: "osm.mapping.providersrepository.disabled"; value: true}
    }

    gesture.acceptedGestures: (settings.autocenter !== 1 )? MapGestureArea.PinchGesture | MapGestureArea.TiltGesture :
                                                            MapGestureArea.PinchGesture | MapGestureArea.PanGesture | MapGestureArea.TiltGesture | MapGestureArea.FlickGesture
    // We disabled MapGestureArea.RotationGesture because we could not find a valid use case for needing it

    activeMapType: supportedMapTypes[supportedMapTypes.length - 1]

    onBearingChanged: {
        if(!trackUp) bearing = 0;
    }

    function distance(item) {
        var coord = mapview.toCoordinate(item.mapToItem(mapview,-item.anchors.margins,-item.anchors.margins))
        var dist = myPosition.distanceTo(coord) / 1852
        return dist.toFixed(dist < 5 ? 1 : 0)
    }

    function updateScale() {
        if(noGL) {
            outerText.text = distance(outerText) + "nm"
            innerText.text = distance(innerText) + "nm"
        }
    }

    function recenterMap() {
        if(settings.autocenter === 2) {
            center = mypos.coordinate
        } else if(settings.autocenter === 3) {
            // Find vertical size of map
            var dist = toCoordinate(Qt.point(0,0)).distanceTo(toCoordinate(Qt.point(0,height)))
            // Offset center by a little less than one half the size of the map in the direction of travel
            // so the center of the reticle will be close to the bottom of the screen
            center = mypos.coordinate.atDistanceAndAzimuth(dist / 2.5, gpsPos.getTrack())
        }
    }

    onZoomLevelChanged: {
        updateScale()
        if(settings.zoomLevel !== zoomLevel)
            settings.zoomLevel = zoomLevel

        // Changes in zoom level can affect the chart's
        // center so if an autocenter setting is selected, it needs to be applied
        // after the zoom level change.
        recenterMap()
    }

    onAutocenterChanged: {
        recenterMap()
    }

    // Show reticle around our own position using MapCircles if GL is available
    MapItemGroup {
        id: myposition
        visible: settings.rangecircles !== 3

        property variant coordinate: myPosition
        property color rangeCircleColor: noChart ? settings.circleColor : settings.circleMapColor
        property int rangeCircleWidth: noChart ? 2 : 4

        MapPolyline {
            visible: !mapview.noGL && (settings.rangecircles === 1)
            line.width: myposition.rangeCircleWidth
            line.color: myposition.rangeCircleColor
            path:[
                { latitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,270+gpsPos.getTrack()).latitude,
                  longitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,270+gpsPos.getTrack()).longitude },
                { latitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,90+gpsPos.getTrack()).latitude,
                  longitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,90+gpsPos.getTrack()).longitude }
            ]
        }
        MapPolyline {
            visible: !mapview.noGL && (settings.rangecircles === 1)
            line.width: myposition.rangeCircleWidth
            line.color: myposition.rangeCircleColor
            path:[
                { latitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,0+gpsPos.getTrack()).latitude,
                  longitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,0+gpsPos.getTrack()).longitude },
                { latitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,180+gpsPos.getTrack()).latitude,
                  longitude: myposition.coordinate.atDistanceAndAzimuth(23*1852,180+gpsPos.getTrack()).longitude }
            ]
        }

        MapCircle {
            visible: !mapview.noGL && (settings.rangecircles === 1)
            center: myposition.coordinate
            radius: 3*1852
            border.width: myposition.rangeCircleWidth
            border.color: myposition.rangeCircleColor
        }
        MapCircle {
            visible: !mapview.noGL && (settings.rangecircles === 1)
            center: myposition.coordinate
            radius: 10*1852
            border.width: myposition.rangeCircleWidth
            border.color: myposition.rangeCircleColor
        }
        MapCircle {
            visible: !mapview.noGL && (settings.rangecircles === 1)
            center: myposition.coordinate
            radius: 20*1852
            border.width: myposition.rangeCircleWidth
            border.color: myposition.rangeCircleColor
        }

        // Show a triangle indicating ground track direction
        MapQuickItem {
            visible: !mapview.noGL && (settings.rangecircles === 1)
            coordinate: QtPositioning.coordinate(myposition.coordinate.atDistanceAndAzimuth(23*1852,0+gpsPos.getTrack()).latitude,
                         myposition.coordinate.atDistanceAndAzimuth(23*1852,0+gpsPos.getTrack()).longitude)
            anchorPoint.x: 10
            anchorPoint.y: 20 * Math.cos(tilt / 180 * Math.PI ) / 2
            rotation: gpsPos.getTrack()-mapview.bearing
            transform: Rotation { origin.x: 0; origin.y: 0; axis { x: 1; y: 0; z: 0} angle: mapview.tilt }
            sourceItem: Rectangle {
                width: 20
                height: 20 //* Math.cos(tilt / 180 * Math.PI )
                color: 'transparent'
                transformOrigin: Item.Center
                Canvas {
                    anchors.fill: parent
                    onPaint:{
                        var context = getContext("2d");
                        // the triangle
                        context.beginPath();
                        context.moveTo(parent.width / 2, 0);
                        context.lineTo(parent.width, parent.height);
                        context.lineTo(0, parent.height);
                        context.closePath();

                        // the fill color
                        context.fillStyle = myposition.rangeCircleColor;
                        context.fill();
                    }
                }
            }
        }

        // If GL is not available, overlay regular Quick items on top of the map to show range
        MapQuickItem {            
            coordinate: myposition.coordinate
            anchorPoint.x: myPosItem.width/2
            anchorPoint.y: myPosItem.height/2

            sourceItem: Item {
                property int h : Window.width > Window.height ? Window.width : Window.height
                property color c: 'lime'
                Rectangle {
                    visible: mapview.noGL && (settings.rangecircles === 1)
                    anchors.centerIn: myPosItem
                    border.color: myposition.rangeCircleColor
                    border.width: myposition.rangeCircleWidth
                    height: parent.h * 0.2
                    width: height
                    radius: height / 2
                    color: 'transparent'
                    Text {
                        id: innerText
                        anchors.left: parent.right
                        anchors.top: parent.verticalCenter
                        anchors.margins: 3
                        color: parent.border.color
                        font.pointSize: textSize
                        font.bold: true
                    }
                }                
                Rectangle {
                    visible: mapview.noGL && (settings.rangecircles === 1)
                    anchors.centerIn: myPosItem
                    border.color: myposition.rangeCircleColor
                    border.width: myposition.rangeCircleWidth
                    height: parent.h * 0.45
                    width: height
                    radius: height / 2
                    color: 'transparent'
                    Text {
                        id: outerText
                        anchors.left: parent.right
                        anchors.top: parent.verticalCenter
                        anchors.margins: 3
                        color: parent.border.color
                        font.pointSize: textSize
                        font.bold: true
                    }
                }
                Rectangle {
                    id: verticalLine
                    visible: mapview.noGL && (settings.rangecircles === 1)
                    anchors.centerIn: myPosItem
                    border.color: myposition.rangeCircleColor
                    border.width: myposition.rangeCircleWidth
                    height: parent.h*0.5
                    width: 1
                    color: 'transparent'
                }
                Rectangle {
                    visible: mapview.noGL && (settings.rangecircles === 1)
                    anchors.centerIn: myPosItem
                    border.color: myposition.rangeCircleColor
                    border.width: myposition.rangeCircleWidth
                    width: parent.h*0.5
                    height: 1
                    color: 'transparent'
                }
                Rectangle {
                    visible: mapview.noGL && (settings.rangecircles === 1)
                    width: parent.h / 40
                    height: parent.h / 40
                    anchors.horizontalCenter: verticalLine.horizontalCenter
                    anchors.bottom: verticalLine.top
                    color: 'transparent'
                    Canvas {
                        anchors.fill: parent
                        onPaint:{
                            var context = getContext("2d");

                            // the triangle
                            context.beginPath();
                            context.moveTo(parent.width / 2, 0);
                            context.lineTo(parent.width, parent.height);
                            context.lineTo(0, parent.height);
                            context.closePath();

                            // the fill color
                            context.fillStyle = myposition.rangeCircleColor;
                            context.fill();
                        }
                    }
                }

                Rectangle {
                    id: myPosItem
                    visible: settings.rangecircles === 2
                    width: 16
                    height: width
                    radius: width/2
                    border.color: 'black'
                    border.width: 2
                    color: 'red'
                }
            }
        }
    }

    // This component gets a notification each time something happens to an
    // ADS-B target and draws an airplane icon with a text label in the right place
    Component {
        id: drawAdsbTarget        

        MapQuickItem {
            id: marker

            // The test for !!adsbdata checks that the underlying data object is valid
            // (i.e. non-null and non-undefined). We found that when beginResetModel()
            // is called on the AbstractListModel containing all the targets, the adsbdata
            // object was reported as undefined once at each press of the declutter button.
            visible: !!adsbdata && adsbdata.visible &&
                      (settings.altitudeFilter === 0
                         || Math.abs(adsbdata.altitudeFt - mapview.myAltitude) < 5000 * settings.altitudeFilter )
                     && !(adsbdata.onGround && settings.declutter < maxDeclutter)

            property var position: adsbdata.coords

            anchorPoint.x: image.width/2
            anchorPoint.y: image.height/2            

            // This animation allows moving the aircraft from the last known position at
            // its last known speed along its last known track
            CoordinateAnimation {
                id:anim
                target: marker
                property: "coordinate"
            }

            onPositionChanged: {

                txt.text = adsbListenerModel.getLabel(adsbdata.address, myPosition, myAltitude, settings.declutter)
                // Update airplane icon rotation to match ground track
                image.rotation = adsbdata.groundTrack - mapview.bearing

                // Animate airplane so it starts moving on the map to match its ground speed and track
                // unless we don't have GL and there are more than 15 targets to display. The animations killed
                // performance one time in NYC when over 160 targets were being tracked and animated on a non-GL device

                if(!params.noGL || adsbListenerModel.targetsTracked <= 15) {
                    if((adsbdata.coords.latitude !== anim.from.latitude) || (adsbdata.coords.longitude !== anim.from.longitude)) {
                        anim.stop()
                        anim.from = adsbdata.coords
                        anim.to = adsbdata.coordsIn60sec
                        anim.duration = 60000
                        anim.start()
                    }
                } else {
                    coordinate = adsbdata.coords
                }

                // Make airplanes that are not on the ground and that are within a predefined distance and altitude
                // from our position blink
                if(!adsbdata.onGround) {
                    if(Math.abs(adsbdata.altitudeFt - myAltitude) <= settings.altAlert) {
                        if(adsbdata.coords.distanceTo(myPosition) / 1852.0 <= settings.distAlert) {                            
                            blink.start()                            
                        } else {
                            blink.stop()
                            image.opacity = 1
                        }
                    } else {
                        blink.stop()
                        image.opacity = 1
                    }
                }
            }

            // Draw a little airplane icon centered on the coordinates of the target and with a
            // text label inside a rectangle box with rounded corners
            sourceItem: Item {
                id: item

                Image {
                    id: image
                    source: noChart ? "images/airplane30y.png" : "images/airplane30.png"
                    z: 2
                    scale: settings.fontsize / 13.0
                    visible: noGL // We display a ColorOverlay of the image instead on GL displays
                    // As NoGL displays don't support tilt, this transform is not required here
                    // transform: Rotation { origin.x: 0; origin.y: height / 2; axis { x: 1; y: 0; z: 0} angle: mapview.tilt }

                    // Bring attention on new targets by rotating them slowly
                    // from pointing north to pointing along their track
                    // NOTE: disabled due to performance concerns on non-GL devices
                    // Behavior on rotation { RotationAnimation { duration: 1000; direction: RotationAnimation.Shortest } }

                    // Make planes blink when within a certain distance and/or altitude (see code in onPositionChanged block)
                    SequentialAnimation on opacity {
                        id: blink                        
                        loops: Animation.Infinite                       
                        PropertyAnimation { to: 1; duration: 0 }
                        PauseAnimation { duration: 750 }
                        PropertyAnimation { to: 0; duration: 500 }                                                
                    }
                    onOpacityChanged: overlay.opacity = opacity
                }

                ColorOverlay {
                    id: overlay
                    visible: !noGL
                    anchors.fill: image
                    source: image
                    scale: image.scale
                    rotation: image.rotation
                    transform: Rotation { origin.x: 0; origin.y: height / 2; axis { x: 1; y: 0; z: 0} angle: mapview.tilt }
                    z: image.z                    
                    color: noChart ? settings.acNoChartColor : settings.acOverChartColor                    
                }

                Image {
                    id: outline
                    visible: !noGL
                    source: "images/airplane-outline30.png"
                    z: image.z
                    scale: image.scale
                    rotation: image.rotation
                    transform: Rotation { origin.x: 0; origin.y: height / 2; axis { x: 1; y: 0; z: 0} angle: mapview.tilt }
                }

                Rectangle {
                    id: rectLabel
                    z: 1
                    color: noChart ? 'transparent' : 'white'
                    radius: 5
                    border.width : 1
                    border.color: noChart ? 'transparent' : 'black'
                    anchors.bottom: image.top
                    width: txt.width + 6
                    height: txt.height + 6
                    Text {
                        id: txt
                        anchors.left: parent.left
                        anchors.topMargin: 1
                        anchors.leftMargin: 3
                        anchors.top: parent.top
                        font.pixelSize: settings.fontsize
                        font.bold: true
                        color: noChart ? 'white' : 'black'                        
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(aircraftData.enabled && (aircraftData.aircraft.icao24 === adsbdata.aircraft.icao24))
                                    aircraftData.enabled = false
                            else {
                                aircraftData.adsbdata = adsbdata
                                aircraftData.aircraft = adsbdata.aircraft
                                aircraftData.enabled = true
                            }
                        }
                    }
                }                
            }
        }
    }

    // Show airplane tracks
    MapItemView {
        id: tracks
        model: adsbListenerModel
        delegate: MapPolyline {
            line.width: 4
            line.color: settings.trackColor
            visible: !!adsbdata && !noGL && settings.tracksVisible && adsbdata.visible && (settings.altitudeFilter === 0
                          || Math.abs(adsbdata.altitudeFt - mapview.myAltitude) < 5000 * settings.altitudeFilter )
            path: track
        }
    }

    // Draw a projected track showing where the planes will be in 1 minute
    MapItemView {
        id: projectedTrackLine
        model: adsbListenerModel
        delegate: MapPolyline {
            line.width: 4
            line.color: settings.projectedTrackColor

            path: projectedTrack
        }
    }

    // Show airplanes with labels
    MapItemView{
        model: adsbListenerModel
        delegate: drawAdsbTarget
    }

    // Add a fixed window to display detailed aircraft data
    AircraftDataList {
        id: aircraftData
        maxHeight: parent.height
        listener: adsbListenerModel
    }

    // Show north indicator
    Image {
        id: northIndicator
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 8
        scale: 0.7
        source: "images/north50.png"
        rotation: -mapview.bearing        
    }

    // Show the gps indicator
    Image {
        id: gpsvalidIndicator
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: -8
        scale: 0.5       
        source: gpsPos.positionValid && gpsPos.position.altitudeValid ? "images/gpsvalid.png"
                                                                      : gpsPos.positionValid ? "images/gpsinvalidalt.png"
                                                                                             : "images/gpsinvalid.png"
    }

    // Show the radio listener indicator
    Image {
        id: radioValidIndicator
        anchors.right: parent.right
        anchors.top: gpsvalidIndicator.bottom
        anchors.margins: -8
        scale: 0.5
        source: adsbListenerModel.listenerActive ? "images/radiovalid.png" : "images/radioinvalid.png"        
    }

    // Get our own position from a GPS source so we can center the map on ourselves
    // and calculate relative altitudes to other aircraft
    PositionSource {
        id: gpsPos
        active: true
        property int autocenter: settings.autocenter
        // This property indicates whether we have a valid position. Sometimes,
        // incorrectly valid positions with lat=long=0 are reported so we exclude
        // that as a valid position
        property bool positionValid: position.latitudeValid && position.longitudeValid && !((position.latitude === 0) && (position.longitude !== 0))
        property bool trackUp: !settings.northUp
        property int altitudeFt: position.coordinate.altitude * 3.28084
        property int lastValidTrack: 0

        // If nmeasource starts with a /, we assume it is a serial device and
        // we set name to serialnmea and a serialport parameter to the device name.
        // Otherwise we set the name parameter to the URL to the GPSD daemon
        name: mapview.nmeasource.substring(0, 1) === "/" ? "serialnmea" : mapview.nmeasource
        PluginParameter { name: "serialnmea.serial_port"; value: mapview.nmeasource }

        onPositionChanged: {
            if(positionValid) {
                myPosition = position.coordinate
                mapview.recenterMap()

                myPosItem.rotation = getTrack()
                if( trackUp && position.speedValid && (position.speed > 2) )
                    mapview.bearing = getTrack()
            }
            mapview.updateScale()
        }

        onAutocenterChanged: {
            if(positionValid) {
                mapview.recenterMap()
            }
        }
        onTrackUpChanged: {
            mapview.bearing = trackUp ? getTrack() : 0;
        }

        function getTrack() {
            if(position.speedValid && (position.speed > 2))
                return lastValidTrack = position.directionValid ? position.direction : 0;
            else
                return lastValidTrack
        }
    }
}
