#include "adsbtarget.h"
#include <QDebug>

QString AdsbTarget::messageType() const
{
    return m_messageType;
}

void AdsbTarget::setMessageType(const QString &value)
{
    m_messageType = value;
}

QString AdsbTarget::address() const
{
    return m_address;
}

void AdsbTarget::setAddress(const QString &value)
{
    m_address = value;
    m_aircraft->setICAO24(value);
}

QString AdsbTarget::callsign() const
{
    return m_callsign;
}

void AdsbTarget::setCallsign(const QString &value)
{
    m_callsign = value;
}

QString AdsbTarget::squawk() const
{
    return m_squawk;
}

void AdsbTarget::setSquawk(const QString &value)
{
    m_squawk = value;
}

quint8 AdsbTarget::msgType() const
{
    return m_msgType;
}

void AdsbTarget::setMsgType(const quint8 &value)
{
    m_msgType = value;
}

bool AdsbTarget::squawkChg() const
{
    return m_squawkChg;
}

void AdsbTarget::setSquawkChg(bool value)
{
    m_squawkChg = value;
}

bool AdsbTarget::squawkAlert() const
{
    return m_squawkAlert;
}

void AdsbTarget::setSquawkAlert(bool value)
{
    m_squawkAlert = value;
}

bool AdsbTarget::squawkIdent() const
{
    return m_squawkIdent;
}

void AdsbTarget::setSquawkIdent(bool value)
{
    m_squawkIdent = value;
}

bool AdsbTarget::onGround() const
{
    return m_onGround;
}

void AdsbTarget::setOnGround(bool value)
{
    m_onGround = value;
}

QTime AdsbTarget::recvTime() const
{
    return m_recvTime;
}

void AdsbTarget::setRecvTime(const QTime &value)
{
    m_recvTime = value;
}

QTime AdsbTarget::msgTime() const
{
    return m_msgTime;
}

void AdsbTarget::setMsgTime(const QTime &value)
{
    m_msgTime = value;
}

QDate AdsbTarget::recvDate() const
{
    return m_recvDate;
}

void AdsbTarget::setRecvDate(const QDate &value)
{
    m_recvDate = value;
}

QDate AdsbTarget::msgDate() const
{
    return m_msgDate;
}

void AdsbTarget::setMsgDate(const QDate &value)
{
    m_msgDate = value;
}

bool AdsbTarget::coordsValid() const
{
    return m_coords.isValid();
}

QGeoCoordinate AdsbTarget::coords() const
{
    return m_coords;
}

void AdsbTarget::setCoords(const QGeoCoordinate &value)
{
    m_coords = value;    
}

quint16 AdsbTarget::groundSpeed() const
{
    return m_groundSpeed;
}

void AdsbTarget::setGroundSpeed(const quint16 &value)
{
    m_groundSpeed = value;
}

quint16 AdsbTarget::groundTrack() const
{    
    return m_groundTrack;
}

void AdsbTarget::setGroundTrack(const quint16 &value)
{
    m_groundTrack = value;
}

double AdsbTarget::verticalRate() const
{
    return m_verticalRate;
}

qint32 AdsbTarget::altitudeFt() const
{
    double a = coords().altitude();
    // Test for NaN
    if(a != a)
        return a;
    else
        return a / 0.3048;
}

qint32 AdsbTarget::verticalRateFt() const
{
    return verticalRate() / 0.3048;
}

void AdsbTarget::setVerticalRate(const double &value)
{
    m_verticalRate = value;
}

QGeoCoordinate AdsbTarget::getCoordsInXsec(const int secs) const
{
    //qDebug() << coords().isValid() << " | " << coords() << " | " << coords().atDistanceAndAzimuth( groundSpeed() * 1852 / 3600.0 * secs, groundTrack(), coords().altitude() + verticalRate() );
    return coords().atDistanceAndAzimuth( groundSpeed() * 1852 / 3600.0 * secs, groundTrack(), coords().altitude() + verticalRate() );
}

QGeoCoordinate AdsbTarget::coordsIn60sec() const
{
    return getCoordsInXsec(60);
}

QGeoCoordinate AdsbTarget::coordsIn120sec() const
{
    return getCoordsInXsec(120);
}

QGeoCoordinate AdsbTarget::coordsIn180sec() const
{
    return getCoordsInXsec(180);
}

QGeoCoordinate AdsbTarget::estimatedCoords()
{
    return coords().atDistanceAndAzimuth( groundSpeed() * 0.0005144444 * msgTime().msecsTo(QTime::currentTime()),
                                          groundTrack() );
}

QList<QGeoCoordinate>& AdsbTarget::getTrack()
{
    return m_track;
}

quint32 AdsbTarget::updateCount() const
{
    return m_updateCount;
}

void AdsbTarget::setUpdateCount(const quint32 &updateCount)
{
    m_updateCount = updateCount;
}

uint AdsbTarget::timeSinceUpdate() const
{
    return ADSBMESSAGE_TIMEOUT - timer->remainingTime() / 1000;
}

bool AdsbTarget::timedOut()
{
    return m_timedOut;
}

AdsbTarget::AdsbTarget()
{
    m_msgType = 0;
    m_groundTrack = 0;
    m_groundSpeed = 0;
    m_verticalRate = 0;
    m_updateCount = 0;
    m_squawkChg = m_squawkAlert = m_squawkIdent = m_onGround = false;
    m_address = "";
    m_timedOut = false;
    m_visible = true;
    m_aircraft = new AircraftData(this);
    m_track.clear();

    timer = new QTimer(this);
    connect( timer, &QTimer::timeout, this, &AdsbTarget::timeout );
}

AdsbTarget::AdsbTarget(QString receivedString) : AdsbTarget()
{
    setUpdate( receivedString );
}

AdsbTarget::~AdsbTarget()
{
    delete m_aircraft;
}

QString AdsbTarget::extractAddress(QString receivedString)
{
    return receivedString.split(',')[4];
}

void AdsbTarget::setUpdate(QString receivedString)
{
    bool newCoords = false;

    this->receivedString = receivedString;

    QStringList list = this->receivedString.split(',');

    setMessageType(list[0]);
    setMsgType(list[1].toUInt());    
    setAddress(list[4]);
    setRecvDate(QDate::fromString(list[6], "yyyy/MM/dd"));
    setRecvTime(QTime::fromString(list[7], "H:mm:ss.zzz"));
    setMsgDate(QDate::fromString(list[8], "yyyy/MM/dd"));
    setMsgTime(QTime::fromString(list[9], "H:mm:ss.zzz"));

    if(list[10] != "" ) setCallsign(list[10]);
    if(list[11] != "" ) {
            m_coords.setAltitude(list[11].toDouble() * 0.3048);
            emit altitudeUpdated(); // Note that we don't care if the new altitude is the same as before.
                                    // We just want to know that a fresh altitude was received so we can stop
                                    // listening on a lower gain setting for our ownship if that's the one.
    }
    if(list[12] != "" ) setGroundSpeed(list[12].toUInt());
    if(list[13] != "" ) setGroundTrack(list[13].toUInt());

    if(list[14] != "" ) {
        m_coords.setLatitude(list[14].toDouble());
        newCoords = true;
    }
    if(list[15] != "" ) {
        m_coords.setLongitude(list[15].toDouble());
        newCoords = true;
    }

    // If coordinates changed, add the new position to the object's track
    if(newCoords) m_track << m_coords;

    if(list[16] != "" ) setVerticalRate(list[16].toDouble() * 0.3048 );
    if(list[17] != "" ) setSquawk(list[17]);
    if(list[18] != "" ) setSquawkChg(list[18] == "-1");
    if(list[19] != "" ) setSquawkAlert(list[19] == "-1");
    if(list[20] != "" ) setSquawkIdent(list[20] == "-1");
    if(list[21] != "" ) setOnGround(list[21] == "-1");

    // See if we've received our custom SBS message format that adds the control field at the end
    // This capability is enabled whenever the internal radio is used (i.e. with the -r parameter on the
    // command line)
    if(list.size()>22) {
        if(list[22] != "" )
            setControl(list[22].toUInt());
    } else setControl(8);

    m_updateCount++;
    m_timedOut = false;
    timer->start(ADSBMESSAGE_TIMEOUT * 1000);    

    emit adsbChanged(this);
}

QString AdsbTarget::getLastUpdate()
{
    return receivedString;
}

void AdsbTarget::timeout()
{
    m_timedOut = true;

    emit stale(this);
}

quint8 AdsbTarget::getControl() const
{
    return m_control;
}

void AdsbTarget::setControl(const quint8 &control)
{
    m_control = control;
}

bool AdsbTarget::isOwnship() const
{
    return m_isOwnship;
}

void AdsbTarget::setIsOwnship(bool isOwnship)
{
    m_isOwnship = isOwnship;
}

QString AdsbTarget::getControlText() const
{
    static QString msg[] = {    /* 0 */ "ADS-B ES/NT device with ICAO 24-bit address",
                                /* 1 */ "ADS-B ES/NT device with other address",
                                /* 2 */ "Fine format TIS-B",
                                /* 3 */ "Coarse format TIS-B",
                                /* 4 */ "TIS-B management message",
                                /* 5 */ "TIS-B relay of ADS-B message with other address",
                                /* 6 */ "ADS-B rebroadcast using DF-17 message format",
                                /* 7 */ "Reserved",
                                /* 8 */ "No control field received"};                    ;
    if(getControl() <= 8)
        return msg[getControl()];
    else
        return "Invalid control field value.";
}

bool AdsbTarget::visible() const
{
    return m_visible;
}

void AdsbTarget::setVisible(bool visible)
{
    m_visible = visible;    
    emit adsbChanged(this);
}

AircraftData *AdsbTarget::getAircraft() const
{
    return m_aircraft;
}
