#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QColor>
#include <QGeoCoordinate>
#include <QBuffer>
#include "charts/faachart.h"
#include "adsbcommandlineargs.h"

#define SETTINGS_DEFAULTZOOMLEVEL_IFR   10
#define SETTINGS_DEFAULTZOOMLEVEL_VFR   11

#define SETTINGS_FILENAME               "adsbreader.settings"

class Settings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString ownshipicao          MEMBER m_ownshipicao    NOTIFY ownshipChanged )
    Q_PROPERTY(int     zoomLevelIFR         MEMBER m_zoomLevelIFR   NOTIFY settingsChanged )
    Q_PROPERTY(int     zoomLevelVFR         MEMBER m_zoomLevelVFR   NOTIFY settingsChanged )
    Q_PROPERTY(QColor  circleColor          MEMBER m_circleColor    NOTIFY circleColorChanged )
    Q_PROPERTY(QColor  circleMapColor       MEMBER m_circleMapColor NOTIFY circleMapColorChanged )
    Q_PROPERTY(QColor  acNoChartColor       MEMBER m_acNoChartColor NOTIFY acNoChartColorChanged )
    Q_PROPERTY(QColor  acOverChartColor     MEMBER m_acOverChartColor NOTIFY acOverChartColorChanged )
    Q_PROPERTY(QColor  trackColor           MEMBER m_trackColor     NOTIFY trackColorChanged )
    Q_PROPERTY(QColor  projectedTrackColor  MEMBER m_projectedTrackColor NOTIFY projectedTrackColorChanged )

    Q_PROPERTY(int     projectedTrackMinutes MEMBER m_projectedTrackMinutes     NOTIFY projectedTrackMinutesChanged )

    Q_PROPERTY(int     chartIndex       MEMBER m_chartIndex     NOTIFY chartIndexChanged )
    Q_PROPERTY(int     autocenter       MEMBER m_autocenter     NOTIFY autocenterChanged )
    Q_PROPERTY(int     rangecircles     MEMBER m_rangecircles   NOTIFY rangecirclesChanged )
    Q_PROPERTY(bool    northUp          MEMBER m_northUp        NOTIFY northUpChanged )
    Q_PROPERTY(int     declutter        MEMBER m_declutter      NOTIFY declutterChanged )
    Q_PROPERTY(int     fontsize         MEMBER m_fontsize       NOTIFY fontsizeChanged )
    Q_PROPERTY(int     normalGainIndex  MEMBER m_gain           NOTIFY gainChanged )
    Q_PROPERTY(int     ownshipGainIndex MEMBER m_ownshipGain    NOTIFY ownshipGainChanged )
    Q_PROPERTY(bool    scanOwnship      MEMBER m_scanOwnship    NOTIFY scanOwnshipChanged )
    Q_PROPERTY(int     altitudeFilter   MEMBER m_altitudeFilter NOTIFY altitudeFilterChanged )
    Q_PROPERTY(float   zoomLevel        MEMBER m_zoomLevel      NOTIFY zoomLevelChanged )
    Q_PROPERTY(bool    tracksVisible    MEMBER m_tracksVisible  NOTIFY tracksVisibleChanged )
    Q_PROPERTY(int     brightness       MEMBER m_brightness     NOTIFY brightnessChanged )
    Q_PROPERTY(int     altAlert         MEMBER m_altAlert       NOTIFY altAlertChanged )
    Q_PROPERTY(int     distAlert        MEMBER m_distAlert      NOTIFY distAlertChanged )
    Q_PROPERTY(QGeoCoordinate defaultCenter MEMBER m_defaultCenter NOTIFY defaultCenterChanged )

    Q_PROPERTY(int     maxGainIndex     MEMBER m_maxGainIndex   NOTIFY settingsChanged)    


public:
    explicit Settings(QObject *parent = nullptr);
    static Settings *getInstance();
    Q_INVOKABLE bool saveSettings();
    Q_INVOKABLE bool loadSettings();
    Q_INVOKABLE void resetDefaults();
    Q_INVOKABLE void setRestorePoint();
    Q_INVOKABLE void rollbackChanges();

    void setUnsavedSettings(bool unsavedSettings);
    bool unsavedSettings() const;

    int getZoomLevel(FAAChart::chartType_t) const;

    void setDefaultValues(AdsbCommandLineArgs *args);

signals:
    void settingsChanged();
    void ownshipChanged();
    void circleColorChanged();
    void circleMapColorChanged();
    void acNoChartColorChanged();
    void acOverChartColorChanged();
    void trackColorChanged();
    void chartIndexChanged();
    void autocenterChanged();
    void rangecirclesChanged();
    void northUpChanged();
    void declutterChanged();
    void fontsizeChanged();
    void gainChanged();
    void ownshipGainChanged();
    void scanOwnshipChanged();
    void altitudeFilterChanged();
    void zoomLevelChanged();
    void defaultCenterChanged();
    void tracksVisibleChanged();
    void brightnessChanged();
    void altAlertChanged();
    void distAlertChanged();
    void projectedTrackColorChanged();
    void projectedTrackMinutesChanged();

private slots:
    void settingsChangedSlot();

private:

    QString     m_ownshipicao;

    int         m_zoomLevelIFR,
                m_zoomLevelVFR;

    bool        m_unsavedSettings,               
                m_northUp,
                m_scanOwnship,
                m_tracksVisible;

    int         m_chartIndex,
                m_autocenter,
                m_rangecircles,
                m_declutter,
                m_fontsize,
                m_altitudeFilter,
                m_gain,
                m_ownshipGain,                
                m_brightness,
                m_distAlert,
                m_altAlert,
                m_projectedTrackMinutes;


    float       m_zoomLevel;

    QColor      m_circleColor,
                m_circleMapColor,
                m_acNoChartColor,
                m_acOverChartColor,
                m_trackColor,
                m_projectedTrackColor;

    QGeoCoordinate m_defaultCenter;

    QBuffer     m_buffer;

    int         m_maxGainIndex;

    AdsbCommandLineArgs* m_args = nullptr;

    // Change this version number each time the settings properties are changed
    // in order to invalidate settings files with
    static const int m_version = 9;
};

#endif // SETTINGS_H
