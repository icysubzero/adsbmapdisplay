import QtQuick 2.0
import QtQuick.VirtualKeyboard 2.4
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12

Item {
    anchors.fill: parent
    signal closed()

    id: wifiDialog

    property string selectedNetwork: wifimanager.currentSSID
    property string password
    property bool hiddenNetwork: false
    property int connectSuccess: -1

    anchors.margins: 5

    ColumnLayout {
        anchors.fill: parent
        Text {
            Layout.margins: 5
            text: "WiFi Network Chooser"
            font.pointSize: textSize
            font.bold: true
            color: 'white'
        }

        Rectangle {
            id: wifichooserrect
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5

            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5
            ListView {
                id: wifilist
                model: wifimanager.networks
                spacing: 3
                anchors.fill: parent
                anchors.margins: 5
                clip: true
                currentIndex: wifimanager.networks.indexOf(wifiDialog.selectedNetwork)
                delegate: Item {
                    width: parent.width
                    height: 25
                    Rectangle {
                        border.width: 0
                        anchors.fill: parent
                        radius: 5
                        color: parent.ListView.isCurrentItem ? 'lightgreen' : 'transparent'
                        Text {
                            id: delegateText
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.margins: 5
                            text: modelData
                            font.pointSize: textSize
                            color: parent.parent.ListView.isCurrentItem ? 'black' : 'white'
                        }
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            wifilist.currentIndex = index
                            wifiDialog.selectedNetwork = modelData
                            wifiDialog.hiddenNetwork = (modelData === "Unknown/Hidden")
                        }
                    }
                }
            }
        }

        Rectangle {
            id: passwordrect
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5
            visible: false

            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5

            Flickable {
                id: scrollview
                anchors.fill: parent
                anchors.margins: 5
                contentWidth: col.width
                contentHeight: col.height

                ScrollBar.vertical: ScrollBar {
                    active: true
                }

                clip: true

                Column {
                    id:col
                    spacing: 2
                    width: parent.width

                    Text {
                        text: "Connection to " + wifiDialog.selectedNetwork
                        font.pointSize: textSize
                        font.bold: true
                        color: 'white'
                        height: 30
                    }

                    Text {
                        visible: wifiDialog.hiddenNetwork
                        text: "Network SSID"
                        font.pointSize: textSize
                        color: 'white'
                    }
                    TextField {
                        id: ssidfield
                        visible: wifiDialog.hiddenNetwork
                        placeholderText: "Specify WiFi SSID"
                        font.pointSize: textSize
                        //implicitHeight: 30
                        width: 250
                        Layout.preferredWidth: parent.width * 2/3
                        onActiveFocusChanged: {
                            if( focus ) {
                                scrollview.contentY = 30
                            }
                        }
                        Keys.onReturnPressed: { passwordfield.forceActiveFocus() }
                    }

                    Text {
                        id: passwordlabel
                        text: "WPA2 password"
                        color: 'white'
                        font.pointSize: textSize
                    }
                    TextField {
                        id: passwordfield
                        text: ''
                        font.pointSize: textSize
                        //implicitHeight: 30
                        Layout.preferredWidth: parent.width * 2/3
                        placeholderText: "Leave empty if open"
                        width: 250
                        onActiveFocusChanged: {
                            if( focus ) {
                                scrollview.contentY = scrollview.contentHeight - passwordfield.height - passwordlabel.height - col.spacing - 5
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: connectrect
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5
            visible: false

            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5

            Text {
                anchors.centerIn: parent
                text: wifiDialog.connectSuccess === 0
                      ? "Connection to " + wifiDialog.selectedNetwork + " successful."
                      : wifiDialog.connectSuccess === 1
                        ? "Unable to connect to " + wifiDialog.selectedNetwork + "."
                        : wifiDialog.connectSuccess === 2
                          ? wifiDialog.selectedNetwork + " can't reach internet."
                          : "An unknown error occurred";
                font.pointSize: 10
                color: 'white'
            }
        }

        RowLayout {
            id: page1buttons
            Layout.margins: 5
            Layout.alignment: Qt.AlignRight
            Layout.preferredHeight: 20
            Button {
                text: "Rescan"
                font.pointSize: buttonSize
                //implicitHeight: 20
                onClicked: {
                    wifimanager.scanWiFi()
                }
            }
            Button {
                text: "Next"
                font.pointSize: buttonSize
                enabled: wifilist.currentIndex != -1
                onClicked: {
                    wifichooserrect.visible = false
                    passwordrect.visible = true
                    page1buttons.visible = false
                    page2buttons.visible = true
                }
            }
            Button {
                text: "Cancel"
                font.pointSize: buttonSize
                onClicked: {
                    wifiDialog.visible = false
                    wifiDialog.closed()
                }
            }
        }

        RowLayout {
            id: page2buttons
            visible: false
            Layout.margins: 5
            Layout.alignment: Qt.AlignRight
            Layout.preferredHeight: 20
            Button {
                text: "Prev"
                font.pointSize: buttonSize
                onClicked: {
                    page1buttons.visible = true
                    page2buttons.visible = false
                    wifichooserrect.visible = true
                    passwordrect.visible = false
                }
            }
            Button {
                text: "Connect"
                font.pointSize: buttonSize
                onClicked: {
                    if(ssidfield.visible) wifiDialog.selectedNetwork = ssidfield.text
                    wifiDialog.password = passwordfield.text
                    wifiDialog.connectSuccess = wifimanager.connect(wifiDialog.selectedNetwork, wifiDialog.password)
                    page2buttons.visible = false
                    page3buttons.visible = true
                    passwordrect.visible = false
                    connectrect.visible = true
                }
            }
            Button {
                text: "Cancel"
                font.pointSize: buttonSize
                onClicked: {
                    wifiDialog.visible = false
                    wifiDialog.closed()
                }
            }
        }
        RowLayout {
            id: page3buttons
            visible: false
            Layout.margins: 5
            Layout.alignment: Qt.AlignRight
            Layout.preferredHeight: 20
            Button {
                text: "Prev"
                font.pointSize: buttonSize
                onClicked: {
                    page2buttons.visible = true
                    page3buttons.visible = false
                    passwordrect.visible = true
                    connectrect.visible = false
                }
            }
            Button {
                text: "Close"
                font.pointSize: buttonSize
                onClicked: {
                    wifiDialog.visible = false
                    wifiDialog.closed()
                }
            }
        }
    }
    Component.onCompleted: {
        Qt.inputMethod.hide()
    }
}
