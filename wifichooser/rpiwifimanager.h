#ifndef RPIWIFIMANAGER_H
#define RPIWIFIMANAGER_H

#include <QObject>

/* Sample /etc/wpa_supplicant/wpa_supplicant.conf file:

ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=CA

network={
    ssid="sdfkjhlksjh"
    psk="casdfkjlhasdf3"
    key_mgmt=WPA-PSK
}

*/


class RpiWifiManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString      currentSSID     READ currentSSID        NOTIFY ssidChanged)
    Q_PROPERTY(QStringList  networks        READ availableSSIDs     NOTIFY scanComplete)
    Q_PROPERTY(int          connectStatus   MEMBER m_connectStatus  NOTIFY connectAttempted)

public:
    explicit RpiWifiManager(QObject *parent = nullptr);

    Q_INVOKABLE void scanWiFi();
    Q_INVOKABLE QString testConnection();

    QStringList availableSSIDs() const;
    QString currentSSID() const;
    Q_INVOKABLE int connect(QString ssid, QString psk);

protected:
    void setCurrentSSID(const QString &currentSSID);

private:
    QString runShellCommand(QString cmd) const;
    int m_connectStatus = -1;

signals:
    void scanComplete();
    void ssidChanged();
    void connectAttempted();

private:
    QStringList m_availableSSIDs;
    QString m_currentSSID;

};

#endif // RPIWIFIMANAGER_H
