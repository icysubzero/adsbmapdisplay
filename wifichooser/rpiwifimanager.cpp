#include "rpiwifimanager.h"
#include <QDebug>
#include <QProcess>
#include <iostream>
#include <fstream>
#include <unistd.h>

RpiWifiManager::RpiWifiManager(QObject *parent) : QObject(parent)
{
}

void RpiWifiManager::scanWiFi() {

    m_currentSSID = runShellCommand("iwgetid -r");

    QString s = runShellCommand("sudo /sbin/iwlist wlan0 scan|grep -P -o '(?<=SSID:\\\").*(?=\\\")'");

    m_availableSSIDs = s.split('\n');

    bool unknownFound = false;
    for(QString& s : m_availableSSIDs ) {
        if( s == "" ) {
            s = "Unknown/Hidden";
            unknownFound = true;
        }
    }
    if(!unknownFound) m_availableSSIDs << "Unknown/Hidden";

    emit scanComplete();
}

QString RpiWifiManager::testConnection()
{
    return runShellCommand("a=`/usr/bin/wget -qO - http://flt.l5.ca/pi/test`;echo $a");
}

QStringList RpiWifiManager::availableSSIDs() const
{
    return m_availableSSIDs;
}

QString RpiWifiManager::currentSSID() const
{
    return m_currentSSID;
}

int RpiWifiManager::connect(QString ssid, QString psk)
{    
    const char *rpiconf = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n"
                          "update_config=1\n"
                          "country=CA\n"
                          "\n"
                          "network={\n";

    // Backup current wifi config
    runShellCommand("cp /etc/wpa_supplicant/wpa_supplicant.conf /tmp");

    // Create new WiFi configuration file for the Pi
    QString s(rpiconf);
    s.append("  ssid=\"" + ssid + "\"\n");
    s.append("  scan_ssid=1\n");
    if(psk != "") {
        s.append("  psk=\"" + psk + "\"\n");
        s.append("  key_mgmt=WPA-PSK\n");
    } else {
        s.append("  key_mgmt=NONE\n");
    }
    s.append("}\n");    

    // Write new config file
    std::ofstream myfile;
    myfile.open("/etc/wpa_supplicant/wpa_supplicant.conf");
    myfile << s.toStdString();
    myfile.close();

    // Reconfigure WiFi with new file
    s = runShellCommand("sudo wpa_cli -i wlan0 reconfigure");

    if(s.startsWith("FAIL")) {
        // Something went wrong. Restore previous config
        runShellCommand("cp /tmp/wpa_supplicant.conf /etc/wpa_supplicant/");
        runShellCommand("rm /tmp/wpa_supplicant.conf");
        s = runShellCommand("sudo wpa_cli -i wlan0 reconfigure");
        emit connectAttempted();
        return m_connectStatus = 1;
    }

    // Wifi connection was successful. Now check that we can access the Internet for 20 seconds.
    for(int i = 1; i < 20; i++ ) {
        sleep(1);
        s = runShellCommand("/usr/bin/wget -qO - http://flt.l5.ca/pi/test");

        if(s.startsWith("Success")) {
            runShellCommand("rm /tmp/wpa_supplicant.conf");
            emit connectAttempted();
            return m_connectStatus = 0;
        }
    }

    // Unable to access internet after 20 seconds. Restore previous config.
    runShellCommand("cp /tmp/wpa_supplicant.conf /etc/wpa_supplicant/");
    runShellCommand("rm /tmp/wpa_supplicant.conf");
    s = runShellCommand("sudo wpa_cli -i wlan0 reconfigure");
    emit connectAttempted();
    return m_connectStatus = 2;
}

void RpiWifiManager::setCurrentSSID(const QString &currentSSID)
{
    m_currentSSID = currentSSID;
}

QString RpiWifiManager::runShellCommand(QString cmd) const
{
    QProcess process;
    process.start("/bin/bash", QStringList() << "-c" << cmd);
    if(!process.waitForStarted())
        qWarning() << "Could not start process" << cmd;
    if(!process.waitForFinished())
        qWarning() << "Process failed: " << process.exitStatus() << cmd;

    QString result = process.readAllStandardOutput();
    if(result.right(1) == '\n') result.chop(1);
    return result;
}
