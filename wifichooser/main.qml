import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls.Universal 2.12

Window {
    id: window
    visible: true
    width: 800
    height: 480
    color: 'black'

    Universal.theme: Universal.Dark
    Universal.accent: Universal.Yellow

    Loader {
        anchors.fill: parent
        source: 'WifiChooserComponent.qml'
    }
 }
