#ifndef ADSBLISTENERMODEL_H
#define ADSBLISTENERMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "adsbtarget.h"
#include "adsblistener.h"
#include <QGeoCoordinate>

//TODO: remove duplication between AdsbListenerModel and AdsbListener
/*
 * It would be nice to just be able to inherit AdsbListener and QAbstractListModel
 * but Qt doesn't support multiple inheritance of QObject. And right now AdsbListener
 * requires inheritance from QObject because it uses signals and slots to notify
 * of changes to ADS-B targets. The benefits of using signals and slots in AdsbListener
 * probably outweigh the inconvenience of some duplication...
 */

class AdsbListenerModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool active READ active WRITE setActive);
    Q_PROPERTY(bool radioAvailable READ isRadioAvailable CONSTANT);
    Q_PROPERTY(QString hostname READ hostname WRITE setHostname);
    Q_PROPERTY(uint port READ port WRITE setPort);    
    Q_PROPERTY(QString ownship READ ownshipString WRITE setOwnshipString NOTIFY ownshipChanged);
    Q_PROPERTY(uint targetsTracked READ targetsTracked NOTIFY targetsChanged)
    Q_PROPERTY(bool ownshipAltitudeAvailable READ ownshipAltitudeAvailable NOTIFY ownshipAvailable)
    Q_PROPERTY(qint32 ownshipAltitudeFt READ ownshipAltitudeFt NOTIFY ownshipAltitudeFtChanged)
    Q_PROPERTY(QList<int> gains READ getGains CONSTANT)
    Q_PROPERTY(bool gainSettingAvailable READ gainSettingAvailable CONSTANT)
    Q_PROPERTY(int gain READ getActualGain WRITE setGain NOTIFY gainChanged)
    Q_PROPERTY(int ownshipGain READ getOwnshipGain NOTIFY ownshipGainChanged)
    Q_PROPERTY(int normalGain READ getNormalGain NOTIFY normalGainChanged)
    Q_PROPERTY(int gainIndex READ getGainIndex WRITE setGainIndex NOTIFY gainChanged)
    Q_PROPERTY(int ownshipGainIndex READ getOwnshipGainIndex WRITE setOwnshipGainIndex NOTIFY ownshipGainChanged)
    Q_PROPERTY(int normalGainIndex READ getNormalGainIndex WRITE setNormalGainIndex NOTIFY normalGainChanged)
    Q_PROPERTY(int projectedTrackMinutes READ getProjectedTrackMinutes WRITE setProjectedTrackMinutes NOTIFY projectedTrackMinutesChanged)

    Q_PROPERTY(bool ownshipGainAdjust READ getOwnshipGainAdjust WRITE setOwnshipGainAdjust NOTIFY ownshipGainAdjustChanged)
    Q_PROPERTY(int debugLevel READ getDebugLevel WRITE setDebugLevel NOTIFY debugLevelChanged)
    Q_PROPERTY(bool listenerActive READ getListenerActive NOTIFY activeChanged)

public:
    enum AdsbListenerModelRoles{positionRole    = Qt::UserRole + 1,
                                dataRole,
                                trackRole,
                                projectedTrackRole
                               };

    AdsbListenerModel();
    AdsbListenerModel(QObject*);
    AdsbListenerModel(AdsbListener *, QObject *); // Useful only in C++ for binding the model to an existing listener

    Q_INVOKABLE int getMaxGainIndex() const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void addPoint(AdsbTarget*);
    void removePoint(AdsbTarget*);
    QHash<int, QByteArray> roleNames() const override;

    QString hostname() const;
    void setHostname(QString);
    uint port() const;
    void setPort(uint);
    bool active() const;
    void setActive(bool);
    QString ownshipString();
    Q_INVOKABLE void setOwnshipString(QString);
    uint targetsTracked() const;
    bool ownshipAltitudeAvailable() const;
    qint32 ownshipAltitudeFt() const;

    Q_INVOKABLE void refreshAll();

    bool gainSettingAvailable() const;
    QList<int> getGains() const;
    int getActualGain() const;
    int getNormalGain() const;
    int getOwnshipGain() const;
    void setGain(int);
    int getGainIndex() const;
    void setGainIndex(int gainIndex);
    int getOwnshipGainIndex() const;
    void setOwnshipGainIndex(int gainIndex);
    int getNormalGainIndex() const;
    void setNormalGainIndex(int gainIndex);
    void setOwnshipGainAdjust(bool);
    bool getOwnshipGainAdjust() const;
    bool isRadioAvailable() const;
    int getDebugLevel() const;
    void setDebugLevel(int debugLevel);
    bool getListenerActive() const;
    void setProjectedTrackMinutes(int);
    int getProjectedTrackMinutes() const;

    Q_INVOKABLE QString getLabel(QString address, QGeoCoordinate position, int altitudeFt, int declutter) const;
    Q_INVOKABLE void clearDebugFile();


private:
    QList<AdsbTarget*> m_list;
    AdsbListener *m_listener;
    bool listenerCreated = false;
    int m_myAltitude, m_projectedTrackMinutes;

    void init();

public slots:
    void adsbChanged(AdsbTarget *);    

signals:
    void targetsChanged();
    void ownshipAvailable();
    void ownshipAltitudeFtChanged();
    void gainChanged();
    void ownshipGainChanged();
    void normalGainChanged();
    void ownshipGainAdjustChanged();
    void ownshipChanged();
    void dataReceived(QString s);
    void debugLevelChanged();
    void activeChanged(bool);
    void projectedTrackMinutesChanged();
};


#endif // ADSBLISTENERMODEL_H
