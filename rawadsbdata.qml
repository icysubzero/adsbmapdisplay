import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import AdsbListenerModel 1.0

Item {
    id: rawadsbdataItem
    anchors.fill: parent
    property AdsbListenerModel listener

    signal closed()

    Connections {
        target: listener
        function onDataReceived(s) {
            view.model.append({msg: s})
            view.positionViewAtEnd()
        }
    }

    ColumnLayout {
        anchors.fill: parent
        Text {
            Layout.margins: 5
            text: "Decoded ADS-B data"            
            font.bold: true
            color: 'white'
        }

        Rectangle {
            id: rawdatarect
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5
            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5

            ListView {
                id: view
                visible: rawmode.checked
                anchors.margins: 5
                anchors.fill: parent
                clip: true
                model: ListModel {}
                delegate:
                    Text {
                        width: rawdatarect.width
                        color: 'white'
                        text: msg
                        font.pointSize: textSize
                        wrapMode: Text.Wrap
                    }
            }
            ListView {
                id: targetlistrect
                visible: targetmode.checked
                anchors.margins: 5
                anchors.fill: parent
                clip: true
                spacing: 5
                model: listener
                delegate:
                    Rectangle {
                        id: rect
                        width: targetlistrect.width
                        height: layout.height + 10
                        color: adsbdata.squawkIdent ? '#880000' : adsbdata.onGround ? '#008800' : '#666666'
                        border.width: 2
                        border.color: 'white'
                        radius: 5

                        property var dataElements : [
                            { label: adsbdata.aircraft.exists ? 'Registration: ' : 'ICAO Address: ',
                                value: adsbdata.aircraft.exists ? adsbdata.aircraft.registration : adsbdata.address,
                                visible: true },
                            { label: 'Callsign: ',
                                value: adsbdata.callsign,
                                visible: true },
                            { label: 'Latitude: ',
                                value: isNaN(adsbdata.coords.latitude) ? 'N/A' : adsbdata.coords.latitude,
                                visible: true },
                            { label: 'Longitude: ',
                                value: isNaN(adsbdata.coords.longitude) ? 'N/A' : adsbdata.coords.longitude,
                                visible: true },
                            { label: 'Altitude: ',
                                value: isNaN(adsbdata.coords.altitude) ? 'N/A' : adsbdata.altitudeFt + ' ft.',
                                visible: true },
                            { label: 'Vertical rate: ',
                                value: adsbdata.verticalRateFt + ' fpm',
                                visible: true },
                            { label: 'Ground speed: ',
                                value: isNaN(adsbdata.groundSpeed) ? 'N/A' : adsbdata.groundSpeed + ' kts',
                                visible: true },
                            { label: 'Ground track: ',
                                value: isNaN(adsbdata.groundTrack) ? 'N/A' : adsbdata.groundTrack +'°',
                                visible: true },
                            { label: 'Manufacturer: ',
                                value: adsbdata.aircraft.manufacturericao,
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.manufacturericao !== '') },
                            { label: 'Model: ',
                                value: adsbdata.aircraft.model + ' (' + adsbdata.aircraft.typecode + ')',
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.model !== '') },
                            { label: 'Category: ',
                                value: adsbdata.aircraft.category,
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.category !== '')
                                         && !adsbdata.aircraft.category.startsWith('No ADS-B') },
                            { label: 'S/N: ',
                                value: adsbdata.aircraft.serialnumber,
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.serialnumber !== '') },
                            { label: 'Engines: ',
                                value: adsbdata.aircraft.engines,
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.engines !== '') },
                            { label: 'Owner: ',
                                value: adsbdata.aircraft.owner,
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.owner !== '') },
                            { label: 'Operator: ',
                                value: adsbdata.aircraft.oper,
                                visible: adsbdata.aircraft.exists && (adsbdata.aircraft.oper !== '') },
                            { label: 'Control field: ',
                                value: adsbdata.controlText,
                                visible: true },
                            { label: 'Last message: ',
                                value: adsbdata.lastMessage,
                                visible: true },

                        ];

                        Grid {
                            id: layout                            
                            anchors.top: parent.top
                            anchors.left: parent.left
                            width: parent.width
                            anchors.margins: 5
                            columns: 1//rect.width < 500 ? 1 : 2
                            spacing: 5                            
                            property int labelWidth: columns === 1 ? width * 0.4 - spacing : width * 0.2 - spacing
                            property int valueWidth: columns === 1 ? width * 0.6 : width * 0.3 - spacing

                            Repeater {
                                model: dataElements.length
                                Row {
                                    Text {
                                        width: layout.labelWidth
                                        color: 'white'
                                        font.pointSize: textSize
                                        text: dataElements[index].label
                                        horizontalAlignment: Text.AlignRight
                                        wrapMode: Text.Wrap
                                    }
                                    Text {
                                        width: layout.valueWidth
                                        font.pointSize: textSize
                                        color: 'white'
                                        text: dataElements[index].value
                                        wrapMode: Text.Wrap
                                    }
                                }
                            }
                        }
                    }
                }
        }
        GridLayout {
            Layout.margins: 5
            Layout.preferredHeight: 50
            Layout.fillWidth: true
            columns: 3

            ButtonGroup {
                buttons: adsbmode.children
            }
            Row {
                Layout.alignment: Qt.AlignLeft

                id: adsbmode

                RadioButton {
                    id: targetmode
                    checked: true                    
                    text: 'Tracked targets'
                    font.pointSize: buttonSize
                }
                RadioButton {
                    id: rawmode
                    text: 'Raw messages'                    
                    font.pointSize: buttonSize
                }
            }

            Text { Layout.fillWidth: true }

            Button {
                text: "Close"                
                font.pointSize: buttonSize
                onClicked: {
                    rawadsbdataItem.closed();
                }
            }
        }
    }
}
