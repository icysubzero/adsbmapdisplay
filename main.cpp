#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QSurfaceFormat>
#include <QFont>
#include "adsbtarget.h"
#include "adsblistener.h"
#include "adsblistmodel.h"
#include "adsblistenermodel.h"
#include "adsbcommandlineargs.h"
#include "charts/installedcharts.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <sys/stat.h>
#include "unistd.h"
#include "dump1090/dump1090.h"
#include <thread>
#include <memory>
#include <string>
#include "charts/availablecharts.h"
#include <QQuickStyle>
#include "wifichooser/rpiwifimanager.h"
#include "download/downloadandunzipitem.h"
#include "download/downloadmanager.h"
#include "aircraftdata/aircraftdata.h"
#include "aircraftdata/openskyupdater.h"
#include "settings.h"
#include "version.h"
#include "download/programupdater.h"

int main(int argc, char *argv[])
{
    // Required for virtual keyboard to work
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setApplicationName("ADS-B Traffic Display");
    QCoreApplication::setApplicationVersion(QString::number(ADSBREADER_BUILD));

    QGuiApplication app(argc, argv);    

    AdsbCommandLineArgs params;
    params.setHostname("localhost");
    params.setPort(30003);
    params.setChartURL("file:///var/www/html/"); // Was ("http://localhost/");
    params.setRangeCircles(1);
    params.setDatabase("aircraft.db");
    params.setGps("/dev/gps"); // Was ("gpsd://localhost:2947");
    params.setOwnship("");
    params.setChartIndex(0); // Default to No Chart
    params.setLatLong("45.5448189,-73.3272084");
    params.setWifichooser(false);
    params.setInitialZoomLevel(9);
    params.setRadio(true);

    // Check if Raspberry Pi display brightness control file exists and is writeable
    params.setBrightness(false);
    struct stat buffer;
    if( stat ("/sys/class/backlight/rpi_backlight/brightness", &buffer) == 0 ) {
        // Raspberry display brightness control file exists. Check that we can write to it.
        params.setBrightness((buffer.st_mode & S_IWOTH)
                          || (buffer.st_mode & S_IWGRP && (buffer.st_gid == getgid()))
                          || (buffer.st_mode & S_IWUSR && (buffer.st_uid == getuid())));
        // Read maximum supported brightness and current brightness
        if(params.brightness()) {
            char s[4];

            FILE *f = fopen("/sys/class/backlight/rpi_backlight/max_brightness", "r");
            fgets(s, 4, f);
            fclose(f);
            params.setMaxBrightness(atoi(s));

            f = fopen("/sys/class/backlight/rpi_backlight/brightness", "r");
            fgets(s, 4, f);
            fclose(f);
            params.setInitialBrightness(atoi(s));
        }
    } else {
        // Display brightness control file doesn't exist so disable brightness control
        params.setBrightness(false);
    }

    // Check if updateCharts.sh script is present in current program directory
    params.setUpdateCharts(false);
    std::string s(argv[0]), folder;
    int found = s.find_last_of("/\\");
    if(found == -1) {
        folder = '.';
    } else {
        folder = s.substr(0, found);
    }
    std::string script = folder + "/updateCharts.sh";
    params.setUpdateCharts(stat(script.c_str(), &buffer) == 0);


    QCommandLineParser parser;
    parser.setApplicationDescription("ADS-B Map Display v1.0");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOptions({
          {{"a", "adsbhostname"},
              QCoreApplication::translate("main", "When -r or --radio is specified, this specifies the host or IP address to connect to for listening to ADS-B messages. (default: localhost)"),
              QCoreApplication::translate("main", "hostname")},
          {{"p", "adsbport"},
              QCoreApplication::translate("main", "When -r or --radio is specified, this indicates the port number to connect to for listening to ADS-B messages. (default: 30003)."),
              QCoreApplication::translate("main", "port")},
          {{"r", "radio"},
              QCoreApplication::translate("main", "Listen for ADS-B messages on socket specified by --adsbhost and --adsbportname parameters instead of running the radio receiver within this program (default: disabled).")},
          {{"l", "latlong"},
              QCoreApplication::translate("main", "Initial center position in decimal lat,long format, e.g. 45.2314,-75.3 (will be overridden by GPS if present) (default: 45,-73 near Montreal, Canada)"),
              QCoreApplication::translate("main", "pos")},
          {{"c", "chartsurl"},
              QCoreApplication::translate("main", "Get charts from <chartsURL> (default: file:///var/www/html/)"),
              QCoreApplication::translate("main", "chartsURL")},
          {{"i", "chartindex"},
              QCoreApplication::translate("main", "Chart index of the chart to display at startup, or 0 for no chart. The index corresponds to the position in the Chart drop down menu on the main screen starting with the chart at the top of the list (default: 0)"),
              QCoreApplication::translate("main", "chartIndex")},
          {{"n", "reticle"},
              QCoreApplication::translate("main", "Reticle style. 1: full reticle, 2: own position only, 3: none (default: 1)"),
              QCoreApplication::translate("main", "reticle")},
          {{"d", "database"},
              QCoreApplication::translate("main", "Path to sqlite aircraft database file (default: aircraft.db)"),
              QCoreApplication::translate("main", "database")},
          {{"g", "gpsurl"},
              QCoreApplication::translate("main", "Device from which to read GPS data (default: /dev/gps)."),
              QCoreApplication::translate("main", "gps")},
          {{"o", "ownship"},
              QCoreApplication::translate("main", "ICAO hex address of our own plane (e.g. C01D8A) to suppress displaying it (default: none)"),
              QCoreApplication::translate("main", "hexaddress")},
          {{"b", "brightness"},
              QCoreApplication::translate("main", "Disable display brightness control (default: enabled if running on Raspberry Pi and display brightness control file is writeable; disabled otherwise)")},
          {{"x", "zoomlevel"},
              QCoreApplication::translate("main", "Sets the display zoom level at startup. This value should be within the zoom level values for which chart tiles were generated (default: 9)"),
              QCoreApplication::translate("main", "zoomlevel")},
          {{"z", "zoombuttons"},
              QCoreApplication::translate("main", "Display buttons for zooming in and out on the chart. Useful when using a mouse without a wheel or a touchscreen without multi-touch capability (default: disabled)")},
          {{"f", "nogl"},
              QCoreApplication::translate("main", "Specify that this device does not support OpenGL in order to suppress features that require it, such as airplane tracks")},
          {{"s", "debug"},
              QCoreApplication::translate("main", "Show a Debug button in the menu to enable recording of all received SBS messages in the debug.log file")},
          {{"w", "wifi"},
              QCoreApplication::translate("main", "Specify that this device supports the Raspberry Pi Wifi configuration functionality and display a Wifi network chooser button in the application")}
    });

    parser.process(app);
    if(parser.isSet("a")) params.setHostname(parser.value("a"));
    if(parser.isSet("p")) params.setPort(parser.value("p").toUInt());
    if(parser.isSet("c")) params.setChartURL(parser.value("c"));
    if(parser.isSet("i")) params.setChartIndex(parser.value("i").toUInt());
    if(parser.isSet("d")) params.setDatabase(parser.value("d"));
    if(parser.isSet("g")) params.setGps(parser.value("g"));
    if(parser.isSet("o")) params.setOwnship(parser.value("o"));
    if(parser.isSet("b")) params.setBrightness(false);
    if(parser.isSet("l")) params.setLatLong(parser.value("l"));    
    if(parser.isSet("x")) params.setInitialZoomLevel(parser.value("x").toUInt());
    if(parser.isSet("n")) params.setRangeCircles(parser.value("n").toUInt());

    params.setRadio(!parser.isSet("r"));
    params.setZoomButtons(parser.isSet("z"));
    params.setNoGL(parser.isSet("f"));
    params.setWifichooser(parser.isSet("w"));
    params.setShowDebugButton(parser.isSet("s"));

    qmlRegisterType<AdsbTarget>("AdsbMessage", 1, 0, "AdsbMessage");    
    qmlRegisterType<AdsbListenerModel>("AdsbListenerModel", 1, 0, "AdsbListenerModel");
    qmlRegisterType<AircraftData>("AircraftData", 1, 0, "AircraftData");    
    qmlRegisterType<AvailableCharts>("AvailableCharts", 1, 0, "AvailableCharts");
    qmlRegisterType<InstalledCharts>("InstalledCharts", 1, 0, "InstalledCharts");
    qmlRegisterUncreatableType<DownloadItem>("DownloadItem", 1, 0, "DownloadItem", "");
    qmlRegisterType<DownloadAndUnzipItem>("DownloadAndUnzip", 1, 0, "DownloadAndUnzip");
    qmlRegisterType<DownloadManager>("DownloadManager", 1, 0, "DownloadManager");
    qmlRegisterUncreatableType<OpenSkyUpdater>("OpenSkyUpdater", 1, 0, "OpenSkyUpdater", "Singleton");
    qmlRegisterType<FAAChart>("FAAChart", 1, 0, "FAAChart");

    QQmlApplicationEngine engine;

    // Enable program to update itself
    qmlRegisterUncreatableType<ProgramUpdater>("ProgramUpdater", 1, 0, "ProgramUpdater", "Singleton");
    engine.rootContext()->setContextProperty("programUpdater", &ProgramUpdater::getInstance());

    // Enable antialiasing
    QSurfaceFormat format;
    format.setSamples(8);
    QSurfaceFormat::setDefaultFormat(format);

    // Make build number available to QML
    engine.rootContext()->setContextProperty("buildnumber", ADSBREADER_BUILD);

    // Make command line parameters available to QML
    engine.rootContext()->setContextProperty("params", &params);

    // Instantiate application settings object
    Settings *settings = Settings::getInstance();
    settings->setDefaultValues(&params);
    settings->loadSettings();
    engine.rootContext()->setContextProperty("settings", settings);
    qmlRegisterType<Settings>("Settings", 1, 0, "Settings");

    // Create object to contain available charts
    AvailableCharts availableCharts(params.chartURL());
    engine.rootContext()->setContextProperty("availableCharts", &availableCharts);

    // Initialize database object for aircraft data
    AircraftData::setDatabaseFilename(params.database());
    AircraftData::openDatabaseConnection();

    // Create Aircraft data downloader helper    
    engine.rootContext()->setContextProperty("openskyupdater", &OpenSkyUpdater::getInstance());


    // Start ADS-B RTL SDR listener in its own thread if specified on the command line
    if(params.radio()) {
        std::thread thread1090(start1090);
        thread1090.detach();
    }

    // Enable Wifi Chooser dialog
    RpiWifiManager wifimanager;
    engine.rootContext()->setContextProperty("wifimanager", &wifimanager);        

    QQuickStyle::setStyle("Universal");

    QFont font("Noto Sans");
    font.setStyleHint(QFont::SansSerif);
    font.setPointSize(7);
    QGuiApplication::setFont(font);
    engine.rootContext()->setContextProperty("textSize", font.pointSize());
    engine.rootContext()->setContextProperty("buttonSize", font.pointSize());

    const QUrl url(QStringLiteral("qrc:/adsbmap.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
