#include "settings.h"
#include <QFile>
#include <QDebug>
#include <QDataStream>
#include <QMetaProperty>

Settings::Settings(QObject *parent) : QObject(parent)
{
    resetDefaults();
    setRestorePoint();

    connect(this, &Settings::settingsChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::ownshipChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::chartIndexChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::autocenterChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::rangecirclesChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::northUpChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::declutterChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::fontsizeChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::gainChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::ownshipGainChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::scanOwnshipChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::altitudeFilterChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::zoomLevelChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::trackColorChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::projectedTrackColorChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::projectedTrackMinutesChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::acOverChartColorChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::acNoChartColorChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::circleColorChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::circleMapColorChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::defaultCenterChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::brightnessChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::distAlertChanged, this, &Settings::settingsChangedSlot);
    connect(this, &Settings::altAlertChanged, this, &Settings::settingsChangedSlot);

}

void Settings::setUnsavedSettings(bool unsavedSettings)
{
    m_unsavedSettings = unsavedSettings;
}

bool Settings::unsavedSettings() const
{
    return m_unsavedSettings;
}

int Settings::getZoomLevel(FAAChart::chartType_t t) const
{
    switch(t) {
        case FAAChart::VFR : return m_zoomLevelVFR;
        case FAAChart::IFR : return m_zoomLevelIFR;
    }
    return -1;
}

void Settings::settingsChangedSlot()
{
    setUnsavedSettings(true);
}

Settings *Settings::getInstance()
{
    static Settings s;
    return &s;
}


QDataStream &operator<<(QDataStream &ds, const QObject &obj) {
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds << obj.metaObject()->property(i).read(&obj);

        }
    }
    return ds;
}
QDataStream &operator>>(QDataStream &ds, QObject &obj) {
    QVariant var;
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds >> var;
            obj.metaObject()->property(i).write(&obj, var);
        }
    }
    return ds;
}

bool Settings::saveSettings() {
    QFile file(SETTINGS_FILENAME);
    if( file.open(QIODevice::WriteOnly) )  {

        QDataStream out(&file);
        out << m_version;
        out << *this;
        file.close();
        setRestorePoint();
        return true;
    }
    return false;
}


void Settings::setRestorePoint()
{
    m_buffer.open(QBuffer::WriteOnly);

    QDataStream out(&m_buffer);
    out << *this;

    m_buffer.close();
}

void Settings::rollbackChanges()
{
    m_buffer.open(QBuffer::ReadOnly);
    QDataStream out (&m_buffer);
    out >> *this;
    m_buffer.close();
}


bool Settings::loadSettings() {
    QFile file(SETTINGS_FILENAME);
    if(file.open(QIODevice::ReadOnly) ) {

        QDataStream out(&file);
        int version;
        out >> version;
        if(version != m_version) {
            // Settings file version mismatch. Load default values instead.
            resetDefaults();
            saveSettings();
        } else {
            // Version check ok. Load settings.
            out >> *this;
            file.close();

            emit ownshipChanged();
            emit circleColorChanged();
            emit circleMapColorChanged();
            emit acNoChartColorChanged();
            emit acOverChartColorChanged();
            emit trackColorChanged();
            emit projectedTrackColorChanged();
            emit projectedTrackMinutesChanged();
            emit chartIndexChanged();
            emit autocenterChanged();
            emit rangecirclesChanged();
            emit northUpChanged();
            emit declutterChanged();
            emit fontsizeChanged();
            emit gainChanged();
            emit ownshipGainChanged();
            emit scanOwnshipChanged();
            emit altitudeFilterChanged();
            emit zoomLevelChanged();
            emit defaultCenterChanged();
            emit tracksVisibleChanged();
            emit brightnessChanged();
            emit distAlertChanged();
            emit altAlertChanged();
        }

        setRestorePoint();

        return true;
    }
    return false;
}

void Settings::setDefaultValues(AdsbCommandLineArgs *args) {
    m_args = args;
}

void Settings::resetDefaults()
{
    m_zoomLevelIFR = SETTINGS_DEFAULTZOOMLEVEL_IFR;
    m_zoomLevelVFR = SETTINGS_DEFAULTZOOMLEVEL_VFR;
    m_unsavedSettings = false;
    m_autocenter = 2;
    m_northUp = false;
    m_scanOwnship = true;
    m_tracksVisible = true;    
    m_declutter = 2;
    m_fontsize = 20;
    m_altitudeFilter = 0;
    m_gain = 29;
    m_ownshipGain = 0;
    m_distAlert = 3;
    m_altAlert = 3000;
    m_brightness = 255; // Works with the pi. Should get Max Brightness from params instead...

    m_circleColor = QColor("lime");
    m_circleMapColor = QColor("darkslategray");
    m_acNoChartColor = QColor("yellow"),
    m_acOverChartColor = QColor("red"),
    m_trackColor = QColor("darkmagenta");
    m_projectedTrackColor = QColor("green");
    m_projectedTrackMinutes = 1;

    m_defaultCenter.setAltitude(0);

    // These parameters can be specified on the command line so they will be overriden by the
    // the command line object values if the object set (it is always set, normally)
    m_ownshipicao = "";
    m_rangecircles = 1;
    m_zoomLevel = 8;
    m_chartIndex = 0;
    m_defaultCenter = QGeoCoordinate(45.5448189,-73.3272084);

    // Values that can be specified from the command line are obtained from there
    // (default values for command line arguments are defined in main.cpp)
    if( m_args != nullptr ) {
        m_ownshipicao = m_args->ownship();
        m_rangecircles = m_args->rangeCircles();
        m_zoomLevel = m_args->initialZoomLevel();
        m_chartIndex = m_args->chartIndex();
        m_defaultCenter = m_args->pos();
    }

    emit ownshipChanged();
    emit circleColorChanged();
    emit circleMapColorChanged();
    emit acNoChartColorChanged();
    emit acOverChartColorChanged();
    emit trackColorChanged();
    emit projectedTrackColorChanged();
    emit projectedTrackMinutesChanged();
    emit chartIndexChanged();
    emit autocenterChanged();
    emit rangecirclesChanged();
    emit northUpChanged();
    emit declutterChanged();
    emit fontsizeChanged();
    emit gainChanged();
    emit ownshipGainChanged();
    emit scanOwnshipChanged();
    emit altitudeFilterChanged();
    emit zoomLevelChanged();
    emit defaultCenterChanged();
    emit tracksVisibleChanged();
    emit brightnessChanged();
    emit distAlertChanged();
    emit altAlertChanged();
}
