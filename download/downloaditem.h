#ifndef DOWNLOADITEM_H
#define DOWNLOADITEM_H

#include <QObject>

class DownloadItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name MEMBER m_name NOTIFY messageChanged )
    Q_PROPERTY(DownloadStatus status READ getStatus NOTIFY statusChanged )
    Q_PROPERTY(float progress READ getProgress NOTIFY progressChanged )
    Q_PROPERTY(QString message READ getMessage NOTIFY messageChanged )
    Q_PROPERTY(bool canBeStarted READ getCanBeStarted NOTIFY actionsChanged)
    Q_PROPERTY(bool canBePaused READ getCanBePaused NOTIFY actionsChanged)
    Q_PROPERTY(bool canBeStopped READ getCanBePaused NOTIFY actionsChanged)

public:
    enum DownloadStatus { NotStarted,
                          Downloading,
                          Processing,
                          Paused,
                          Canceled,
                          Failed,
                          Finished };

    Q_ENUM(DownloadStatus)

    explicit DownloadItem(QObject *parent = nullptr);

    virtual QString getName() const;
    virtual float getProgress() const;
    virtual DownloadStatus getStatus() const;
    virtual QString getMessage() const;

    Q_INVOKABLE virtual void startDownload() = 0;
    Q_INVOKABLE virtual void pauseDownload() = 0;
    Q_INVOKABLE virtual void resumeDownload() = 0;
    Q_INVOKABLE virtual void cancelDownload() = 0;    

    bool getCanBePaused() const;
    void setCanBePaused(bool canBePaused);

    bool getCanBeStopped() const;
    void setCanBeStopped(bool canBeStopped);

    bool getCanBeStarted() const;
    void setCanBeStarted(bool canBeStarted);

protected:
    virtual void setMessage(const QString&);
    virtual void setProgress(float);
    virtual void setStatus(DownloadStatus);
    virtual void setName(const QString&);

private:
    QString m_name, m_message;
    DownloadStatus m_status;
    float m_progress;    
    bool m_canBePaused, m_canBeStopped, m_canBeStarted;

public slots:

signals:
    void nameChanged(QString);
    void progressChanged(float);
    void statusChanged(DownloadStatus);
    void messageChanged(QString);    
    void actionsChanged();
    void completed(DownloadStatus);
};

#endif // DOWNLOADITEM_H
