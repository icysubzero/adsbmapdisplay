#ifndef PROGRAMUPDATER_H
#define PROGRAMUPDATER_H

#include <QObject>
#include "downloadandunzipitem.h"
#include "downloadmanager.h"

class ProgramUpdater : public DownloadAndUnzipItem
{
    Q_OBJECT

    Q_PROPERTY(bool updateAvailable READ getUpdateAvailable NOTIFY updateAvailableChanged)

public:
    Q_INVOKABLE static ProgramUpdater& getInstance();

    Q_INVOKABLE void reboot();
    Q_INVOKABLE void updateProgram(DownloadManager*);

    ProgramUpdater(ProgramUpdater const&) = delete;
    void operator=(ProgramUpdater const&) = delete;

    Q_INVOKABLE void checkUpdateAvailable();
    bool getUpdateAvailable() const;

signals:
    void updateAvailableChanged();

private slots:
    void checkFinished();

private:
    ProgramUpdater();
    static ProgramUpdater *instance;
    bool m_updateAvailable;

    void setUpdateAvailable(bool updateAvailable);
};

#endif // PROGRAMUPDATER_H
