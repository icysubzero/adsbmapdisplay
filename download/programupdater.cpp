#include "programupdater.h"
#include <QNetworkReply>

ProgramUpdater& ProgramUpdater::getInstance()
{
    static ProgramUpdater instance;

    return instance;
}

ProgramUpdater::ProgramUpdater() : DownloadAndUnzipItem(nullptr,
                                                        "ADS-B Traffic Display Program",
                                                        QUrl("http://flt.l5.ca/pi/program.zip"),
                                                        ".",
                                                        true)
{
    setUpdateAvailable(false);
}

bool ProgramUpdater::getUpdateAvailable() const
{
    return m_updateAvailable;
}

void ProgramUpdater::setUpdateAvailable(bool updateAvailable)
{
    m_updateAvailable = updateAvailable;
    emit updateAvailableChanged();
}

void ProgramUpdater::checkUpdateAvailable()
{
    static QUrl qurl("http://flt.l5.ca/pi/timestamp.txt");
    static QNetworkAccessManager manager;

    QNetworkRequest request(qurl);
    QNetworkReply* reply = manager.get(request);

    connect(reply, SIGNAL(finished()), this, SLOT(checkFinished()));

    if ( reply->error() != QNetworkReply::NoError ) {
        setUpdateAvailable(false);
        qDebug() << "Unable to connect";
    }
}

void ProgramUpdater::checkFinished()
{
    QNetworkReply *reply = (QNetworkReply*)QObject::sender();
    QByteArray newFile = reply->readAll();

    QFile f("timestamp.txt");

    if(f.open(QIODevice::ReadOnly)) {
        QByteArray currentFile = f.readAll();

        setUpdateAvailable(newFile != currentFile);
        qDebug() << "Update Available test: " << getUpdateAvailable();
        f.close();
    } else {
        setUpdateAvailable(true);
        qDebug() << "No local timestamp.";
    }
    reply->close();
    reply->deleteLater();
}

void ProgramUpdater::reboot()
{
    system("reboot");
}

void ProgramUpdater::updateProgram(DownloadManager *dm)
{
    dm->addDownloadItem(&getInstance());
    setUpdateAvailable(false);
}
