#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H
#include <QAbstractListModel>
#include "downloaditem.h"

class DownloadManager : public QAbstractListModel
{
    Q_OBJECT

public:

    enum Roles { Name = Qt::UserRole + 1,
                 Message,
                 Progress,
                 Status,
                 CanBeStarted,
                 CanBeStopped,
                 CanBePaused,
                 Index
               };    

    DownloadManager(QObject *parent = nullptr);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void addDownloadItem(DownloadItem *);
    Q_INVOKABLE DownloadItem *getItem(int) const;    

    Q_INVOKABLE void pauseDownload(int index);
    Q_INVOKABLE void cancelDownload(int index);
    Q_INVOKABLE void startDownload(int index);    
protected:
    virtual QHash<int, QByteArray> roleNames() const override;

public slots:
    void downloadComplete(DownloadItem::DownloadStatus);

protected slots:
    void itemDataChanged();
private:
    QList<DownloadItem *> m_downloadItems;
};

#endif // DOWNLOADMANAGER_H
