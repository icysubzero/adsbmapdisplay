import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.13
import QtQuick.Controls.Universal 2.12
import QtQuick.Layouts 1.12
import DownloadAndUnzip 1.0
import DownloadManager 1.0
import DownloadItem 1.0


Window {
    id: window
    visible: true
    width: 1800
    height: 480
    color: 'black'

    Universal.theme: Universal.Dark
    Universal.accent: Universal.Yellow


    Item {
        id: dialogItem
        anchors.fill: parent

        DownloadAndUnzip {
            id: downloadItem
            url: "https://aeronav.faa.gov/enroute/11-05-2020/enr_l01.zip"
        }

        DownloadManager {
            id: downloadmanager
        }

        Item {
            id: downloadDialog

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 5

                ColumnLayout {
                    anchors.fill: parent
                    RowLayout {
                        Text {
                            color: 'white'
                            text: "URL:"
                        }
                        TextInput {
                            color: 'white'
                            id: urlinput
                            text: "https://aeronav.faa.gov/enroute/11-05-2020/enr_l05.zip"
                        }
                        Button {
                            text: "Add"
                            onClicked: downloadmanager.addDownloadAndUnzip(urlinput.text);
                        }
                    }

                    Rectangle {
                        border.color: 'white'
                        color:'red'
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.margins: 5

                        ListView {
                            id: downloadList
                            model: downloadmanager
                            anchors.fill: parent
                            delegate: Rectangle {
                                height: 60
                                width: parent.width
                                color: 'gray'
                                Text {
                                    id: textname
                                    text: name + ' - ' + downloadmanager.getItem(index).message
                                }
                                Slider {
                                    anchors.bottom: parent.bottom
                                    anchors.left: parent.left
                                    width: parent.width - 50
                                    from: 0
                                    to: 100
                                    value: downloadmanager.getItem(index).progress
                                }
                                Button {
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.right: parent.right
                                    visible: !(downloadmanager.getItem(index).status === DownloadItem.Completed) && (
                                                  ( downloadmanager.getItem(index).status === DownloadItem.NotStarted && downloadmanager.getItem(index).canBeStarted )
                                               || ( downloadmanager.getItem(index).status === DownloadItem.Downloading && downloadmanager.getItem(index).canBePaused )
                                               || ( downloadmanager.getItem(index).status === DownloadItem.Processing && downloadmanager.getItem(index).canBePaused )
                                               || ( downloadmanager.getItem(index).status === DownloadItem.Paused )
                                               || ( downloadmanager.getItem(index).status === DownloadItem.Failed && downloadmanager.getItem(index).canBeStarted )
                                               )

                                    text: (downloadmanager.getItem(index).status === DownloadItem.NotStarted )
                                          || ( downloadmanager.getItem(index).status === DownloadItem.Paused )
                                          || ( downloadmanager.getItem(index).status === DownloadItem.Failed )? 'Start' : 'Pause'
                                    onClicked: {
                                        text == 'Start' ?
                                                    downloadmanager.getItem(index).startDownload()
                                                  : downloadmanager.getItem(index).pauseDownload()
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }
}
