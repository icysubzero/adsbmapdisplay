import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12
import DownloadManager 1.0
import DownloadItem 1.0

// Component must be instantiatied with downloadmanager set to a valid DownloadManager instance

Item {
    id: downloadManagerItem
    property DownloadManager downloadmanager
    anchors.fill: parent
    signal closed()

    ColumnLayout {
        anchors.fill: parent
        Text {
            Layout.margins: 5
            text: "Download Manager"
            font.pointSize: textSize
            font.bold: true
            color: 'white'
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5
            border.width: 2
            border.color: 'white'
            color: 'transparent'
            radius: 5

            ListView {
                id: downloadList
                model: downloadmanager
                anchors.fill: parent
                anchors.margins: 5
                clip: true

                delegate: Rectangle {
                    height: 60
                    width: parent.width
                    color: 'gray'
                    Text {
                        id: textname
                        text: name + ' - ' + message + " (" + progress + "%)"
                        font.pointSize: textSize - 1
                    }
                    ProgressBar {
                        anchors.top: textname.bottom
                        anchors.margins: 10
                        anchors.left: parent.left
                        anchors.right: downloadbutton.left

                        from: 0
                        to: 100
                        value: progress
                    }
                    Button {
                        id: downloadbutton
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        visible: !(model.status === DownloadItem.Completed) && (                                      
                                      ( ((model.status === DownloadItem.Downloading) || (model.status === DownloadItem.Processing)) && (model.canBePaused || model.canBeStopped) )
                                   || ( model.status === DownloadItem.Paused )
                                   || ( ((model.status === DownloadItem.Failed) || (model.status === DownloadItem.Canceled) || ( model.status === DownloadItem.NotStarted)) && model.canBeStarted )
                                   )

                        text: (model.status === DownloadItem.NotStarted )
                              || ( model.status === DownloadItem.Canceled )
                              || ( model.status === DownloadItem.Failed )? 'Start' : 'Stop'
                        font.pointSize: textSize
                        onClicked: {
                            text == 'Start' ?
                                        downloadmanager.startDownload(index)
                                      : downloadmanager.cancelDownload(index)
                        }
                    }
                }
            }
        }

        RowLayout {
            id: downloadManagerButtons
            Layout.margins: 5
            Layout.alignment: Qt.AlignRight
            Layout.preferredHeight: 50
            Button {
                text: "Close"                                
                font.pointSize: buttonSize
                onClicked: {
                    downloadManagerItem.closed()
                }
            }
        }
    }
}
