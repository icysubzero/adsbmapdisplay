#ifndef DOWNLOADANDUNZIPITEM_H
#define DOWNLOADANDUNZIPITEM_H

#include <QObject>
#include <QDebug>
#include <QUrl>
#include <QTemporaryFile>
#include <QNetworkAccessManager>
#include "downloaditem.h"

class DownloadAndUnzipItem : public DownloadItem
{
    Q_OBJECT

    Q_PROPERTY(QString  destinationFolder   READ destinationFolder  WRITE setDestinationFolder  NOTIFY destinationFolderChanged)
    Q_PROPERTY(QUrl     url                 READ url                WRITE setUrl                NOTIFY urlChanged)
    Q_PROPERTY(bool     unzip               MEMBER m_unzip                                      NOTIFY zipChanged)


public:
    DownloadAndUnzipItem(QObject *parent = nullptr);
    DownloadAndUnzipItem(QObject *parent, QString name, QUrl url, QString destFolder, bool unzip);

    Q_INVOKABLE virtual void startDownload();
    Q_INVOKABLE virtual void pauseDownload();
    Q_INVOKABLE virtual void resumeDownload();
    Q_INVOKABLE virtual void cancelDownload();       

    QUrl url() const;
    void setUrl(const QUrl &url);

    QString destinationFolder() const;
    void setDestinationFolder(const QString &destinationFolder);

    bool unzip() const;
    void setUnzip(bool unzip);

private:
    QUrl m_url;
    QString m_destinationFolder;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    QTemporaryFile *file;
    qint64 m_fileSize, m_bytesRead;
    bool m_unzip;

protected slots:
    void downloadRequestFinished();
    void readyToRead();
    void fileSize();
    void errorOccurred();

signals:
    void destinationFolderChanged(QString);
    void urlChanged(QUrl);
    void zipChanged();
};

#endif // DOWNLOADANDUNZIPITEM_H
