#include <QProcess>
#include "downloadmanager.h"
#include "downloadandunzipitem.h"

DownloadManager::DownloadManager(QObject *parent) : QAbstractListModel(parent)
{

}

int DownloadManager::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_downloadItems.count();
}

QVariant DownloadManager::data(const QModelIndex &index, int role) const
{
    if(index.isValid()) {
        DownloadItem *item = m_downloadItems.at(index.row());

        switch(role) {
            case Name       :   return item->getName();
            case Status     :   return item->getStatus();
            case Progress   :   return item->getProgress();
            case Message    :   return item->getMessage();
            case Index      :   return index.row();
            case CanBePaused:   return item->getCanBePaused();
            case CanBeStarted:  return item->getCanBeStarted();
            case CanBeStopped:  return item->getCanBeStopped();
        }
    }
    return QVariant();
}

QHash<int, QByteArray> DownloadManager::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[Name]         =   "name";
    roles[Status]       =   "status";
    roles[Progress]     =   "progress";
    roles[Message]      =   "message";
    roles[Index]        =   "index";
    roles[CanBePaused]  =   "canBePaused";
    roles[CanBeStarted] =   "canBeStarted";
    roles[CanBeStopped] =   "canBeStopped";

    return roles;
}

void DownloadManager::addDownloadItem(DownloadItem *item)
{
    if(m_downloadItems.contains(item)) return;

    beginInsertRows(QModelIndex(), m_downloadItems.count(), m_downloadItems.count());
    m_downloadItems << item;
    connect(item, &DownloadItem::progressChanged, this, &DownloadManager::itemDataChanged);
    connect(item, &DownloadItem::messageChanged, this, &DownloadManager::itemDataChanged);
    connect(item, &DownloadItem::actionsChanged, this, &DownloadManager::itemDataChanged);
    connect(item, &DownloadItem::statusChanged, this, &DownloadManager::itemDataChanged);
    connect(item, &DownloadItem::nameChanged, this, &DownloadManager::itemDataChanged);
    connect(item, &DownloadItem::completed, this, &DownloadManager::downloadComplete);
    item->startDownload();
    endInsertRows();
}

DownloadItem *DownloadManager::getItem(int i) const
{
    return m_downloadItems.at(i);
}

void DownloadManager::itemDataChanged() {
    int i = m_downloadItems.indexOf((DownloadItem*)sender());

    // If a download is stopped and there is no way to restart it, remove it
    DownloadItem *item = (DownloadItem *)sender();
    if((item->getStatus() == DownloadItem::Failed
        || item->getStatus() == DownloadItem::Canceled)
            && !item->getCanBeStarted()) {
        beginRemoveRows(QModelIndex(), i, i);
        m_downloadItems.removeAt(i);
        endRemoveRows();
    }

    emit dataChanged(index(i), index(i), QVector<int>::fromList(roleNames().keys()));
}

void DownloadManager::downloadComplete(DownloadItem::DownloadStatus s)
{
    if(s == DownloadItem::Canceled) return;

    DownloadItem *item = (DownloadItem *)sender();
    int i = m_downloadItems.indexOf(item);
    if(i != -1) {
        beginRemoveRows(QModelIndex(), i, i);
        m_downloadItems.removeAt(i);        
        endRemoveRows();
    }
}

void DownloadManager::pauseDownload(int index) {
    if(index >=0) {
        DownloadItem *i = m_downloadItems.at(index);
        if(i != nullptr)
            i->pauseDownload();
    }
}

void DownloadManager::cancelDownload(int index) {
    if(index >=0) {
        DownloadItem *i = m_downloadItems.at(index);
        if(i != nullptr)
            i->cancelDownload();
    }
}

void DownloadManager::startDownload(int index) {
    if(index >=0) {
        DownloadItem *i = m_downloadItems.at(index);
        if(i != nullptr)
            i->startDownload();
    }
}
