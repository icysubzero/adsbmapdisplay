#include "downloadandunzipitem.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTemporaryFile>
#include <QProcess>
#include <fstream>
#include <iostream>
#include <ios>
#include <cstdio>

DownloadAndUnzipItem::DownloadAndUnzipItem(QObject *parent) : DownloadItem(parent)
{
    manager = new QNetworkAccessManager(this);
    m_fileSize = m_bytesRead = 0;
    m_unzip = true;
}

DownloadAndUnzipItem::DownloadAndUnzipItem(QObject *parent, QString name, QUrl url, QString destFolder, bool unzip = true) : DownloadAndUnzipItem(parent)
{
   setUrl(url);
   setDestinationFolder(destFolder);
   setName(name);
   setUnzip(unzip);
}

QUrl DownloadAndUnzipItem::url() const
{
    return m_url;
}

void DownloadAndUnzipItem::setUrl(const QUrl &url)
{
    m_url = url;
    emit urlChanged(url);
}

QString DownloadAndUnzipItem::destinationFolder() const
{
    return m_destinationFolder;
}

void DownloadAndUnzipItem::setDestinationFolder(const QString &destinationFolder)
{
    m_destinationFolder = destinationFolder;
    emit destinationFolderChanged(m_destinationFolder);
}

bool DownloadAndUnzipItem::unzip() const
{
    return m_unzip;
}

void DownloadAndUnzipItem::setUnzip(bool unzip)
{
    m_unzip = unzip;
    emit zipChanged();
}

void DownloadAndUnzipItem::downloadRequestFinished()
{
    if((getStatus() != Paused) && (getStatus() != Failed)) {

        QNetworkReply *reply = (QNetworkReply*)QObject::sender();

        file->close();
        reply->close();
        reply->deleteLater();

        setMessage("Download complete");
        setStatus(Processing);
        setCanBePaused(false);

        // Now unzip the downloaded file to the destination folder
        QProcess *proc = new QProcess(this);
        QObject::connect(proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
                               [&]( int exitCode, QProcess::ExitStatus exitStatus ) {
                                        if((exitStatus == QProcess::NormalExit) && (exitCode == 0 ) ) {
                                            setStatus(Finished);
                                            setMessage("File successfully unzipped");
                                         } else {
                                            setStatus(Failed);
                                            setMessage("Error while unzipping file");
                                        }
                                        file->deleteLater();
                                        emit completed(getStatus());
                                    }
        );
        proc->setStandardOutputFile("/dev/null");
        proc->setStandardErrorFile("/dev/null");

        QStringList cmd;

        if(unzip()) {
            cmd << "-o" << "-d" << destinationFolder() << file->fileName();

            proc->start("/usr/bin/unzip", cmd);
            setMessage("Unzipping to " + destinationFolder());
        } else {
            // No unzip required. Move the temporary files to the destination folder as-is.

            std::ifstream in(file->fileName().toStdString(), std::ios::in | std::ios::binary);
            std::ofstream out((destinationFolder() + "/" + url().fileName()).toStdString(), std::ios::out | std::ios::binary);
            out << in.rdbuf();

            file->deleteLater();
            setStatus(Finished);
            emit completed(Finished);
        }
    }
}

void DownloadAndUnzipItem::readyToRead()
{
    QByteArray data = reply->readAll();
    file->write(data);
    m_bytesRead += data.size();

    if(m_fileSize != 0) {
        setProgress(int(100.0 * m_bytesRead / m_fileSize));
        setMessage( QString::number(m_bytesRead / 1000.0, 'f', 0 ) + " / " + QString::number(m_fileSize / 1000.0, 'f', 0) + " KB" );
    }
}

void DownloadAndUnzipItem::fileSize()
{
    if(m_fileSize == 0)
        m_fileSize = reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
}

void DownloadAndUnzipItem::errorOccurred()
{
    reply->abort();
    reply->deleteLater();
    file->close();
    file->deleteLater();
    setStatus(Failed);
    setMessage("Error occurred");
}

void DownloadAndUnzipItem::startDownload() {

    if(getStatus() == Paused) {        
        resumeDownload();
    } else if((getStatus() == NotStarted) || (getStatus() == Failed) || (getStatus() == Canceled)) {
        file = new QTemporaryFile(this);
        file->open();
        file->fileName(); // Required to get a named temporary file

        m_fileSize = 0;
        m_bytesRead = 0;
        setProgress(0);

        QNetworkRequest request(url());
        reply = manager->get(request);
        setStatus(Downloading);
        setCanBePaused(true);

        connect(reply, &QNetworkReply::metaDataChanged,
                this, &DownloadAndUnzipItem::fileSize);
        connect(reply, &QNetworkReply::finished,
                this, &DownloadAndUnzipItem::downloadRequestFinished);
        connect(reply, &QNetworkReply::readyRead,
                this, &DownloadAndUnzipItem::readyToRead);

        if ( reply->error() != QNetworkReply::NoError ) {
            qWarning() <<"ErrorNo: "<< reply->error() << "for url: " << reply->url().toString();
            qDebug() << "Request failed, " << reply->errorString();
            qDebug() << "Headers:"<<  reply->rawHeaderList()<< "content:" << reply->readAll();
            file->close();
            file->deleteLater();
            reply->deleteLater();
            setStatus(Failed);
        }
    }
}

void DownloadAndUnzipItem::pauseDownload()
{
    if(getStatus() == Downloading) {
        disconnect(reply, &QNetworkReply::finished,
                this, &DownloadAndUnzipItem::downloadRequestFinished);
        setStatus(Paused);
        setMessage(QString::number((int)getProgress()) + "% - Paused");
        reply->abort();
        reply->deleteLater();
    }
}

void DownloadAndUnzipItem::resumeDownload()
{
    if(getStatus() == Paused ){
        QNetworkRequest request(url());
        QByteArray rangeHeaderValue = "bytes=" + QByteArray::number(m_bytesRead) + "-";
        request.setRawHeader("Range", rangeHeaderValue);

        reply = manager->get(request);
        setStatus(Downloading);
        setCanBePaused(true);
        connect(reply, &QNetworkReply::metaDataChanged,
                this, &DownloadAndUnzipItem::fileSize);
        connect(reply, &QNetworkReply::finished,
                this, &DownloadAndUnzipItem::downloadRequestFinished);
        connect(reply, &QNetworkReply::readyRead,
                this, &DownloadAndUnzipItem::readyToRead);


        if ( reply->error() != QNetworkReply::NoError ) {
            qWarning() <<"ErrorNo: "<< reply->error() << "for url: " << reply->url().toString();
            qDebug() << "Request failed, " << reply->errorString();
            qDebug() << "Headers:"<<  reply->rawHeaderList()<< "content:" << reply->readAll();
            file->close();
            file->deleteLater();
            reply->deleteLater();
            setStatus(Failed);
        }
    }
}

void DownloadAndUnzipItem::cancelDownload()
{
    if(getStatus() == Downloading ) {
        pauseDownload();
    }
    if(getStatus() == Paused ) {
        file->close();
        file->deleteLater();
    }
    setStatus(Canceled);
    setMessage("Canceled");
    setProgress(0);
}
