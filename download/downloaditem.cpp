#include "downloaditem.h"
#include <QDebug>

DownloadItem::DownloadItem(QObject *parent) : QObject(parent)
{
    setStatus(NotStarted);
    setCanBePaused(true);
    setCanBeStopped(true);
    setCanBeStarted(true);
    setProgress(0);
}

QString DownloadItem::getName() const
{
    return m_name;
}

float DownloadItem::getProgress() const
{    
    return m_progress;
}

DownloadItem::DownloadStatus DownloadItem::getStatus() const
{
    return m_status;
}

QString DownloadItem::getMessage() const
{
    return m_message;
}

void DownloadItem::setMessage(const QString &m)
{
    this->m_message = m;
    emit messageChanged(m);
}

void DownloadItem::setProgress(float p)
{
    this->m_progress = p;
    emit progressChanged(p);    
}

void DownloadItem::setStatus(DownloadItem::DownloadStatus s)
{
    this->m_status = s;
    emit statusChanged(s);
}

void DownloadItem::setName(const QString &n)
{
    m_name = n;
    emit nameChanged(n);
}

bool DownloadItem::getCanBeStarted() const
{
    return m_canBeStarted;
}

void DownloadItem::setCanBeStarted(bool canBeStarted)
{
    m_canBeStarted = canBeStarted;
    emit actionsChanged();
}

bool DownloadItem::getCanBeStopped() const
{
    return m_canBeStopped;
}

void DownloadItem::setCanBeStopped(bool canBeStopped)
{
    m_canBeStopped = canBeStopped;
    emit actionsChanged();
}

bool DownloadItem::getCanBePaused() const
{
    return m_canBePaused;
}

void DownloadItem::setCanBePaused(bool canBePaused)
{
    m_canBePaused = canBePaused;
    emit actionsChanged();
}
