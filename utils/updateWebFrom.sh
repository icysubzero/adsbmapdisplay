#!/bin/bash

# Usage: updateFrom <host>

# Packs a zip file with executables from <host>'s /home/pi/bin folder and
# uploads the zip file to our web server

tmp=$(mktemp -d)
pushd $tmp

date > timestamp.txt
scp $1:bin/adsbreader $1:bin/*.py $1:bin/update.sql $1:bin/*.sh .
zip program.zip *
scp -P 2222 -i ~/.ssh/id_router.pub -o 'KexAlgorithms=+diffie-hellman-group1-sha1' program.zip timestamp.txt root@192.168.4.1:/opt/usr/www/pi

popd
rm -rf $tmp


# NOTE: this is how to connect to a router running an old version of dd-wrt
# ssh-keygen -t rsa -b 2048
# ssh-copy-id root@192.168.4.1 -p 2222 -oKexAlgorithms=+diffie-hellman-group1-sha1
# ssh -p '2222' -o 'KexAlgorithms=+diffie-hellman-group1-sha1' 'root@192.168.4.1'
