#!/bin/bash
#
# This script can serve a file containing SBS records typically recorded from
# dump1090 to a TCP socket. To a program, this would look exactly like a
# real dump1090 process running. When the TCP socket is closed by the listener,
# this server process will end and a new one will be restarted and will
# serve data starting at the beginning of the file. The script must be terminated
# or killed in order to stop it.
#
# usage: replay.sh [interval] [file] [port]
#
#   interval  : the time in seconds to wait between the transmission of each
#               line in the file. Default: 0.01 second
#   file      : the name of the file containing the data to be replayed, in
#               SBS format. Default: datareplay.log.
#   port      : the port on which to accept client connections. Default: 30003
#
# The arguments are positional so it is necessary to specify [interval] and [file]
# in order to change the default port.
#
# Sample format of a line in the file to be replayed (see dump1090 documentation
# for details):
#
# MSG,3,111,11111,4BB1C7,111111,2021/01/17,18:23:50.736,2021/01/17,18:23:50.683,,7425,,,45.69768,-73.18268,,,,,,0
#
#

if [ -z "$2" ]
then
  file='datareplay.log'
else
  file=$2
fi

if [ -z "$3" ]
then
  port=30003
else
  port=$3
fi

if [ -z "$1" ]
then
  interval='0.01'
else
  interval=$1
fi

while :
do
  echo 'Serving '$file' on port '$port' with interval of '$interval' second'
  while read -r line 
  do 
    echo "$line"
    sleep $interval
  done < $file | nc -l $port

  if [ $? != 0 ]
  then
    break
  fi
done
