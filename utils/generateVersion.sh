#!/bin/bash

cd $1

codeVersion=`grep ADSBREADER_BUILD version.h|cut -d " " -f 3`
gitVersion=`git rev-list --all --count`

if [ $codeVersion != $gitVersion ]; then

echo "#ifndef VERSION_H
#define VERSION_H

#define ADSBREADER_BUILD "$gitVersion"

#endif" > $1/version.h

fi
