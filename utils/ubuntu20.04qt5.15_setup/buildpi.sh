#!/bin/bash

# Check out pisetup.sh for important Raspberry Pi OS
# version information.
# This script was tested with Qt 5.15.16 and the Raspberry
# Pi OS version listed in pisetup.sh

# OTHER VERSION COMBINATIONS OF QT AND Pi OS ARE UNLIKELY
# TO WORK

# This script will configure our Linux host 
# to be a cross-compiler for a Raspberry Pi device
# It should be run from the host computer under a
# regular user identity (i.e. not with sudo)

# First, check that we're running on a PC and not the Pi
if (( `uname -m | grep -c x86 -` != 1 ))
then
  echo This script must be run on the Linux host machine,
  echo not on the Pi
  exit -1
fi

HOME=/home/pi
KNOWN_HOSTS=$HOME/.ssh/known_hosts
SYSROOT=$HOME/qt/sysroot
PIUSER=pi
PIHOST=pi3
PIIP=`getent hosts $PIHOST|awk '{ print $1 }'`

mkdir -p $SYSROOT

# Manage SSH authentication between the Pi and our host
if (( `grep -c $PIHOST $KNOWN_HOSTS` > 0 ))
then
  echo Removing old key from $KNOWN_HOSTS
  ssh-keygen -R $PIHOST
  ssh-keygen -R $PIIP
fi
echo Adding $PIHOST to $KNOWN_HOSTS
ssh-keyscan $PIHOST >> $KNOWN_HOSTS

# Sysroot-related steps
mkdir $HOME/qt/utils
cd $HOME/qt/utils
# Get a script to fix relative links within sysroot
wget https://raw.githubusercontent.com/riscv/riscv-poky/master/scripts/sysroot-relativelinks.py
chmod a+x sysroot-relativelinks.py

# Create a script to sync the sysroot directory
echo "
# Create a copy of the Pi libraries on our host to enable
# cross compilation
rsync -avz --rsync-path=\"sudo rsync\" --delete $PIUSER@$PIHOST:/lib $SYSROOT
rsync -avz --rsync-path=\"sudo rsync\" --delete $PIUSER@$PIHOST:/usr/include $SYSROOT/usr
rsync -avz --rsync-path=\"sudo rsync\" --delete $PIUSER@$PIHOST:/usr/lib $SYSROOT/usr
#rsync -avz --rsync-path=\"sudo rsync\" --delete $PIUSER@$PIHOST:/opt/vc $SYSROOT/opt
rsync -avz --rsync-path=\"sudo rsync\" --delete $PIUSER@$PIHOST:/etc $SYSROOT
pushd $SYSROOT
$HOME/qt/utils/sysroot-relativelinks.py $SYSROOT
popd
" > $HOME/qt/utils/sync-sysroot.sh

chmod a+x $HOME/qt/utils/sync-sysroot.sh
$HOME/qt/utils/sync-sysroot.sh



# Build Qt for the Pi

# Fix the device configurations to enable 64-bit compilation
# (This is necessary with Qt 5.15 and a 64-bit OS on the Pi)

cd $HOME/qt/source/qt5/qtbase/mkspecs/devices/linux-rasp-pi3-vc4-g++
if [ ! -f qmake.conf.bak ]
then
  cp qmake.conf qmake.conf.bak
fi

sed -i 's/-march=armv8-a -mtune=cortex-a53 -mfpu=crypto-neon-fp-armv8/-march=armv8-a -mtune=cortex-a53/g' qmake.conf

cd ../common

if [ ! -f linux_arm_device_post.conf.bak ] 
then 
  cp linux_arm_device_post.conf linux_arm_device_post.conf.bak
fi

sed -i 's/-mfloat-abi=hard//g' linux_arm_device_post.conf


# Now that our source is ready, cross-compile the Qt library for the Pi
mkdir -p $HOME/qt/5.15/pi
cd $HOME/qt/build

# Clean previous builds
rm -rf *

../source/qt5/configure -release -opengl es2 -device linux-rasp-pi3-vc4-g++ \
  -device-option CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- -sysroot $SYSROOT \
  -opensource -confirm-license -make libs -prefix $HOME/qt \
  -nomake examples -nomake tests \
  -extprefix $HOME/qt/5.15/pi -v 

make -j `nproc`
make install

# Copy the cross-compiled QT files to the pi. The /usr/local/qt5 folder must previously exist and be owned by pi:pi
scp -r $HOME/qt/5.15/pi/* ${PIUSER}@${PIHOST}:/usr/local/qt5

# Add QT libraries to local sysroot
mkdir -p $HOME/qt/sysroot/usr/local
ln -s $HOME/qt/5.15/pi $HOME/qt/sysroot/usr/local/qt5


# Build our app for the Pi and deploy it
mkdir -p $HOME/projects/build_pi
cd $HOME/projects/build_pi
$HOME/qt/5.15/pi/bin/qmake $HOME/projects/adsbmapdisplay/adsbreader.pro -spec devices/linux-rasp-pi3-vc4-g++ CONFIG+=debug CONFIG+=qml_debug
make -j `nproc`
make install
ssh $PIHOST 'mkdir -p /home/pi/bin'
scp $HOME/bin/* ${PIUSER}@${PIHOST}:/home/pi/bin
rm -rf $HOME/bin

ssh pi@pi3 "cd bin && ./updateAircraftDB.sh aircraft.db"

echo Run the following command on $PIHOST to finish the configuration:
echo "  echo /usr/local/qt5/lib | sudo tee /etc/ld.so.conf.d/qt5.conf && sudo ldconfig"

