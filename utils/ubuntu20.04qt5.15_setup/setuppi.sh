#!/bin/bash
# This must be run as root (e.g. sudo setup.sh)
if [ `whoami` != root ]
then
        echo 'This script needs to be run as root, e.g. sudo <script>'
        exit 1
fi

USER=pi
HOME=/home/$USER
KNOWN_HOSTS=$HOME/.ssh/known_hosts

ID_RSA='-----BEGIN RSA PRIVATE KEY-----
MIIEoAIBAAKCAQEAjzOR9SYFNJZlVWBhMgjmWJPF7u9alI0iXWBS6J3Ec0n8J8kF
5ivtXO9SAuu3GDvqL3d87GSKXPGTbRrc7v6necgJp+BOfrQ0xRzcwkEbHZS0gv7y
V1Rg5E6V5IJcAiYWP/BKUFnVvblnRb1VHlGdQ11kYtrPGjBfeSCQpAGfMGcFOo2m
olWYNL8w5fnEAv+ZSEqZYimTJRONVJ4RLyhkO/lBc4IeSSIsbMY8XFyiJJOJl8xN
W2gBE3+iqN7XBCZafhoRbu1gywdpaTxk4OZKxiI9a3acle4ZVE/CrGaSDdt6uVau
+6620gouTcEvphlDasg3+R/ZQkNBoFE+4LLXmQIBJQKCAQAms/3vOrVMe6yvSnsi
R5gznY9xAmuCGE56GgiR5XNdbfEfgnBMC99JjMoOoJJZk6cTv23fBmqVqRMWkaN+
3QqrStItXzfPN5+PODuqH28cwGgjZ3/Ek1h1DlIGaGvk57L8hiHsM/SUJEVsy2Mc
83aclcgat62YYBnNtcY6KfOsNx1LoSGh35K/Lr3YAkuQdKWQhQPTIVg1uy4sEAzX
bRjlJxjuRac3/WiEkiSyjKSWQ2c/7hjj9OtWQfav+VN8hZwf8MV5GI1mmLku5qbu
eV1qSvk5epk2w2H/WMCGGFQ3h63eYmZ6hnVWzbfkvi+estNo4SnQsnjqB7P+9EYq
1HIVAoGBAMc4wpql61U6c3NZwdnFHPtx4BMcoSFA2Hl7vph9HxQePLIHpzM9D2kg
zYaf4W9FQGaAMK1FjeWdnci4cmw/8tfDU8M0belHn8iHqvZhKP9jZQvwa1qJm5JR
Lk790cYcDsruaT80l99WFOHJFxhB5Yxl3L5FGDxF4fG01/aOBlJPAoGBALgDkHXy
SKQDuGWGldKnZjk/sZG4Wd63BoKcPa+U9ac0RX/6P5nwHFouQmEn2ThM81hEqqpk
AxsdIPxlUekREfREmEBg19K6/1oabzgNYqTzDjDzLAsoGTYFjETxoS3xI3lHTifO
i77t/CQctzP/atwA3ZIv2apnRM+I8XfnZ0WXAoGAYOsuPWV5Z7uSHHDa2KUVBLPw
eAAXCUIkH2yh6VGgaqbtGFbACxbJOg/1SGl0lv8YaTdxoGcGwrthhD4OJtMAhKQ2
llDbhj6FFXJu2LmlPfkAubM7JSBZhXOZ/Oous2eYfmYlXQTUQyL1Zuw0v7Fa6lsR
cVILykuCrPAxtjdBWHkCgYBjd4xbbjUaYuA24PcKFUwDRQYJlBTnFtMWKvDpSZmK
0CWRSP/I1NEOJtfF0Fm9kWDxcTmoNg+ERxi/yBCZqFy7Y1k3jkslzN0OHCBxU1fq
dYshYNmJfXVpTxtjiYeOdIHgsOv5124iBBmW+sPkwWoxu0dV8Fn7Pr1pQxq9Wn0D
BQKBgH/OyXIf7yz32+PNuDXPgyg3RhK+vxgUi/hHwybP8ZUPmbIJ7LOL0suJXSFr
xtIFtKJP1tG5Q7DCDAYK6vHckqdEAiA/QXywbT+qQ7us1vKQpXuR6DC21DgWydqz
VU1ynBIa83BR8xGm6F6SZNrGtrBbWtcWefWbyMvJSww3OL1C
-----END RSA PRIVATE KEY-----'

PUBLIC_KEY="ssh-rsa \
AAAAB3NzaC1yc2EAAAABJQAAAQEAjzOR9SYFNJZlVWBhMgjmWJPF7u9alI0iXWBS\
6J3Ec0n8J8kF5ivtXO9SAuu3GDvqL3d87GSKXPGTbRrc7v6necgJp+BOfrQ0xRzc\
wkEbHZS0gv7yV1Rg5E6V5IJcAiYWP/BKUFnVvblnRb1VHlGdQ11kYtrPGjBfeSCQ\
pAGfMGcFOo2molWYNL8w5fnEAv+ZSEqZYimTJRONVJ4RLyhkO/lBc4IeSSIsbMY8\
XFyiJJOJl8xNW2gBE3+iqN7XBCZafhoRbu1gywdpaTxk4OZKxiI9a3acle4ZVE/C\
rGaSDdt6uVau+6620gouTcEvphlDasg3+R/ZQkNBoFE+4LLXmQ== \
rsa-key-20191222"

# If the gps udev file exists, the script was run previously so we skip this section
if [ ! -f /etc/udev/rules.d/90-gps.rules ]
then

mkdir -p $HOME/.ssh
chown $USER:$USER $HOME/.ssh
echo "$ID_RSA" > $HOME/.ssh/id_rsa
echo $PUBLIC_KEY > $HOME/.ssh/authorized_keys
chown $USER:$USER ~/.ssh/id_rsa
chown $USER:$USER ~/.ssh/authorized_keys
chmod 600 ~/.ssh/id_rsa


apt-get -y update
apt-get -y upgrade

apt-get install -y make build-essential libclang-dev ninja-build gcc git bison \
  python3 gperf pkg-config libfontconfig1-dev libfreetype6-dev libx11-dev \
  libx11-xcb-dev libxext-dev libxfixes-dev libxi-dev libxrender-dev libxcb1-dev \
  libxcb-glx0-dev libxcb-keysyms1-dev libxcb-image0-dev libxcb-shm0-dev \
  libxcb-icccm4-dev libxcb-sync-dev libxcb-xfixes0-dev libxcb-shape0-dev \
  libxcb-randr0-dev libxcb-render-util0-dev libxcb-util-dev libxcb-xinerama0-dev \
  libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev libatspi2.0-dev \
  libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev librtlsdr-dev libgps-dev \
  sqlite3 gdbserver gdal-bin libegl1-mesa-dev libgbm-dev libgles2-mesa-dev mesa-common-dev \
  libssl-dev
  
#apt-get install -y rpd-plym-splash

# Add variable definitions to run QT and adapt content to Waveshare 4.3" screen

if (( `grep -c LD_LIBRARY_PATH $HOME/.profile` == 0 ))
then
  echo "
export LD_LIBRARY_PATH=/usr/local/qt5/lib
export QT_QPA_EGLFS_PHYSICAL_WIDTH=95
export QT_QPA_EGLFS_PHYSICAL_HEIGHT=54
" >> $HOME/.profile
fi

# Configure RTL-SDR as USB device accessible to all
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="0bda", ATTRS{idProduct}=="2838", MODE="0666", SYMLINK+="rtl_sdr"'\
  > /etc/udev/rules.d/20.rtlsdr.rules

# Configure GPS device as /dev/gps accessible to all users
echo 'KERNEL=="ttyACM[0-9]", ATTRS{idProduct}=="01a7", ATTRS{idVendor}=="1546", MODE="0666", SYMLINK+="gps"' \
  > /etc/udev/rules.d/90-gps.rules

# Reload USB rules
udevadm control --reload-rules && service udev restart && udevadm trigger

# Edit config.txt to use the right kernel module for the Waveshare screen
if (( `grep -c vc4-fkms /boot/config.txt` == 0 ))
then
  # Change the graphics driver and disable splash screen
  sed -i -e 's/dtoverlay=/#dtoverlay=/g' -e 's/\[all\]/\[all\]\ndtoverlay=vc4-fkms-v3d\nstart_x=1\ndisable_splash=1/g' /boot/config.txt
  # Disable rainbow screen at bootup
  echo "Kernel configuration changed to enable touchscreen support. REBOOT REQUIRED."
fi

# Enable I2C support in the kernel so the Waveshare touchscreen will work
if (( `grep -c \#dtparam=i2c_arm=on /boot/config.txt` > 0 ))
then
  sed -i -e 's/\#dtparam=i2c_arm/dtparam=i2c_arm/g' /boot/config.txt
fi

# Disable startup informational logs and console
#if (( `grep -c logo.nologo /boot/cmdline.txt` == 0 ))
#then
#  sed -i 's/rootwait/rootwait logo.nologo consoleblank=1 loglevel=3 splash vt.global_cursor_default=0 plymouth.ignore-serial-consoles/g' /boot/cmdline.txt
#fi

# Set new splash screen
#echo Put the splash screen image at /usr/share/plymouth/themes/pix/splash.png

# Enable autologin for USER
systemctl --quiet set-default multi-user.target
echo "[Service]
ExecStart=
ExecStart=-/sbin/agetty --skip-login --noissue --login-options \"-f pi\" --noclear %I $TERM
" > /etc/systemd/system/getty@tty1.service.d/autologin.conf

# Suppress login messages
touch ~/.hushlogin

# Create local QT directory
mkdir -p /usr/local/qt5
chown pi:pi /usr/local/qt5

# Create directory for charts
mkdir -p /var/www/html
chown pi:pi /var/www/html


echo First portion of setup script complete. Reboot using (sudo reboot) and re-run.

# The script above was previously run so we continue here
else





# Enable write access to the brightness control system file
# to allow our application to adjust the screen brightness
if [ -f /sys/class/backlight/rpi_backlight/brightness ]
then
  chown pi:pi /sys/class/backlight/rpi_backlight/brightness
  echo Screen brightness control sucessfully activated
else
  echo Brightness control file not found. Either the screen
  echo does not support brightness control or the Pi needs to
  echo be rebooted for configuration changes made by this
  echo script to be effective.
  echo Try rebooting and re-running this script.
fi

# Auto start our ADS-B program on startup
if (( `grep -c adsbreader $HOME/.profile` == 0 ))
then
        echo "
$HOME/bin/adsbreader > /dev/null 2>&1
" >> $HOME/.profile
fi

echo Configuration complete. Run the buildpi.sh script on the
echo Linux host to install Qt and the ADS-B Traffic display application
echo and reboot the Pi.
fi
