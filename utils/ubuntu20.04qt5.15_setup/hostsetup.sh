#!/bin/bash

# First, check that we're running on a PC and not the Pi
if (( `uname -m | grep -c x86 -` != 1 ))
then
  echo This script must be run on the Linux host machine,
  echo not on the Pi
  exit -1
fi

USER=pi
HOME=/home/$USER
KNOWN_HOSTS=$HOME/.ssh/known_hosts
GPS_IDVENDOR=1546
GPS_IDPRODUCT=01a7
ID_RSA='-----BEGIN RSA PRIVATE KEY-----
MIIEoAIBAAKCAQEAjzOR9SYFNJZlVWBhMgjmWJPF7u9alI0iXWBS6J3Ec0n8J8kF
5ivtXO9SAuu3GDvqL3d87GSKXPGTbRrc7v6necgJp+BOfrQ0xRzcwkEbHZS0gv7y
V1Rg5E6V5IJcAiYWP/BKUFnVvblnRb1VHlGdQ11kYtrPGjBfeSCQpAGfMGcFOo2m
olWYNL8w5fnEAv+ZSEqZYimTJRONVJ4RLyhkO/lBc4IeSSIsbMY8XFyiJJOJl8xN
W2gBE3+iqN7XBCZafhoRbu1gywdpaTxk4OZKxiI9a3acle4ZVE/CrGaSDdt6uVau
+6620gouTcEvphlDasg3+R/ZQkNBoFE+4LLXmQIBJQKCAQAms/3vOrVMe6yvSnsi
R5gznY9xAmuCGE56GgiR5XNdbfEfgnBMC99JjMoOoJJZk6cTv23fBmqVqRMWkaN+
3QqrStItXzfPN5+PODuqH28cwGgjZ3/Ek1h1DlIGaGvk57L8hiHsM/SUJEVsy2Mc
83aclcgat62YYBnNtcY6KfOsNx1LoSGh35K/Lr3YAkuQdKWQhQPTIVg1uy4sEAzX
bRjlJxjuRac3/WiEkiSyjKSWQ2c/7hjj9OtWQfav+VN8hZwf8MV5GI1mmLku5qbu
eV1qSvk5epk2w2H/WMCGGFQ3h63eYmZ6hnVWzbfkvi+estNo4SnQsnjqB7P+9EYq
1HIVAoGBAMc4wpql61U6c3NZwdnFHPtx4BMcoSFA2Hl7vph9HxQePLIHpzM9D2kg
zYaf4W9FQGaAMK1FjeWdnci4cmw/8tfDU8M0belHn8iHqvZhKP9jZQvwa1qJm5JR
Lk790cYcDsruaT80l99WFOHJFxhB5Yxl3L5FGDxF4fG01/aOBlJPAoGBALgDkHXy
SKQDuGWGldKnZjk/sZG4Wd63BoKcPa+U9ac0RX/6P5nwHFouQmEn2ThM81hEqqpk
AxsdIPxlUekREfREmEBg19K6/1oabzgNYqTzDjDzLAsoGTYFjETxoS3xI3lHTifO
i77t/CQctzP/atwA3ZIv2apnRM+I8XfnZ0WXAoGAYOsuPWV5Z7uSHHDa2KUVBLPw
eAAXCUIkH2yh6VGgaqbtGFbACxbJOg/1SGl0lv8YaTdxoGcGwrthhD4OJtMAhKQ2
llDbhj6FFXJu2LmlPfkAubM7JSBZhXOZ/Oous2eYfmYlXQTUQyL1Zuw0v7Fa6lsR
cVILykuCrPAxtjdBWHkCgYBjd4xbbjUaYuA24PcKFUwDRQYJlBTnFtMWKvDpSZmK
0CWRSP/I1NEOJtfF0Fm9kWDxcTmoNg+ERxi/yBCZqFy7Y1k3jkslzN0OHCBxU1fq
dYshYNmJfXVpTxtjiYeOdIHgsOv5124iBBmW+sPkwWoxu0dV8Fn7Pr1pQxq9Wn0D
BQKBgH/OyXIf7yz32+PNuDXPgyg3RhK+vxgUi/hHwybP8ZUPmbIJ7LOL0suJXSFr
xtIFtKJP1tG5Q7DCDAYK6vHckqdEAiA/QXywbT+qQ7us1vKQpXuR6DC21DgWydqz
VU1ynBIa83BR8xGm6F6SZNrGtrBbWtcWefWbyMvJSww3OL1C
-----END RSA PRIVATE KEY-----'

# This must be run as root (e.g. sudo hostconfig.sh)
if [ `whoami` != root ]
then
	echo 'This script needs to be run as root, e.g. sudo <script>'
	exit 1
fi

apt-get -y update
apt-get -y upgrade

su $USER bash -c "echo \"$ID_RSA\" > $HOME/.ssh/id_rsa"
chmod 600 $HOME/.ssh/id_rsa

apt-get install -y libboost-all-dev libudev-dev libinput-dev \
  libts-dev libmtdev-dev libjpeg-dev libfontconfig1-dev libssl-dev \
  libdbus-1-dev libglib2.0-dev libxkbcommon-dev libegl1-mesa-dev \
  libgbm-dev libgles2-mesa-dev mesa-common-dev libasound2-dev \
  libpulse-dev gstreamer1.0-omx libgstreamer1.0-dev \
  libgstreamer-plugins-base1.0-dev  gstreamer1.0-alsa libvpx-dev \
  libsrtp2-dev libsnappy-dev libnss3-dev "^libxcb.*" flex bison \
  libxslt-dev ruby gperf libbz2-dev libcups2-dev libatkmm-1.6-dev \
  libxi6 libxcomposite1 libfreetype6-dev libicu-dev libsqlite3-dev \
  libxslt1-dev libavcodec-dev libavformat-dev libswscale-dev \
  libx11-dev freetds-dev libsqlite3-dev libpq-dev libiodbc2-dev \
  firebird-dev libgst-dev libxext-dev libxcb1 libxcb1-dev libx11-xcb1 \
  libx11-xcb-dev libxcb-keysyms1 libxcb-keysyms1-dev libxcb-image0 \
  libxcb-image0-dev libxcb-shm0 libxcb-shm0-dev libxcb-icccm4 \
  libxcb-icccm4-dev libxcb-sync1 libxcb-sync-dev libxcb-render-util0 \
  libxcb-render-util0-dev libxcb-xfixes0-dev libxrender-dev \
  libxcb-shape0-dev libxcb-randr0-dev libxcb-glx0-dev libxi-dev \
  libdrm-dev libxcb-xinerama0 libxcb-xinerama0-dev libatspi2.0-dev \
  libxcursor-dev libxcomposite-dev libxdamage-dev libxss-dev \
  libxtst-dev libpci-dev libcap-dev libxrandr-dev libdirectfb-dev \
  libaudio-dev libxkbcommon-x11-dev libwayland-egl-backend-dev llvm \
  python3 gdal-bin libwayland-dev g++-aarch64-linux-gnu \
  sqlite3 librtlsdr-dev git gdb-multiarch

# Configure a serial device for our USB GPS
echo 'KERNEL=="ttyACM[0-9]", ATTRS{idProduct}=="'$GPS_IDPRODUCT'", ATTRS{idVendor}=="'$GPS_IDVENDOR'", MODE="0666", SYMLINK+="gps"' \
  > /etc/udev/rules.d/90-gps.rules
 
udevadm control --reload-rules && service udev restart && udevadm trigger
 
# If kernel rtlsdr module is not blacklisted already, add entry to blacklist
if [ `grep -c dvb_usb_rtl28xxu /etc/modprobe.d/blacklist.conf` = 0 ]
then 
echo '
# Disable DVB module so the kernel does not hijack our RTL-SDR radio
blacklist dvb_usb_rtl28xxu
' >> /etc/modprobe.d/blacklist.conf
fi

# Set up proper screen scaling parameters for a 27 inch 4K monitor
if (( `grep -c xrandr $HOME/.profile` == 0 ))
then
  echo "
/usr/bin/xrandr --dpi 283/LVDS --fbmm 344x194
/usr/bin/gsettings set org.gnome.desktop.interface scaling-factor 2
/usr/bin/gsettings set org.gnome.settings-daemon.plugins.xsettings overrides \"{'Gdk/WindowScalingFactor': <2>}\"
export QT_AUTO_SCREEN_SCALE_FACTOR=1 " >> $HOME/.profile
fi


su $USER -c 'mkdir ~/qt ~/qt/source ~/qt/build ~/qt/install ~/qt/5.15 ~/qt/5.15/host ~/qt/5.15/pi'

cd $HOME/qt/source
su $USER -c 'git clone git://code.qt.io/qt/qt5.git'
cd qt5
su $USER -c 'git checkout 5.15'
su $USER -c 'perl init-repository --module-subset=default,-qtwebengine -f'

cd $HOME/qt/build

su $USER -c '../source/qt5/configure -developer-build -confirm-license -opensource -skip qtwebengine -nomake examples -nomake tests -prefix $HOME/qt/5.15/host'
su $USER -c 'make -j `nproc`'
su $USER -c 'make install'

cd $HOME
su $USER -c 'mkdir projects'
cd projects

if [ ! -f $KNOWN_HOSTS ]
then
  echo Creating $KNOWN_HOSTS
  touch $KNOWN_HOSTS
  chown $USER:$USER $KNOWN_HOSTS
fi

if (( `grep -c bitbucket.org $KNOWN_HOSTS` == 0 ))
then
  echo Adding bitbucket.org to $KNOWN_HOSTS
  ssh-keyscan bitbucket.org >> $KNOWN_HOSTS
fi

# Create chart directory
mkdir -p /var/www/html
chown $USER:$USER /var/www/html

su $USER -c 'git clone git@bitbucket.org:icysubzero/adsbmapdisplay.git'

cd adsbmapdisplay
su $USER -c '$HOME/qt/5.15/host/bin/qmake'
su $USER -c 'make -j `nproc`'
su $USER -c 'make clean'
su $USER -c 'nohup ./adsbreader &'

cd $HOME/qt/install
su $USER -c 'wget -O qtcreator.run https://d13lb3tujbc8s0.cloudfront.net/onlineinstallers/qt-unified-linux-x64-4.5.1-online.run'
chmod a+x qtcreator.run 
su $USER -c './qtcreator.run &'

# Make python3 the default python interpreter for the sysroot symbolic link fix script
pushd /usr/bin
ln -s python3 python
popd
