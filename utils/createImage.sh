#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

curdir=`pwd`

ssh $1 -o User=pi sudo mount -t cifs -o username=dev,password=dev //pc/dev /home/pi/pc

ssh $1 -o User=pi sudo dd if=/dev/mmcblk0 of=/home/pi/pc/imgfile.img bs=1M
cd /home/pi/pc

$curdir/pishrink.sh -v imgfile.img imgshrink.img

zip image$1.zip imgshrink.img

rm imgfile.img imgshrink.img
cd $curdir
 
